//////////////////////////////////////////////////////////////////////////////////////////////
/// \file CostFxnTensorL2WarpFieldSymmetricMaskedExcludedUtils.h
/// \brief Utility functions used by tensor CostFxns.
/// \details
/// \author Frederik Lange
/// \date February 2024
/// \copyright Copyright (C) 2018 University of Oxford
//////////////////////////////////////////////////////////////////////////////////////////////


#ifndef COST_FXN_TENSOR_L2_WARP_FIELD_SYMMETRIC_MASKED_EXCLUDED_UTILS_H
#define COST_FXN_TENSOR_L2_WARP_FIELD_SYMMETRIC_MASKED_EXCLUDED_UTILS_H

#include "CostFxn.h"
#include "Volume.h"
#include "VolumeBSpline.h"
#include "VolumeTensor.h"
#include "WarpFieldBSpline.h"

#include "basisfield/fsl_splines.h"

#include <armadillo>

#include <memory>
#include <vector>


namespace MMORF
{
  namespace CostFxnTensorL2WarpFieldSymmetricMaskedExcludedUtils
  {

    /// Get cost under current parameterisation
    float cost(
        const std::shared_ptr<MMORF::VolumeTensor>     vol_ref,
        const std::shared_ptr<MMORF::VolumeTensor>     vol_mov,
        const std::shared_ptr<MMORF::Volume>           mask_ref,
        const std::shared_ptr<MMORF::Volume>           mask_mov,
        const arma::fmat                               affine_ref,
        const arma::fmat                               affine_mov,
        const std::shared_ptr<MMORF::WarpFieldBSpline> warp_field,
        const std::vector<std::vector<float> >         sample_positions_ref,
        const std::vector<std::vector<float> >         sample_positions_warp
    );

    /// Get Jte under current parameterisation
    arma::fvec grad(
        const std::shared_ptr<MMORF::VolumeTensor>     vol_ref,
        const std::shared_ptr<MMORF::VolumeTensor>     vol_mov,
        const std::shared_ptr<MMORF::Volume>           mask_ref,
        const std::shared_ptr<MMORF::Volume>           mask_mov,
        const arma::fmat                               affine_ref,
        const arma::fmat                               affine_mov,
        const std::shared_ptr<MMORF::WarpFieldBSpline> warp_field,
        const std::vector<std::vector<float> >         sample_positions_ref,
        const std::vector<std::vector<float> >         sample_positions_warp,
        const BASISFIELD::Spline1D<float>              spline_1D,
        const std::vector<int>                         sample_dimensions,
        const std::vector<float>                       sample_resolution
    );

    /// Get JtJ under current parameterisation
    MMORF::SparseDiagonalMatrixTiled hess(
        const std::shared_ptr<MMORF::VolumeTensor>     vol_ref,
        const std::shared_ptr<MMORF::VolumeTensor>     vol_mov,
        const std::shared_ptr<MMORF::Volume>           mask_ref,
        const std::shared_ptr<MMORF::Volume>           mask_mov,
        const arma::fmat                               affine_ref,
        const arma::fmat                               affine_mov,
        const std::shared_ptr<MMORF::WarpFieldBSpline> warp_field,
        const std::vector<std::vector<float> >         sample_positions_ref,
        const std::vector<std::vector<float> >         sample_positions_warp,
        const BASISFIELD::Spline1D<float>              spline_1D,
        const std::vector<int>                         sample_dimensions,
        const std::vector<float>                       sample_resolution,
        enum MMORF::CostFxn::HessType                  hess_type
    );

    /// Calculate the rotational effect of the warp field in the warp space
    // warp_field_
    std::vector<arma::fmat> calculate_rotation(
        const std::shared_ptr<MMORF::WarpFieldBSpline> warp_field,
        const std::vector<std::vector<float> >&        sample_positions);

    /// Calculate the following for each position:
    ///
    ///         S = (JJ^T)^1/2
    // warp_field_
    std::vector<arma::fmat> calculate_s(
        const std::shared_ptr<MMORF::WarpFieldBSpline> warp_field,
        const std::vector<std::vector<float> >&        sample_positions);

    // TODO Put in xfm utils module?
    /// Convert a 3x3 row-major vectorised matrix into a vector of armadillo matrices
    std::vector<arma::fmat> vector_to_arma(
        const std::vector<std::vector<float> >& vector_rm);

    /// Convert a 3x3 armadillo matrix to row-major vectorised format
    std::vector<std::vector<float> > arma_to_vector(
        const std::vector<arma::fmat>& matrices);

    /// Rotate tensors by calculating the rotational effect of the affine matrix as follows:
    ///
    ///     R  = ((JJ^T)^-1/2)J
    ///     D' = R*D*R^T
    void affine_reorient_samples(
        std::vector<std::vector<float> >& tensors,
        const arma::fmat&                 affine_mat);

    /// Rotate tensors as follows
    ///
    ///     D' = R*D*R^T
    void nonlin_reorient_samples(
        std::vector<std::vector<float> >& tensors,
        const std::vector<arma::fmat>&    rotations);

    /// Calculate the derivitive of the moving volume wrt to changes in the x, y and z
    /// displacements in the shared reference space
    /// \detail The dimensions of the result are: [3,9,N], i.e. number of directions, number
    ///         of elements in the tensor, number of positions.
    std::vector<std::vector<std::vector<float> > > calculate_derivatives_mov(
        const std::shared_ptr<MMORF::VolumeTensor> vol_mov,
        const std::vector<std::vector<float> >&    positions,
        const arma::fmat&                          affine_mat);

    /// Calculate the gradient of the cost per jacobian element
    std::vector<std::vector<std::vector<float> > > calculate_grad_per_jacobian_element(
        const std::vector<std::vector<float> >& g_samples,
        const std::vector<arma::fmat>&          r_mats,
        const std::vector<arma::fmat>&          s_mats);

    /// Calculate the gradient of the cost per spline coefficient
    arma::fvec calculate_grad_per_spline(
        const std::shared_ptr<MMORF::Volume>                  mask_ref,
        const std::shared_ptr<MMORF::Volume>                  mask_mov,
        const std::shared_ptr<MMORF::WarpFieldBSpline>        warp_field,
        const std::vector<std::vector<float> >                sample_positions_ref,
        const std::vector<std::vector<float> >                sample_positions_warp,
        const BASISFIELD::Spline1D<float>                     spline_1D,
        const std::vector<int>                                sample_dimensions,
        const std::vector<float>                              sample_resolution,
        const std::vector<std::vector<std::vector<float> > >& grad_per_jacobian_element,
        const std::vector<std::vector<float> >&               samples_error,
        const std::vector<std::vector<float> >&               sample_positions_warped);

    /// Calculate the Hessian of the cost per spline coefficient
    MMORF::SparseDiagonalMatrixTiled calculate_hess_per_spline(
        const std::shared_ptr<MMORF::Volume>                  mask_ref,
        const std::shared_ptr<MMORF::Volume>                  mask_mov,
        const std::shared_ptr<MMORF::WarpFieldBSpline>        warp_field,
        const std::vector<std::vector<float> >                sample_positions_ref,
        const std::vector<std::vector<float> >                sample_positions_warp,
        const BASISFIELD::Spline1D<float>                     spline_1D,
        const std::vector<int>                                sample_dimensions,
        const std::vector<float>                              sample_resolution,
        const std::vector<std::vector<std::vector<float> > >& grad_per_jacobian_element,
        const std::vector<std::vector<float> >&               sample_positions_warped);

    /// Calculate the Hessian of the cost per spline coefficient
    MMORF::SparseDiagonalMatrixTiled calculate_hess_per_spline_diag_hess(
        const std::shared_ptr<MMORF::Volume>                  mask_ref,
        const std::shared_ptr<MMORF::Volume>                  mask_mov,
        const std::shared_ptr<MMORF::WarpFieldBSpline>        warp_field,
        const std::vector<std::vector<float> >                sample_positions_ref,
        const std::vector<std::vector<float> >                sample_positions_warp,
        const BASISFIELD::Spline1D<float>                     spline_1D,
        const std::vector<int>                                sample_dimensions,
        const std::vector<float>                              sample_resolution,
        const std::vector<std::vector<std::vector<float> > >& grad_per_jacobian_element,
        const std::vector<std::vector<float> >&               sample_positions_warped);

  } // namespace CostFxnTensorL2WarpFieldSymmetricMaskedExcludedUtils
} // namespace MMORF

#endif // COST_FXN_TENSOR_L2_WARP_FIELD_SYMMETRIC_MASKED_EXCLUDED_UTILS_H
