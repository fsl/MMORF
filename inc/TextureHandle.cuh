//////////////////////////////////////////////////////////////////////////////////////////////
/// \file
/// \brief RAII wrappers for dealing with CUDA Texture objects
/// \details Use these helper classes to deal with the hassle of loading image volumes into
///          texture memory, and ensuring proper memory acquisition and release
/// \author Frederik Lange
/// \date May 2018
/// \copyright Copyright (C) 2018 University of Oxford
//////////////////////////////////////////////////////////////////////////////////////////////
#ifndef TEXTURE_HANDLE_CUH
#define TEXTURE_HANDLE_CUH

#include <thrust/device_vector.h>

#include <vector>

/// Multi-Modal Registration Framework
namespace MMORF
{
  class TextureHandleLinear
  {
    public:
////////////////////////////////////////////////////////////////////////////////
// Rule of 5 (Moveable but not copyable)
////////////////////////////////////////////////////////////////////////////////
      /// Move ctor
      TextureHandleLinear(TextureHandleLinear&& rhs) = default;
      /// Move assignment operator
      TextureHandleLinear& operator=(TextureHandleLinear&& rhs) = default;
      /// Copy ctor
      TextureHandleLinear(const TextureHandleLinear& rhs) = delete;
      /// Copy assignment operator
      TextureHandleLinear& operator=(const TextureHandleLinear& rhs) = delete;
////////////////////////////////////////////////////////////////////////////////
// Class Specific Functions
////////////////////////////////////////////////////////////////////////////////
      /// Ctor
      TextureHandleLinear(
        const std::vector<float>& ima,
        const std::vector<int>& ima_sz,
        const bool clamp_border = false,
        const bool normalised = false);
      TextureHandleLinear(
        const std::vector<float>& ima,
        const int xsz,
        const int ysz,
        const int zsz,
        const bool clamp_border = false,
        const bool normalised = false);
      /// Dtor
      ~TextureHandleLinear();
      /// Return created texture
      cudaTextureObject_t get_texture();
    private:
      cudaTextureObject_t texture_;
      thrust::device_vector<float> memory_;
  }; // TextureHandleLinear


  class TextureHandlePitched
  {
    public:
      //////////////////////////////////////////////////////////////////////////
      // Rule of 5 (Moveable but not copyable)
      //////////////////////////////////////////////////////////////////////////
      /// Move ctor
      TextureHandlePitched(TextureHandlePitched&& rhs) = default;
      /// Move assignment operator
      TextureHandlePitched& operator=(TextureHandlePitched&& rhs) = default;
      /// Copy ctor
      TextureHandlePitched(const TextureHandlePitched& rhs) = delete;
      /// Copy assignment operator
      TextureHandlePitched& operator=(const TextureHandlePitched& rhs) = delete;
      //////////////////////////////////////////////////////////////////////////
      // Class Specific Functions
      //////////////////////////////////////////////////////////////////////////
      /// Ctor
      TextureHandlePitched(
          const std::vector<float>& ima,
          const std::vector<int>& ima_sz,
          const bool clamp_border = false,
          const bool normalised = false);
      TextureHandlePitched(
        const std::vector<float>& ima,
        const int xsz,
        const int ysz,
        const int zsz,
        const bool clamp_border = false,
        const bool normalised = false);
      /// Dtor
      ~TextureHandlePitched();
      /// Return created texture
      cudaTextureObject_t get_texture();
    private:
      cudaTextureObject_t texture_;
      cudaArray*          memory_;
  }; // TextureHandlePitched

} // MMORF
#endif // TEXTURE_HANDLE_CUH
