//////////////////////////////////////////////////////////////////////////////////////////////
/// \file CostFxnSSDWarpFieldSymmetricMaskedExcludedUtils.h
/// \brief Utility functions used by CostFxnsSSDWarpFieldSymmetricMaskedExcluded classes.
/// \details
/// \author Frederik Lange
/// \date February 2024
/// \copyright Copyright (C) 2018 University of Oxford
//////////////////////////////////////////////////////////////////////////////////////////////


#ifndef COST_FXN_SSD_WARP_FIELD_SYMMETRIC_MASKED_EXCLUDED_UTILS_H
#define COST_FXN_SSD_WARP_FIELD_SYMMETRIC_MASKED_EXCLUDED_UTILS_H


#include "Volume.h"
#include "CostFxn.h"
#include "WarpFieldBSpline.h"
#include "BiasFieldBSpline.h"

#include "basisfield/fsl_splines.h"

#include <armadillo>

#include <memory>
#include <vector>


namespace MMORF {

  namespace CostFxnSSDWarpFieldSymmetricMaskedExcludedUtils {

    /// Get cost under current parameterisation
    float cost(
        const std::shared_ptr<MMORF::Volume>           vol_mov,
        const std::shared_ptr<MMORF::Volume>           vol_ref,
        const std::shared_ptr<MMORF::Volume>           mask_mov,
        const std::shared_ptr<MMORF::Volume>           mask_ref,
        const std::shared_ptr<MMORF::BiasFieldBSpline> bias_field,
        const std::shared_ptr<MMORF::WarpFieldBSpline> warp_field,
        const arma::fmat                               affine_mov,
        const float                                    norm_factor_mov,
        const float                                    norm_factor_ref,
        const std::vector<std::vector<float> >         sample_positions_ref,
        const std::vector<std::vector<float> >         sample_positions_warp);

    /// Get Jte under current parameterisation
    arma::fvec grad(
        const std::shared_ptr<MMORF::Volume>           vol_mov,
        const std::shared_ptr<MMORF::Volume>           vol_ref,
        const std::shared_ptr<MMORF::Volume>           mask_mov,
        const std::shared_ptr<MMORF::Volume>           mask_ref,
        const std::shared_ptr<MMORF::BiasFieldBSpline> bias_field,
        const std::shared_ptr<MMORF::WarpFieldBSpline> warp_field,
        const arma::fmat                               affine_mov,
        const float                                    norm_factor_mov,
        const float                                    norm_factor_ref,
        const std::vector<std::vector<float> >         sample_positions_ref,
        const std::vector<std::vector<float> >         sample_positions_warp,
        const std::vector<int>                         sample_dimensions,
        const BASISFIELD::Spline1D<float>&             spline_1D);

    /// Get JtJ under current parameterisation
    MMORF::SparseDiagonalMatrixTiled hess(
        const std::shared_ptr<MMORF::Volume>           vol_mov,
        const std::shared_ptr<MMORF::Volume>           mask_mov,
        const std::shared_ptr<MMORF::Volume>           mask_ref,
        const std::shared_ptr<MMORF::WarpFieldBSpline> warp_field,
        const arma::fmat                               affine_mov,
        const float                                    norm_factor_mov,
        const std::vector<std::vector<float> >         sample_positions_ref,
        const std::vector<std::vector<float> >         sample_positions_warp,
        const std::vector<int>                         sample_dimensions,
        const BASISFIELD::Spline1D<float>&             spline_1D,
        const MMORF::CostFxn::HessType                 hess_type);

    /// Calculate a scaling value that normalises to the robust mean
    /// \details By robust mean we refer to the mean of values which are part of the actual
    ///          object of interest. In order to do this, we first find the global mean of
    ///          the samples, and then find the mean of all samples with values greater
    ///          than 1/6th of the global mean
    float calculate_robust_norm_factor(
        const std::vector<float>& image_samples,
        const std::vector<float>& ref_mask_samples,
        const std::vector<float>& mov_mask_samples);

  } // namespace CostFxnSSDWarpFieldSymmetricMaskedExcludedUtils
} // namespace MMORF


#endif // COST_FXN_SSD_WARP_FIELD_SYMMETRIC_MASKED_EXCLUDED_UTILS_H
