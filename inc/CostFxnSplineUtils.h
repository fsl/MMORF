//////////////////////////////////////////////////////////////////////////////////////////////
/// \file CostFxnSplineUtils.h
/// \brief Spline field utility functions used by CostFxns
/// \details
/// \author Frederik Lange
/// \date February 2024
/// \copyright Copyright (C) 2018 University of Oxford
//////////////////////////////////////////////////////////////////////////////////////////////

#ifndef COST_FXN_SPLINE_UTILS_H
#define COST_FXN_SPLINE_UTILS_H


#include "SparseDiagonalMatrixTiled.h"
#include "WarpFieldBSpline.h"
#include "CostFxn.h"

#include "basisfield/fsl_splines.h"

#include <memory>
#include <vector>


namespace MMORF {

  namespace CostFxnSplineUtils {

    // Calculate a sub-vector of Jte
    std::vector<float> calculate_sub_jte(
        const std::vector<float>& prod_ima,
        const std::vector<int>&   ima_sz,
        const std::vector<float>& spline_x,
        const std::vector<float>& spline_y,
        const std::vector<float>& spline_z,
        const std::vector<int>&   coef_sz,
        const std::vector<int>&   ksp);

    // Calculate the offsets for a sparse diagonal matrix
    // for a field of the specified size
    std::vector<int> calculate_sparse_diag_offsets(
        const std::vector<int> &coef_sz);

    // Calclulate sub-matrix of jtj for symmetrical splines
    void calculate_sub_jtj_symmetrical(
        const std::vector<float>&         prod_ima,
        const std::vector<int>&           ima_sz,
        const std::vector<float>&         spline_x,
        const std::vector<float>&         spline_y,
        const std::vector<float>&         spline_z,
        const std::vector<int>&           coef_sz,
        const std::vector<int>&           ksp,
        const unsigned int                sub_jtj_row,
        const unsigned int                sub_jtj_col,
        MMORF::SparseDiagonalMatrixTiled& sparse_jtj);

    // Calclulate sub-matrix of jtj for symmetrical splines
    void calculate_sub_jtj_symmetrical_diag_hess(
        const std::vector<float>&         prod_ima,
        const std::vector<int>&           ima_sz,
        const std::vector<float>&         spline_x,
        const std::vector<float>&         spline_y,
        const std::vector<float>&         spline_z,
        const std::vector<int>&           coef_sz,
        const std::vector<int>&           ksp,
        const unsigned int                sub_jtj_row,
        const unsigned int                sub_jtj_col,
        MMORF::SparseDiagonalMatrixTiled& sparse_jtj);

    // Calclulate sub-matrix of jtj for symmetrical splines
    void calculate_sub_jtj_non_symmetrical(
        const std::vector<float>&         prod_ima,
        const std::vector<int>&           ima_sz,
        const std::vector<float>&         spline_x_1,
        const std::vector<float>&         spline_y_1,
        const std::vector<float>&         spline_z_1,
        const std::vector<float>&         spline_x_2,
        const std::vector<float>&         spline_y_2,
        const std::vector<float>&         spline_z_2,
        const std::vector<int>&           coef_sz,
        const std::vector<int>&           ksp,
        const unsigned int                sub_jtj_row,
        const unsigned int                sub_jtj_col,
        MMORF::SparseDiagonalMatrixTiled& sparse_jtj);

    // Calclulate sub-matrix of jtj for symmetrical splines
    void calculate_sub_jtj_non_symmetrical_diag_hess(
        const std::vector<std::vector<float> >& prod_ima,
        const std::vector<int>&                 ima_sz,
        const std::vector<float>&               spline_x_1,
        const std::vector<float>&               spline_y_1,
        const std::vector<float>&               spline_z_1,
        const std::vector<float>&               spline_x_2,
        const std::vector<float>&               spline_y_2,
        const std::vector<float>&               spline_z_2,
        const std::vector<int>&                 coef_sz,
        const std::vector<int>&                 ksp,
        const unsigned int                      sub_jtj_row,
        const unsigned int                      sub_jtj_col,
        MMORF::SparseDiagonalMatrixTiled&       sparse_jtj);

    /// Calculate a sub-vector of Jte
    std::vector<float> calculate_sub_grad(
        const std::vector<float>& grad_jxx_ima,
        const std::vector<int>&   ima_sz,
        const std::vector<float>& spline_x,
        const std::vector<float>& spline_y,
        const std::vector<float>& spline_z,
        const std::vector<int>&   coef_sz,
        const std::vector<int>&   ksp);

    // Convert 1D spline to vector of floats
    // Note that this function takes care of correcting the derivative scaling if the
    // spline_res parameter is used. This parameter is the resolution between each value
    // in the spline
    std::vector<float> spline_as_vec(
        const BASISFIELD::Spline1D<float>& spline,
        float                              spline_res = 1.0f,
        int                                diff_order = 0);

    // Create an empty JtJ matrix.
    template<typename Field>
    MMORF::SparseDiagonalMatrixTiled create_empty_jtj(
        const std::shared_ptr<Field>  field,
        enum MMORF::CostFxn::HessType hess_type,
        unsigned int                  n_tiles);

  } // namespace CostFxnSplineUtils
} // namespace MMORF

#endif // COST_FXN_SPLINE_UTILS_H
