//////////////////////////////////////////////////////////////////////////////////////////////
/// \file CostFxnJacobianSingularValuesUtils.h
/// \brief Utility functions used by jacobian-based CostFxns.
/// \details
/// \author Frederik Lange
/// \date February 2024
/// \copyright Copyright (C) 2018 University of Oxford
//////////////////////////////////////////////////////////////////////////////////////////////

#ifndef COST_FXN_LOG_JACOBIAN_SINGULAR_VALUES_UTILS_H
#define COST_FXN_LOG_JACOBIAN_SINGULAR_VALUES_UTILS_H

#include "CostFxn.h"
#include "WarpFieldBSpline.h"
#include "SparseDiagonalMatrixTiled.h"
#include "basisfield/fsl_splines.h"

#include <armadillo>

#include <functional>
#include <memory>
#include <vector>


namespace MMORF {

  namespace CostFxnLogJacobianSingularValuesUtils {

    /// Get cost under current parameterisation
    /// The cost in this case is equal to the sum of the square of the log of the
    /// singular values at each point in the volume
    float cost(
        const std::shared_ptr<MMORF::WarpFieldBSpline> warp_field,
        const int                                      sampling_frequency,
        float                                          norm_factor);

    /// Get grad under current parameterisation
    arma::fvec grad(
        const std::shared_ptr<MMORF::WarpFieldBSpline> warp_field,
        const BASISFIELD::Spline1D<float>&             spline_1D,
        const int                                      sampling_frequency,
        const std::vector<float>&                      sample_resolution,
        float                                          norm_factor);

    /// Get hess under current parameterisation
    MMORF::SparseDiagonalMatrixTiled hess(
        const std::shared_ptr<MMORF::WarpFieldBSpline> warp_field,
        const BASISFIELD::Spline1D<float>&             spline_1D,
        const int                                      sampling_frequency,
        const std::vector<float>&                      sample_resolution,
        float                                          norm_factor,
        const MMORF::CostFxn::HessType                 hess_type);

    /// Calculate the contribution to the cost per sample position
    std::vector<float> calculate_cost_per_sample(
        const std::vector<std::vector<float> >& jacobian_elements);

    /// Calculate the gradient of the cost per jacobian element
    std::vector<std::vector<float> > calculate_grad_per_jacobian_element(
        const std::vector<std::vector<float> >& jacobian_elements);

    /// Calculate the gradient of the cost per spline coefficient
    arma::fvec calculate_grad_per_spline(
        const std::shared_ptr<MMORF::WarpFieldBSpline> warp_field,
        const BASISFIELD::Spline1D<float>&             spline_1D,
        const int                                      sampling_frequency,
        const std::vector<float>&                      sample_resolution,
        const std::vector<std::vector<float> >&        grad_per_jacobian_element);

    MMORF::SparseDiagonalMatrixTiled calculate_hess_per_spline(
        const std::vector<float>&                      cost_per_sample,
        const std::vector<std::vector<float> >&        grad_per_jacobian_element,
        const std::shared_ptr<MMORF::WarpFieldBSpline> warp_field,
        const BASISFIELD::Spline1D<float>&             spline_1D,
        const int                                      sampling_frequency,
        const std::vector<float>&                      sample_resolution);

    MMORF::SparseDiagonalMatrixTiled calculate_hess_per_spline_diag_hess(
        const std::vector<float>&                      cost_per_sample,
        const std::vector<std::vector<float> >&        grad_per_jacobian_element,
        const std::shared_ptr<MMORF::WarpFieldBSpline> warp_field,
        const BASISFIELD::Spline1D<float>&             spline_1D,
        const int                                      sampling_frequency,
        const std::vector<float>&                      sample_resolution);
  } // namespace CostFxnLogJacobianSingularValuesUtils
} // namespace MMORF

#endif // COST_FXN_LOG_JACOBIAN_SINGULAR_VALUES_UTILS_H
