//////////////////////////////////////////////////////////////////////////////////////////////
/// \file
/// \brief Linear solver implementaion using conjugate gradient
/// \details
/// \author Jesper Andersson, Frederik Lange, Paul McCarthy
/// \date May 2024
/// \copyright Copyright (C) 2024 University of Oxford
//////////////////////////////////////////////////////////////////////////////////////////////
#ifndef LINEAR_SOLVER_CG_H
#define LINEAR_SOLVER_CG_H


#include "LinearSolver.h"
#include "SparseDiagonalMatrixTiled.h"

#include <armadillo>


/// Multi-Modal Registration Framework
namespace MMORF
{
  class LinearSolverCG : public LinearSolver
  {
  public:
    LinearSolverCG(
        const int   max_iterations     = 100,
        const float relative_tolerance = 1e-3f);

    ~LinearSolverCG();

    arma::fmat solve(
        const MMORF::SparseDiagonalMatrixTiled& coefficients,
        const arma::fmat&                       constants) const override;

  private:
    int   max_iterations_;
    float relative_tolerance_;

  }; // LinearSolverCG

} // MMORF

#endif // LINEAR_SOLVER_CG_H
