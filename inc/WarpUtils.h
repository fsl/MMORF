//////////////////////////////////////////////////////////////////////////////////////////////
/// \file WarpUtils.h
/// \brief Warp/affine transformation utility functions
/// \author Frederik Lange
/// \date April 2024
/// \copyright Copyright (C) 2018 University of Oxford
//////////////////////////////////////////////////////////////////////////////////////////////
#ifndef WARP_UTILS_H
#define WARP_UTILS_H


#include "WarpField.h"

#include <armadillo>

#include <vector>


namespace MMORF
{
  namespace WarpUtils
  {

    /// Affine transform a given set of positions
    std::vector<std::vector<float> > apply_affine_transform(
        const std::vector<std::vector<float> >& positions,
        const arma::fmat&                       affine_mat);

    /// Given a set of positions, return the warped positions
    std::vector<std::vector<float> > apply_nonlinear_transform(
        const std::vector<std::vector<float> >& positions,
        const MMORF::WarpField&                 field);

    /// Apply both an affine transform and a warp to a given set of positions
    /// \details This is necessary when the affine transform is acting only on the original
    ///          sample positions, and independent of the warp field
    std::vector<std::vector<float> > apply_affine_then_nonlinear_transform(
        const std::vector<std::vector<float> >& positions,
        const arma::fmat&                       affine_mat,
        const MMORF::WarpField&                 field);

    /// Warp the given positions and then apply the affine transform.
    /// \details This is necessary in a number of situations, such as when finding the
    ///          derivative of a volume in a space reached via the given affine transform
    std::vector<std::vector<float> > apply_nonlinear_then_affine_transform(
        const std::vector<std::vector<float> >& positions,
        const arma::fmat&                       affine_mat,
        const MMORF::WarpField&                 field);

    /// Add two arrays of positions together
    std::vector<std::vector<float> > add_positions(
        const std::vector<std::vector<float> >& positions_first,
        const std::vector<std::vector<float> >& positions_second);

  } // namespace WarpUtils
} // namespace MMORF

#endif // WARP_UTILS_H
