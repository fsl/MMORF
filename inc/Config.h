///
/// This file declares the Config class, for storage of global configuration
/// settings. The Config class follows the Singleton pattern. A Config instance
/// is created within mmorf.cpp when command-line arguments are parsed, and
/// accessed by other parts of MMROF via the static Config::getInstance function.
///
/// This class is intended to be used only for configuration settings which are
/// fixed for the duration of execution.
///

#ifndef MMORF_CONFIG_H
#define MMORF_CONFIG_H

namespace MMORF {

  class Config {

    // Allow configuration settings to be overridden within
    // unit tests - see test/mmorf_test_utils.h for more details.
    friend class TestConfig;

  public:

    static const Config& getInstance(
      bool         debug    = false,
      unsigned int nthreads = 1);

    bool         debug()    const;
    unsigned int nthreads() const;

    Config(const Config &)           = delete;
    Config& operator=(const Config&) = delete;
    Config(Config &&)                = delete;
    Config& operator=(Config &&)     = delete;

  private:
    bool           debug_;
    unsigned int   nthreads_;

    Config(
      bool         debug,
      unsigned int nthreads);
    ~Config() = default;
  };
}

#endif
