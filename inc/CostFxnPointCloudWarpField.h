//////////////////////////////////////////////////////////////////////////////////////////////
/// \file
/// \brief Cost function based on the sum of squared distances between two point clouds
/// \details The warp is defined by b-spline field coefficients, one set per dimension in the
///          volume. Implicit point correspondance is assumed (i.e., clouds must be the same
///          size and ordered identically)
/// \author Frederik Lange
/// \date April 2024
/// \copyright Copyright (C) 2024 University of Oxford
//////////////////////////////////////////////////////////////////////////////////////////////
#ifndef COST_FXN_POINT_CLOUD_WARP_FIELD_H
#define COST_FXN_POINT_CLOUD_WARP_FIELD_H

#include "CostFxn.h"
#include "WarpFieldBSpline.h"
#include "SparseDiagonalMatrixTiled.h"

#include "newmesh/newmesh.h"

#include <armadillo>

#include <memory>

/// Multi-Modal Registration Framework
namespace MMORF
{
  template<MMORF::CostFxn::HessType hess_type>
  class CostFxnPointCloudWarpField : public MMORF::CostFxn
  {
    public:
////////////////////////////////////////////////////////////////////////////////
// Class Specific Functions
////////////////////////////////////////////////////////////////////////////////
      /// Construct by passing in a list of points
      /// \param points_ref Reference (stationary) points list (3 x n_points)
      /// \param points_mov Moving points list (3 x n_points)
      /// \param affine_ref Pre-calculated affine transform FROM a common space TO the
      ///                   reference space
      /// \param affine_mov Pre-calculated affine transform FROM a common space TO the
      ///                   moving space
      /// \param warp_field Shared pointer to fully formed warp field
      CostFxnPointCloudWarpField(
          const std::vector<std::vector<float> >&  points_ref,
          const std::vector<std::vector<float> >&  points_mov,
          const arma::fmat&                        affine_ref,
          const arma::fmat&                        affine_mov,
          std::shared_ptr<MMORF::WarpFieldBSpline> warp_field
          );
      /// Construct by passing in fully constructed newmesh gifti objects
      /// \param mesh_ref Reference (stationary) gifti mesh
      /// \param mesh_mov Moving gifti mesh
      /// \param affine_ref Pre-calculated affine transform FROM a common space TO the
      ///                   reference space
      /// \param affine_mov Pre-calculated affine transform FROM a common space TO the
      ///                   moving space
      /// \param warp_field Shared pointer to fully formed warp field
      CostFxnPointCloudWarpField(
          const NEWMESH::newmesh&                  points_ref,
          const NEWMESH::newmesh&                  points_mov,
          const arma::fmat&                        affine_ref,
          const arma::fmat&                        affine_mov,
          std::shared_ptr<MMORF::WarpFieldBSpline> warp_field
          );
      /// Get the current value of the parameters
      std::vector<std::vector<float> > get_parameters() const override;
      /// Set the current value of the parameters
      void set_parameters(
          const std::vector<std::vector<float> >& parameters) override;
      /// Get cost under current parameterisation
      float cost() const override;
      /// Get Jte under current parameterisation
      arma::fvec grad() const override;
      /// Get JtJ under current parameterisation
      MMORF::SparseDiagonalMatrixTiled hess() const override;
    private:
//////////////////////////////////////////////////
// Private datamembers
//////////////////////////////////////////////////
      std::vector<std::vector<float> >         points_ref_;
      std::vector<std::vector<float> >         points_mov_;
      arma::fmat                               affine_ref_;
      arma::fmat                               affine_mov_;
      std::shared_ptr<MMORF::WarpFieldBSpline> warp_field_;
      MMORF::SparseDiagonalMatrixTiled         hess_;
  }; // CostFxnPointCloudWarpField
} // MMORF
#endif // COST_FXN_POINT_CLOUD_WARP_FIELD_H
