#include "mmorf_test_utils.h"

#include "catch_amalgamated.hpp"

#include "CubicInterp.cuh"
#include "TextureHandle.cuh"
#include "helper_cuda.cuh"

#include "newimage/newimageall.h"

#include <thrust/host_vector.h>
#include <thrust/device_vector.h>

#include <armadillo>
#include <string>
#include <vector>
#include <memory>
#include <iomanip>
#include <iostream>
#include <algorithm>
#include <filesystem>
#include <fstream>
#include <utility>

namespace test_CubicInterp {

namespace fs          = std::filesystem;
namespace CubicInterp = CubicInterpCUDA;
using namespace         MMORFTestUtils;


class CubicInterpFixture
{
public:

  const std::string       input_file_;
  const std::string       prefilter_benchmark_file_;
  const std::string       splinterp_benchmark_file_;
  NEWIMAGE::volume<float> input_;
  NEWIMAGE::volume<float> prefilter_benchmark_;
  NEWIMAGE::volume<float> splinterp_benchmark_;

  CubicInterpFixture()
    : input_file_(              mmorf_data_dir() / "MNI152_T1_2mm.nii.gz")
    , prefilter_benchmark_file_(mmorf_data_dir() / "CubicInterp" / "prefilter_benchmark.nii.gz")
    , splinterp_benchmark_file_(mmorf_data_dir() / "CubicInterp" / "splinterp_benchmark.nii.gz")
  {
    NEWIMAGE::read_volume(input_,               input_file_);
    NEWIMAGE::read_volume(prefilter_benchmark_, prefilter_benchmark_file_);
    NEWIMAGE::read_volume(splinterp_benchmark_, splinterp_benchmark_file_);
  }
};

TEST_CASE_METHOD(CubicInterpFixture, "CubicBSplinePrefilter3D")
{

  unsigned int xs = input_.xsize();
  unsigned int ys = input_.ysize();
  unsigned int zs = input_.zsize();
  thrust::host_vector<float> values(xs * ys * zs);
  unsigned int i = 0;
  for (auto it = input_.fbegin(); it != input_.fend(); ++it, ++i)
  {
    values[i] = *it;
  }

  // Calculate the b-spline coefficients for the volume
  auto coeffs_pitched_ptr = CubicInterp::CopyVolumeHostToDevice(
    values.data(), xs, ys, zs);
  CubicInterp::CubicBSplinePrefilter3D(
    static_cast<float*>(coeffs_pitched_ptr.ptr),
    static_cast<unsigned int>(coeffs_pitched_ptr.pitch),
    xs, ys, zs);
  CubicInterp::CopyVolumeDeviceToHost(
    values.data(), coeffs_pitched_ptr, xs, ys, zs);

  i = 0;
  for (int z = 0; z < zs; z++) {
  for (int y = 0; y < ys; y++) {
  for (int x = 0; x < xs; x++) {
    REQUIRE_APPROX(values[i], prefilter_benchmark_(x, y, z), 1e-6);
    i++;
  }}}
}


__global__ void kernel_cubicTex3D(
  cudaTextureObject_t       tex,
  const unsigned int        n_samples,
  const float* __restrict__ x_coords,
  const float* __restrict__ y_coords,
  const float* __restrict__ z_coords,
  float*       __restrict__ output)
{
  auto sample_id = blockIdx.x * blockDim.x + threadIdx.x;
  if (sample_id < n_samples){
    auto coord = make_float3(
        x_coords[sample_id] + 0.5,
        y_coords[sample_id] + 0.5,
        z_coords[sample_id] + 0.5);
    output[sample_id] = CubicInterp::cubicTex3D<float, float>(tex, coord);
  }
}

// Test B-spline interpolation by upsampling
// a prefiltered MNI152_T1_2mm ROI by a
// factor of 2 along each dimension
TEST_CASE_METHOD(CubicInterpFixture, "cubicTex3D")
{
  // input MNI152_T1_2mm ROI dimensions
  unsigned int xsz      = 35;
  unsigned int ysz      = 45;
  unsigned int zsz      = 36;
  unsigned int nvoxels  = xsz * ysz * zsz;
  unsigned int nsamples = nvoxels * 8;

  std::vector<float> values( nvoxels);
  std::vector<float> xcoords(nsamples);
  std::vector<float> ycoords(nsamples);
  std::vector<float> zcoords(nsamples);

  // copy image data to a vector
  unsigned int i = 0;
  for (auto z = 0; z < zsz; z++) {
  for (auto y = 0; y < ysz; y++) {
  for (auto x = 0; x < xsz; x++) {
    // MNI152_T1_2mm ROI offsets
    values[i] = prefilter_benchmark_(x + 27, y + 35, z + 28);
    i++;
  }}}

  // Create vectors of sampling voxel
  // coordinates.  The offsets here
  // cause the image to be up-sampled
  // by a factor of 2, following the
  // NIfTI convention of aligning the
  // centres of the first voxels in
  // the input and output grids.
  i = 0;
  std::vector<float> offsets{0.0, 0.5};
  for (auto z = 0; z < zsz; z++) {
  for (auto zoff : offsets) {
  for (auto y = 0; y < ysz; y++) {
  for (auto yoff : offsets) {
  for (auto x = 0; x < xsz; x++) {
  for (auto xoff : offsets) {
    xcoords[i] = x + xoff;
    ycoords[i] = y + yoff;
    zcoords[i] = z + zoff;
    i++;
  }}}}}}

  MMORF::TextureHandlePitched tex(values, xsz, ysz, zsz);

  // Copy sampling coordinates to device
  auto xcoords_dev =   thrust::device_vector<float>(xcoords);
  auto ycoords_dev   = thrust::device_vector<float>(ycoords);
  auto zcoords_dev   = thrust::device_vector<float>(zcoords);
  auto output_dev    = thrust::device_vector<float>(nsamples);
  float *xcoords_raw = thrust::raw_pointer_cast(xcoords_dev.data());
  float *ycoords_raw = thrust::raw_pointer_cast(ycoords_dev.data());
  float *zcoords_raw = thrust::raw_pointer_cast(zcoords_dev.data());
  float *output_raw  = thrust::raw_pointer_cast(output_dev .data());

  // Calculate kernel parameters
  int min_grid_size;
  int block_size;
  cudaOccupancyMaxPotentialBlockSize(
    &min_grid_size,
    &block_size,
    kernel_cubicTex3D,
    0,
    0);
  unsigned int grid_size = (nsamples + block_size - 1) / block_size;

  // Call CUDA Kernel
  kernel_cubicTex3D<<<grid_size, block_size>>>(
    tex.get_texture(),
    nsamples,
    xcoords_raw,
    ycoords_raw,
    zcoords_raw,
    output_raw);
  checkCudaErrors(cudaDeviceSynchronize());

  // Copy interpolated data back to host
  auto output_host = thrust::host_vector<float>(output_dev);
  auto output      = std::vector<float>(output_host.size());
  thrust::copy(output_host.begin(), output_host.end(), output.begin());

  // data has been usampled
  // by a factor of 2
  xsz = xsz * 2;
  ysz = ysz * 2;
  zsz = zsz * 2;

  i  = 0;
  for (auto z = 0; z < zsz; z++) {
  for (auto y = 0; y < ysz; y++) {
  for (auto x = 0; x < xsz; x++) {
    REQUIRE_APPROX(output[i], splinterp_benchmark_(x, y, z), 1e-6);
    i++;
  }}}
}

} // namespace test_CubicInterp
