#include "SparseDiagonalMatrixTiled.h"
#include "LinearSolverCG.h"

#include "mmorf_test_utils.h"

#include "catch_amalgamated.hpp"

#include <armadillo>

#include <vector>


// Solve systems of linear equations, e.g.
//
// 3*m + 2*n =  2
// 2*m + 6*n = -8
//
// by solving the equation Ax = b for x,
// where (e.g.):
//
//   A = [ 3 2 ]    b = [  2 ]   x = [ m ]
//       [ 2 6 ]        [ -8 ]       [ n ]
//
// A is stored as a sparse matrix via
// the SparseDiagonalMatrixTiled class

namespace test_LinearSolver {
  
void test_solver(arma::fmat&       Adata,
                 std::vector<int> &offsets,
                 int               ntiles,
                 arma::fvec&       b,
                 arma::fvec&       expx)
{

  MMORF::SparseDiagonalMatrixTiled A(Adata, ntiles, offsets);
  MMORF::LinearSolverCG solver(100, 1e-4);

  auto x = solver.solve(A, b);

  REQUIRE(x.n_rows == expx.n_rows);

  for (int i = 0; i < x.n_rows; i++) {
    REQUIRE_APPROX(x[i], expx[i], 1e-3);
  }
}


TEST_CASE("Basic LinearSolverCG test 2 variables") {
  std::vector<int> offsets{-1, 0, 1};
  arma::fmat       A    = {{3, 2}, {2, 6}};
  arma::fvec       b    = {2, -8};
  arma::fvec       expx = {2, -2};
  test_solver(A, offsets, 1, b, expx);
}

TEST_CASE("Basic LinearSolverCG test 3 variables") {
  std::vector<int> offsets{-2, 0, 2};
  arma::fmat       A    = {{6, 0, 3},
                           {0, 7, 0},
                           {3, 0, 8}};
  arma::fvec       b    = {20, 23, 28};
  arma::fvec       expx = {1.94871795, 3.28571429, 2.76923077};
  test_solver(A, offsets, 1, b, expx);
}

TEST_CASE("Basic LinearSolverCG test 4 variables") {
  std::vector<int> offsets{-3, -1, 0, 1, 3};
  arma::fmat       A    = {{9, 3, 0, 1},
                           {3, 8, 4, 0},
                           {0, 4, 7, 3},
                           {1, 0, 3, 6}};
  arma::fvec       b    = {30, 34, 36, 27};
  arma::fvec       expx = {2.4206709, 1.86491387, 2.95466908, 2.61922031};
  test_solver(A, offsets, 1, b, expx);
}

TEST_CASE("Basic LinearSolverCG test 5 variables") {
  std::vector<int> offsets{-4, -3, -1, 0, 1, 3, 4};
  arma::fmat       A    = {{10, 2, 0, 3, 4},
                           {2, 11, 2, 0, 3},
                           {0, 2, 12, 2, 0},
                           {3, 0, 2, 13, 2},
                           {4, 3, 0, 2, 14}};
  arma::fvec       b    = {40, 45, 50, 55, 60};
  arma::fvec       expx = {1.4606557, 2.43223581, 3.27127972, 2.94008589, 2.92717843};
  test_solver(A, offsets, 1, b, expx);
}

} // namespace test_LinearSolver
