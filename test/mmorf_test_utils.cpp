#include "mmorf_test_utils.h"

#include "newimage/newimage.h"
#include "newimage/newimageio.h"

#include <cassert>
#include <cstdlib>
#include <filesystem>
#include <vector>


namespace fs = std::filesystem;

namespace MMORFTestUtils {

  // MMORF_TEST_DIRECTORY is passed in as
  // a macro when the tests are compiled
  std::filesystem::path mmorf_test_dir()
  {
    return MMORF_TEST_DIRECTORY;
  }

  std::filesystem::path mmorf_data_dir(std::string suffix)
  {
    if (suffix == "") {
      return mmorf_test_dir() / "data";
    }
    else {
      return mmorf_test_dir() / "data" / suffix;
    }
  }

  TempDir::TempDir() {
    std::string dir_template_s(fs::temp_directory_path() / "mmorf_XXXXXX");
    std::vector<char> dir_template_v(dir_template_s.begin(), dir_template_s.end());
    dir_template_v.push_back('\0');

    char* temp_dir_ptr = mkdtemp(dir_template_v.data());

    if (temp_dir_ptr == nullptr) {
      throw std::runtime_error("Could not create temporary directory");
    }

    prev_dir = fs::current_path();
    temp_dir = std::string(temp_dir_ptr);
    fs::current_path(temp_dir);
  };

  TempDir::~TempDir() {
    fs::current_path(prev_dir);
    fs::remove_all(temp_dir);
  };

  std::filesystem::path TempDir::path() { return temp_dir; }

  void save_nifti(std::vector<float> values,
                  int xsz, int ysz, int zsz,
                  arma::mat sform,
                  std::string filename)
  {
    std::vector<std::vector<float>> tvals{values};
    save_nifti({tvals}, xsz, ysz, zsz, sform, filename);
  }

  void save_nifti(std::vector<std::vector<float>> values,
                  int xsz, int ysz, int zsz,
                  arma::mat sform,
                  std::string filename)
  {

    int tsz = values.size();
    NEWIMAGE::volume<float> vol(xsz, ysz, zsz, tsz);
    vol.set_sform(2, sform);

    for (int t = 0; t < tsz; t++) {
      int i = 0;
      for (int z = 0; z < zsz; z++) {
      for (int y = 0; y < ysz; y++) {
      for (int x = 0; x < xsz; x++) {
        vol(x, y, z, t) = values[t][i];
        i++;
      }}}
    }
    NEWIMAGE::write_volume(vol, filename);
  }

  void compare_niftis(std::string filename1,
                      std::string filename2,
                      float       tol,
                      bool        header) {

    NEWIMAGE::volume<float> vol1, vol2;

    NEWIMAGE::read_volume(vol1, filename1, false);
    NEWIMAGE::read_volume(vol2, filename2, false);

    assert(vol1.xsize() == vol2.xsize());
    assert(vol1.ysize() == vol2.ysize());
    assert(vol1.zsize() == vol2.zsize());
    assert(vol1.tsize() == vol2.tsize());

    if (header) {
      REQUIRE_ARMA(  vol1.sform_mat(), vol2.sform_mat(), 1e-6);
      REQUIRE_APPROX(vol1.xdim(),      vol2.xdim(),      1e-6);
      REQUIRE_APPROX(vol1.ydim(),      vol2.ydim(),      1e-6);
      REQUIRE_APPROX(vol1.zdim(),      vol2.zdim(),      1e-6);
    }

    for (auto z = 0; z < vol1.zsize(); z++) {
    for (auto y = 0; y < vol1.ysize(); y++) {
    for (auto x = 0; x < vol1.xsize(); x++) {
      REQUIRE_APPROX(vol1(x, y, z), vol2(x, y, z), tol);
    }}}
  }

} // namespace MMORFTestUtils
