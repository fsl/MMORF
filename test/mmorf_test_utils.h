#ifndef MMORF_TEST_UTILS_H
#define MMORF_TEST_UTILS_H

#include "catch_amalgamated.hpp"

#include <armadillo>

#include <filesystem>


// Short-hand for comparing two floating point values.
#define REQUIRE_APPROX(value, expected, tol) \
  REQUIRE_THAT(value, Catch::Matchers::WithinRel(expected, tol))

// Short-hand for comparing two armadillo matrices/vectors.
#define REQUIRE_ARMA(a, b, tol) \
  REQUIRE(arma::approx_equal(a, b, "absdiff", tol))

// Short-hand for checking that two values are
// within a certain distance of each other
#define REQUIRE_NEAR(a, b, dist) \
  REQUIRE_THAT(a, Catch::Matchers::WithinAbs(b, dist));



namespace MMORFTestUtils {

  // Return <mmorf-dir>/test/
  std::filesystem::path mmorf_test_dir();

  // Return <mmorf-dir>/test/data/[suffix/]
  std::filesystem::path mmorf_data_dir(std::string suffix="");

  // Load an armadillo matrix from file
  template<typename Mat> Mat
  load_test_file(const std::filesystem::path filename,
                 const arma::file_type       format = arma::arma_binary)
  {

    Mat mat;
    if (!mat.load(filename, format)) {
      std::cout << "Could not load file: " << filename << std::endl;
      throw std::runtime_error(filename);
    }
    return mat;
  }

  // Class which, upon instantiation, creates and changes into a temporary diretory
  // and, on destruction, restores the current directory, and deletes the temporary
  // directory.
  class TempDir
  {
  private:
    std::filesystem::path prev_dir;
    std::filesystem::path temp_dir;
  public:
    TempDir();
    ~TempDir();

    std::filesystem::path path();
  };

  void save_nifti(std::vector<float> values,
                  int xsz, int ysz, int zsz,
                  arma::mat sform,
                  std::string filename);

  void save_nifti(std::vector<std::vector<float>> values,
                  int xsz, int ysz, int zsz,
                  arma::mat sform,
                  std::string filename);

  void compare_niftis(std::string filename1,
                      std::string filename2,
                      float       tol    = 0,
                      bool        header = true);

} // namespace MMORFTestUtils

#endif
