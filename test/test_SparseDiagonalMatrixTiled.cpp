// Unit tests for the SparseDiagonalMatrixTiled class functionality
#include "SparseDiagonalMatrixTiled.h"

#include "mmorf_test_utils.h"

#include "catch_amalgamated.hpp"

#include <armadillo>

#include <filesystem>
#include <iostream>
#include <vector>

namespace test_SparseDiagonalMatrixTiled {

namespace fs = std::filesystem;
using namespace MMORFTestUtils;
  
class SPDTFixture
{
public:

  std::vector<std::vector<int>> offsets;
  std::vector<arma::fmat>       data;
  std::vector<int>              ntiles;

  int num_datasets() {
    return ntiles.size();
  }

  SPDTFixture() {
    ntiles .push_back(1);
    offsets.push_back({-1, 0, 1});
    data   .push_back({{3, 2},
                       {2, 6}});

    ntiles .push_back(1);
    offsets.push_back({-2, 0, 2});
    data   .push_back({{8, 0, 7, 0, 0, 0, 0},
                       {0, 9, 0, 5, 0, 0, 0},
                       {9, 0, 9, 0, 7, 0, 0},
                       {0, 3, 0, 9, 0, 1, 0},
                       {0, 0, 3, 0, 1, 0, 8},
                       {0, 0, 0, 6, 0, 7, 0},
                       {0, 0, 0, 0, 8, 0, 8}});

    ntiles .push_back(1);
    offsets.push_back({-3, -2, -1,  0, 1, 2, 3});
    data   .push_back({{ 1,  2,  3,  4},
                       { 5,  6,  7,  8},
                       { 9, 10, 11, 12},
                       {13, 14, 15, 16}});

    ntiles .push_back(1);
    offsets.push_back({-2, 0, 2});
    data   .push_back({{4, 0, 1, 0},
                       {0, 9, 0, 3},
                       {8, 0, 5, 0},
                       {0, 7, 0, 2}});

    ntiles .push_back(2);
    offsets.push_back({0});
    data   .push_back({{4, 0,  1, 0},
                       {0, 9,  0, 3},

                       {8, 0,  5, 0},
                       {0, 7,  0, 2}});

    ntiles .push_back(2);
    offsets.push_back({-1, 0});
    data   .push_back({{4, 0,  1, 0},
                       {3, 9,  2, 3},

                       {8, 0,  5, 0},
                       {5, 7,  0, 2}});

    ntiles .push_back(1);
    offsets.push_back({0, 1, 2});
    data   .push_back({{4, 0, 1, 0, 0},
                       {0, 3, 2, 0, 0},
                       {0, 0, 5, 0, 1},
                       {0, 0, 0, 1, 0},
                       {0, 0, 0, 0, 2}});

    ntiles .push_back(3);
    offsets.push_back({-2, 0, 2});
    data   .push_back({{8, 0, 7, 0,  1, 0, 7, 0,  0, 0, 0, 0},
                       {0, 9, 0, 5,  0, 3, 0, 2,  0, 0, 0, 0},
                       {9, 0, 9, 0,  7, 0, 7, 0,  5, 0, 0, 0},
                       {0, 3, 0, 9,  0, 1, 0, 4,  0, 9, 0, 0},

                       {0, 0, 3, 0,  1, 0, 8, 0,  4, 0, 0, 0},
                       {0, 0, 0, 6,  0, 7, 0, 6,  0, 2, 0, 0},
                       {0, 0, 0, 0,  8, 0, 8, 0,  3, 0, 7, 0},
                       {0, 0, 0, 0,  0, 7, 0, 2,  0, 4, 0, 3},

                       {0, 0, 0, 0,  0, 0, 6, 0,  4, 0, 6, 0},
                       {0, 0, 0, 0,  0, 0, 0, 8,  0, 5, 0, 3},
                       {0, 0, 0, 0,  0, 0, 0, 0,  2, 0, 1, 0},
                       {0, 0, 0, 0,  0, 0, 0, 0,  0, 7, 0, 9}});
  }
};


TEST_CASE_METHOD(SPDTFixture, "Matrix creation") {
  for (auto i = 0; i < num_datasets(); i++) {
    MMORF::SparseDiagonalMatrixTiled A(data[i], ntiles[i], offsets[i]);
    REQUIRE_ARMA(A.convert_to_arma(), data[i], 1e-7);
  }
}


TEST_CASE_METHOD(SPDTFixture, "Matrix vector multiplication") {
  for (auto i = 0; i < num_datasets(); i++) {
    MMORF::SparseDiagonalMatrixTiled A(data[i], ntiles[i], offsets[i]);

    arma::fvec b     = arma::randi<arma::fvec>(data[i].n_rows, arma::distr_param(-100, 100));
    arma::fvec expAb = data[i] * b;
    auto       Ab    = A       * b;

    REQUIRE_ARMA(Ab, expAb, 1e-7);
  }
}


TEST_CASE_METHOD(SPDTFixture, "diag() access") {
  for (auto i = 0; i < num_datasets(); i++) {
    MMORF::SparseDiagonalMatrixTiled A(data[i], ntiles[i], offsets[i]);
    arma::fcolvec expd = A.diag(0);
    REQUIRE_ARMA(expd, A.diag(0), 1e-7);
  }
}


void test_matvecmul_files(std::string prefix, unsigned int ntiles) {

  auto Aspmat = load_test_file<arma::sp_fmat>(prefix + "_A.mat");
  auto offmat = load_test_file<arma::mat>(    prefix + "_offsets.mat");
  auto b      = load_test_file<arma::fmat>(   prefix + "_b.mat");

  auto Amat    = arma::conv_to<arma::fmat>::from(Aspmat);
  auto offsets = arma::conv_to<std::vector<int>>::from(offmat);

  MMORF::SparseDiagonalMatrixTiled A(Amat, ntiles, offsets);

  arma::fmat expAb = Amat * b;
  arma::fmat Ab    = A    * b;

  REQUIRE_ARMA(Ab, expAb, 1e-6);
}

TEST_CASE("Matrix vector multiplication (large 1x1 tiled)") {
  test_matvecmul_files(mmorf_data_dir() / "SparseDiagonalMatrixTiled" / "matvecmul_1_tile", 1);
}
TEST_CASE("Matrix vector multiplication (large 3x3 tiled)") {
  test_matvecmul_files(mmorf_data_dir() / "SparseDiagonalMatrixTiled" / "matvecmul_3_tiles", 3);
}

} // namespace test_SparseDiagonalMatrixTiled 
