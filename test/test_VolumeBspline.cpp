// Testing VolumeBSpline class functionality

#include "mmorf_test_utils.h"

#include "catch_amalgamated.hpp"

#include "Volume.h"
#include "VolumeBSpline.h"

#include "WarpUtils.h"

#include "MMORFio.h"
#include "newimage/newimageall.h"

#include <armadillo>

#include <cstdlib>
#include <string>
#include <vector>
#include <memory>
#include <iomanip>
#include <iostream>
#include <algorithm>
#include <filesystem>
#include <fstream>
#include <utility>

namespace test_VolumeBSpline {

namespace fs = std::filesystem;

using namespace MMORFTestUtils;


template<NEWIMAGE::interpolation INTERP,
         NEWIMAGE::extrapolation EXTRAP> class VolumeBSplineFixtureBase
{
public:

  // We use a 4mm version of the MNI152 standard
  // with interpolation/extrapolation specified
  // by the class template parameters
  const std::string       filename_;
  NEWIMAGE::volume<float> test_vol_;

  // The sample_points_4mm_vector contains mm
  // coordinates for each voxel, for the 4mm
  // version of the MNI152 template
  //
  // The sample_points_ and sample_points_3mm_
  // vectors contain mm coordinates at 2mm and
  // 3mm resolution, which extend beyond the
  // MNI152 bounds, and are used for testing
  // interpolation/ extrapolation.
  std::vector<std::vector<float> > sample_points_;
  std::vector<std::vector<float> > sample_points_3mm_;
  std::vector<std::vector<float> > sample_points_4mm_;

  // mm coordinates of low/high voxel centres
  std::vector<std::vector<float> > bounds_;
  std::vector<std::vector<float> > bounds_3mm_;
  std::vector<std::vector<float> > bounds_4mm_;

  // size of 2mm, 3mm, and 4mm volumes
  std::vector<int> size_;
  std::vector<int> size_3mm_;
  std::vector<int> size_4mm_;

  // Original sform (4mm)
  arma::mat sform_4mm() {
    return test_vol_.sform_mat();
  }

  arma::fmat invsform() {
    return arma::conv_to<arma::fmat>::from(test_vol_.sform_mat()).i();
  }

  arma::mat sform_3mm() {
    arma::mat xfm = arma::eye(4, 4);
    xfm(0, 0)     = -3;
    xfm(1, 1)     =  3;
    xfm(2, 2)     =  3;
    xfm(0, 3)     = bounds_3mm_[0][0];
    xfm(1, 3)     = bounds_3mm_[1][0];
    xfm(2, 3)     = bounds_3mm_[2][0];
    return xfm;
  }

  // Upsampled sform (2mm)
  arma::mat sform() {
    auto xdim     = fabs(test_vol_.xdim());
    arma::mat xfm = arma::eye(4, 4);
    xfm(0, 0)     = -xdim / 2.0;
    xfm(1, 1)     =  xdim / 2.0;
    xfm(2, 2)     =  xdim / 2.0;
    xfm(0, 3)     = bounds_[0][0];
    xfm(1, 3)     = bounds_[1][0];
    xfm(2, 3)     = bounds_[2][0];
    return xfm;
  }

  int num_sample_points()     { return sample_points_[0]    .size(); }
  int num_sample_points_3mm() { return sample_points_3mm_[0].size(); }
  int num_sample_points_4mm() { return sample_points_4mm_[0].size(); }
  int xsize()                 { return size_[0]; }
  int ysize()                 { return size_[1]; }
  int zsize()                 { return size_[2]; }
  int xsize_3mm()             { return size_3mm_[0]; }
  int ysize_3mm()             { return size_3mm_[1]; }
  int zsize_3mm()             { return size_3mm_[2]; }
  int xsize_4mm()             { return test_vol_.xsize(); }
  int ysize_4mm()             { return test_vol_.ysize(); }
  int zsize_4mm()             { return test_vol_.zsize(); }

  VolumeBSplineFixtureBase()
    : filename_(mmorf_data_dir() / "MNI152_T1_4mm.nii.gz")
  {

    NEWIMAGE::read_volume(test_vol_,filename_);
    test_vol_.setextrapolationmethod(EXTRAP);
    test_vol_.setinterpolationmethod(INTERP);

    // actual bounds in 4mm coordinate space,
    // and extended bounds in 2mm/3mm space,
    // in voxels
    bounds_4mm_ = {{ 0, static_cast<float>(test_vol_.xsize()) - 1},
                   { 0, static_cast<float>(test_vol_.ysize()) - 1},
                   { 0, static_cast<float>(test_vol_.zsize()) - 1}};
    bounds_     = {{-3, static_cast<float>(test_vol_.xsize()) + 2},
                   {-3, static_cast<float>(test_vol_.ysize()) + 2},
                   {-3, static_cast<float>(test_vol_.zsize()) + 2}};
    bounds_3mm_ = {{-3, static_cast<float>(test_vol_.xsize()) + 2},
                   {-3, static_cast<float>(test_vol_.ysize()) + 2},
                   {-3, static_cast<float>(test_vol_.zsize()) + 2}};

    // number of voxels in up-sampled 2mm volume
    size_ = {2 * (test_vol_.xsize() + 5) + 1,
             2 * (test_vol_.ysize() + 5) + 1,
             2 * (test_vol_.zsize() + 5) + 1};

    // convert bounds to mm
    auto xfm    = arma::conv_to<arma::fmat>::from(test_vol_.sform_mat());
    bounds_4mm_ = MMORF::WarpUtils::apply_affine_transform(bounds_4mm_, xfm);
    bounds_3mm_ = MMORF::WarpUtils::apply_affine_transform(bounds_3mm_, xfm);
    bounds_     = MMORF::WarpUtils::apply_affine_transform(bounds_,     xfm);

    // number of voxels in 3mm volume
    size_3mm_    = std::vector<int>(3);
    size_3mm_[0] = ceil(abs(bounds_3mm_[0][1] - bounds_3mm_[0][0]) / 3) + 1;
    size_3mm_[1] = ceil(abs(bounds_3mm_[1][1] - bounds_3mm_[1][0]) / 3) + 1;
    size_3mm_[2] = ceil(abs(bounds_3mm_[2][1] - bounds_3mm_[2][0]) / 3) + 1;

    // adjust 3mm bounds to fit volume
    bounds_3mm_[0][1] = bounds_3mm_[0][0] - (size_3mm_[0] - 1) * 3;
    bounds_3mm_[1][1] = bounds_3mm_[1][0] + (size_3mm_[1] - 1) * 3;
    bounds_3mm_[2][1] = bounds_3mm_[2][0] + (size_3mm_[2] - 1) * 3;

    // generate sample points
    std::vector<float> x_points, x_points_3mm, x_points_4mm;
    std::vector<float> y_points, y_points_3mm, y_points_4mm;
    std::vector<float> z_points, z_points_3mm, z_points_4mm;

    // x goes from high to low due
    // to L/R flip in MNI152 sform
    auto step_4mm = fabs(test_vol_.xdim());
    auto step     = step_4mm / 2;
    for (auto z = bounds_[2][0]; z <= bounds_[2][1]; z+=step){
    for (auto y = bounds_[1][0]; y <= bounds_[1][1]; y+=step){
    for (auto x = bounds_[0][0]; x >= bounds_[0][1]; x-=step){
      x_points.push_back(static_cast<float>(x));
      y_points.push_back(static_cast<float>(y));
      z_points.push_back(static_cast<float>(z));
    }}}
    for (auto z = bounds_4mm_[2][0]; z <= bounds_4mm_[2][1]; z+=step_4mm){
    for (auto y = bounds_4mm_[1][0]; y <= bounds_4mm_[1][1]; y+=step_4mm){
    for (auto x = bounds_4mm_[0][0]; x >= bounds_4mm_[0][1]; x-=step_4mm){
      x_points_4mm.push_back(static_cast<float>(x));
      y_points_4mm.push_back(static_cast<float>(y));
      z_points_4mm.push_back(static_cast<float>(z));
    }}}

    for (auto z = bounds_3mm_[2][0]; z <= bounds_3mm_[2][1]; z+=3){
    for (auto y = bounds_3mm_[1][0]; y <= bounds_3mm_[1][1]; y+=3){
    for (auto x = bounds_3mm_[0][0]; x >= bounds_3mm_[0][1]; x-=3){
      x_points_3mm.push_back(static_cast<float>(x));
      y_points_3mm.push_back(static_cast<float>(y));
      z_points_3mm.push_back(static_cast<float>(z));
    }}}
    sample_points_    .push_back(x_points);
    sample_points_    .push_back(y_points);
    sample_points_    .push_back(z_points);
    sample_points_4mm_.push_back(x_points_4mm);
    sample_points_4mm_.push_back(y_points_4mm);
    sample_points_4mm_.push_back(z_points_4mm);
    sample_points_3mm_.push_back(x_points_3mm);
    sample_points_3mm_.push_back(y_points_3mm);
    sample_points_3mm_.push_back(z_points_3mm);
  }
};

// A range of fixtures allowing each test to select the NEWIMAGE
// interpolation/extrapolation regimes. Most tests use "spline"
// (interp = cubic) and "zeropad" (clamp_border = false), as that
// is the default behaviour of the VolumeBSpline class
template class VolumeBSplineFixtureBase<NEWIMAGE::spline,    NEWIMAGE::zeropad>;
template class VolumeBSplineFixtureBase<NEWIMAGE::trilinear, NEWIMAGE::zeropad>;

using VolumeBSplineFixture       = VolumeBSplineFixtureBase<NEWIMAGE::spline,
                                                            NEWIMAGE::zeropad>;
using VolumeBSplineFixtureLinear = VolumeBSplineFixtureBase<NEWIMAGE::trilinear,
                                                            NEWIMAGE::zeropad>;


TEST_CASE_METHOD(VolumeBSplineFixture, "ctor_vecs_func_get_dimensions")
{
  auto dims_in = std::vector<int>{
    (int)test_vol_.xsize(),
    (int)test_vol_.ysize(),
    (int)test_vol_.zsize()};
  auto res_in = std::vector<float>{
    test_vol_.xdim(),
    test_vol_.ydim(),
    test_vol_.zdim()};
  auto origin_in = std::vector<float>{0, 0, 0};

  auto my_vol = MMORF::VolumeBSpline(dims_in,res_in,origin_in);
  auto dims_out = my_vol.get_dimensions();
  REQUIRE(test_vol_.xsize() == dims_out.at(0));
  REQUIRE(test_vol_.ysize() == dims_out.at(1));
  REQUIRE(test_vol_.zsize() == dims_out.at(2));
}

TEST_CASE_METHOD(VolumeBSplineFixture, "ctor_newimage_volume_func_get_dimensions")
{
  MMORF::VolumeBSpline my_vol(test_vol_);
  auto dims = my_vol.get_dimensions();
  REQUIRE(test_vol_.xsize() == dims.at(0));
  REQUIRE(test_vol_.ysize() == dims.at(1));
  REQUIRE(test_vol_.zsize() == dims.at(2));
}

TEST_CASE_METHOD(VolumeBSplineFixture, "ctor_string_func_get_dimensions")
{
  MMORF::VolumeBSpline my_vol(filename_);
  auto dims = my_vol.get_dimensions();
  REQUIRE(test_vol_.xsize() == dims.at(0));
  REQUIRE(test_vol_.ysize() == dims.at(1));
  REQUIRE(test_vol_.zsize() == dims.at(2));
}

TEST_CASE_METHOD(VolumeBSplineFixture, "ctor_newimage_volume_smoothed_func_get_dimensions")
{
  MMORF::VolumeBSpline my_vol(test_vol_, 10.0f);
  auto dims = my_vol.get_dimensions();
  REQUIRE(test_vol_.xsize() == dims.at(0));
  REQUIRE(test_vol_.ysize() == dims.at(1));
  REQUIRE(test_vol_.zsize() == dims.at(2));
}

TEST_CASE_METHOD(VolumeBSplineFixture, "ctor_string_smoothed_func_get_dimensions")
{
  MMORF::VolumeBSpline my_vol(filename_, 10.0f);
  auto dims = my_vol.get_dimensions();
  REQUIRE(test_vol_.xsize() == dims.at(0));
  REQUIRE(test_vol_.ysize() == dims.at(1));
  REQUIRE(test_vol_.zsize() == dims.at(2));
}


// sample() at 2mm resolution with cubic interpolation
TEST_CASE_METHOD(VolumeBSplineFixture, "ctor_string_func_sample_2mm")
{
  TempDir td;
  MMORF::VolumeBSpline my_vol(filename_);

  auto sampled_vol = my_vol.sample(sample_points_);

  std::string filename = Catch::getResultCapture().getCurrentTestName() + ".nii.gz";
  save_nifti(sampled_vol, xsize(), ysize(), zsize(), sform(), td.path() / filename);
  compare_niftis(td.path() / filename, mmorf_data_dir() / "VolumeBSpline" / filename);
}

// sample() at 3mm resolution with cubic interpolation
TEST_CASE_METHOD(VolumeBSplineFixture, "ctor_string_func_sample_3mm")
{
  TempDir td;
  MMORF::VolumeBSpline my_vol(filename_);

  auto sampled_vol = my_vol.sample(sample_points_3mm_);

  std::string filename = Catch::getResultCapture().getCurrentTestName() + ".nii.gz";
  save_nifti(sampled_vol, xsize_3mm(), ysize_3mm(), zsize_3mm(), sform_3mm(), filename);
  compare_niftis(td.path() / filename, mmorf_data_dir() / "VolumeBSpline" / filename);
}

// sample() at voxel centres with cubic interpolation
TEST_CASE_METHOD(VolumeBSplineFixture, "ctor_string_func_sample_4mm")
{
  TempDir td;
  MMORF::VolumeBSpline my_vol(filename_);

  auto sampled_vol = my_vol.sample(sample_points_4mm_);

  std::string filename = Catch::getResultCapture().getCurrentTestName() + ".nii.gz";
  save_nifti(sampled_vol, xsize_4mm(), ysize_4mm(), zsize_4mm(), sform_4mm(), filename);
  compare_niftis(td.path() / filename, mmorf_data_dir() / "VolumeBSpline" / filename);
}

// sample() at up-sampled resolution with linear interpolation
TEST_CASE_METHOD(VolumeBSplineFixtureLinear, "ctor_string_func_sample_linear_2mm")
{
  TempDir td;
  MMORF::VolumeBSpline my_vol(filename_, false, MMORF::VolumeBSpline::LINEAR);

  auto sampled_vol = my_vol.sample(sample_points_);

  std::string filename = Catch::getResultCapture().getCurrentTestName() + ".nii.gz";
  save_nifti(sampled_vol, xsize(), ysize(), zsize(), sform(), filename);
  compare_niftis(td.path() / filename, mmorf_data_dir() / "VolumeBSpline" / filename);
}

// sample() at up-sampled resolution with linear interpolation
TEST_CASE_METHOD(VolumeBSplineFixtureLinear, "ctor_string_func_sample_linear_3mm")
{
  TempDir td;
  MMORF::VolumeBSpline my_vol(filename_, false, MMORF::VolumeBSpline::LINEAR);

  auto sampled_vol = my_vol.sample(sample_points_3mm_);

  std::string filename = Catch::getResultCapture().getCurrentTestName() + ".nii.gz";
  save_nifti(sampled_vol, xsize_3mm(), ysize_3mm(), zsize_3mm(), sform_3mm(), filename);
  compare_niftis(td.path() / filename, mmorf_data_dir() / "VolumeBSpline" / filename);
}

// sample() at voxel centres with linear interpolation
TEST_CASE_METHOD(VolumeBSplineFixtureLinear, "ctor_string_func_sample_linear_4mm")
{
  TempDir td;
  MMORF::VolumeBSpline my_vol(filename_, false, MMORF::VolumeBSpline::LINEAR);

  auto sampled_vol = my_vol.sample(sample_points_4mm_);

  std::string filename = Catch::getResultCapture().getCurrentTestName() + ".nii.gz";
  save_nifti(sampled_vol, xsize_4mm(), ysize_4mm(), zsize_4mm(), sform_4mm(), filename);
  compare_niftis(td.path() / filename, mmorf_data_dir() / "VolumeBSpline" / filename);
}

// sample_gradient() at up-sampled resolution with cubic interpolation
TEST_CASE_METHOD(VolumeBSplineFixture, "ctor_string_func_sample_gradient_2mm")
{
  TempDir td;
  MMORF::VolumeBSpline my_vol(filename_);

  auto sampled_vol = my_vol.sample_gradient(sample_points_);

  std::string filename = Catch::getResultCapture().getCurrentTestName() + ".nii.gz";
  save_nifti(sampled_vol, xsize(), ysize(), zsize(), sform(), filename);
  compare_niftis(td.path() / filename, mmorf_data_dir() / "VolumeBSpline" / filename);
}


// sample_gradient() at up-sampled resolution with cubic interpolation
TEST_CASE_METHOD(VolumeBSplineFixture, "ctor_string_func_sample_gradient_3mm")
{
  TempDir td;
  MMORF::VolumeBSpline my_vol(filename_);

  auto sampled_vol = my_vol.sample_gradient(sample_points_3mm_);

  std::string filename = Catch::getResultCapture().getCurrentTestName() + ".nii.gz";
  save_nifti(sampled_vol, xsize_3mm(), ysize_3mm(), zsize_3mm(), sform_3mm(), filename);
  compare_niftis(td.path() / filename, mmorf_data_dir() / "VolumeBSpline" / filename);
}

// sample_gradient() at voxel centres with cubic interpolation
TEST_CASE_METHOD(VolumeBSplineFixture, "ctor_string_func_sample_gradient_4mm")
{
  TempDir td;
  MMORF::VolumeBSpline my_vol(filename_);

  auto sampled_vol = my_vol.sample_gradient(sample_points_4mm_);

  std::string filename = Catch::getResultCapture().getCurrentTestName() + ".nii.gz";
  save_nifti(sampled_vol, xsize_4mm(), ysize_4mm(), zsize_4mm(), sform_4mm(), filename);
  compare_niftis(td.path() / filename, mmorf_data_dir() / "VolumeBSpline" / filename);
}

// sample_gradient() at up-sampled resolution with linear interpolation
TEST_CASE_METHOD(VolumeBSplineFixtureLinear, "ctor_string_func_sample_gradient_linear_2mm")
{
  TempDir td;
  MMORF::VolumeBSpline my_vol(filename_, false, MMORF::VolumeBSpline::LINEAR);

  auto sampled_vol = my_vol.sample_gradient(sample_points_);

  std::string filename = Catch::getResultCapture().getCurrentTestName() + ".nii.gz";
  save_nifti(sampled_vol, xsize(), ysize(), zsize(), sform(), filename);
  compare_niftis(td.path() / filename, mmorf_data_dir() / "VolumeBSpline" / filename);
}

// sample_gradient() at up-sampled resolution with linear interpolation
TEST_CASE_METHOD(VolumeBSplineFixtureLinear, "ctor_string_func_sample_gradient_linear_3mm")
{
  TempDir td;
  MMORF::VolumeBSpline my_vol(filename_, false, MMORF::VolumeBSpline::LINEAR);

  auto sampled_vol = my_vol.sample_gradient(sample_points_3mm_);

  std::string filename = Catch::getResultCapture().getCurrentTestName() + ".nii.gz";
  save_nifti(sampled_vol, xsize_3mm(), ysize_3mm(), zsize_3mm(), sform_3mm(), filename);
  compare_niftis(td.path() / filename, mmorf_data_dir() / "VolumeBSpline" / filename);
}


// sample_gradient() at voxel centres with linear interpolation
TEST_CASE_METHOD(VolumeBSplineFixtureLinear, "ctor_string_func_sample_gradient_linear_4mm")
{
  TempDir td;
  MMORF::VolumeBSpline my_vol(filename_, false, MMORF::VolumeBSpline::LINEAR);

  auto sampled_vol = my_vol.sample_gradient(sample_points_4mm_);

  std::string filename = Catch::getResultCapture().getCurrentTestName() + ".nii.gz";
  save_nifti(sampled_vol, xsize_4mm(), ysize_4mm(), zsize_4mm(), sform_4mm(), filename);
  compare_niftis(td.path() / filename, mmorf_data_dir() / "VolumeBSpline" / filename);
}


// Check that we can create and use multiple volumes
// simultaneously (throwback to when there was shared
// global state via a CUDA texture<> reference)
TEST_CASE_METHOD(VolumeBSplineFixture, "multiple_vols")
{
  MMORF::VolumeBSpline my_vol_1(filename_);
  MMORF::VolumeBSpline my_vol_2(filename_);
  auto sampled_vol_1 = my_vol_1.sample(sample_points_);
  auto sampled_vol_2 = my_vol_2.sample(sample_points_);
  sampled_vol_1 = my_vol_1.sample(sample_points_);
  sampled_vol_2 = my_vol_2.sample(sample_points_);

  auto vec1 = arma::conv_to<arma::fvec>::from(sampled_vol_1);
  auto vec2 = arma::conv_to<arma::fvec>::from(sampled_vol_2);

  REQUIRE_ARMA(vec1, vec2, 1e-8);
}

TEST_CASE_METHOD(VolumeBSplineFixture, "ctor_string_func_set_coefficients")
{
  auto my_vol = MMORF::VolumeBSpline(filename_);
  auto dims = my_vol.get_dimensions();
  auto sampled_vol_1 = my_vol.sample(sample_points_);
  auto coef_sz = 1;
  for (const auto& dim : dims){
    coef_sz *= dim;
  }
  auto coeffs_new = std::vector<float>(coef_sz, 1.0);
  my_vol.set_coefficients(coeffs_new);
  auto sampled_vol_2 = my_vol.sample(sample_points_);
  REQUIRE((sampled_vol_1 != sampled_vol_2));
}

TEST_CASE_METHOD(VolumeBSplineFixture, "ctor_string_func_get_extents")
{
  auto my_vol = MMORF::VolumeBSpline(filename_);
  auto extents = my_vol.get_extents();

  // swap xhi/xlo due to MNI152 L/R flip
  std::pair<std::vector<float>, std::vector<float>> expect;
  expect.first  = {bounds_4mm_[0][1], bounds_4mm_[1][0], bounds_4mm_[2][0]};
  expect.second = {bounds_4mm_[0][0], bounds_4mm_[1][1], bounds_4mm_[2][1]};

  REQUIRE(extents.first  == expect.first);
  REQUIRE(extents.second == expect.second);
}

TEST_CASE_METHOD(VolumeBSplineFixture, "ctor_vecs_func_get_extents")
{
  auto dims_in = std::vector<int>{5,5,5};
  auto res_in = std::vector<float>{10,10,10};
  auto origin_in = std::vector<float>{2,2,2};
  auto my_vol = MMORF::VolumeBSpline(dims_in,res_in,origin_in);
  auto extents = my_vol.get_extents();
  for (auto i = 0; i< 3; ++i)
    {
      REQUIRE_APPROX(extents.first[i], -20.0f, 1e-6);
      REQUIRE_APPROX(extents.second[i], 20.0f, 1e-6);
    }
}

TEST_CASE_METHOD(VolumeBSplineFixture, "ctor_string_func_reparameterise")
{
  MMORF::VolumeBSpline my_vol(filename_);
  auto reparameterised_vol = my_vol.reparameterise(2);
  auto sampled_vol_1 = my_vol.sample(sample_points_);
  auto sampled_vol_2 = reparameterised_vol.sample(sample_points_);

  REQUIRE(sampled_vol_1.size() == sampled_vol_2.size());

  NEWIMAGE::volume<float> sv1(xsize(), ysize(), zsize());
  NEWIMAGE::volume<float> sv2(xsize(), ysize(), zsize());
  sv1.set_sform(4, sform());
  sv2.set_sform(4, sform());

  for (auto i = 0; i < sampled_vol_1.size(); ++i) {

    // skip boundary voxels
    auto x =  i %  xsize();
    auto y = (i /  xsize()) % ysize();
    auto z =  i / (xsize()  * ysize());
    if (x <= 6 || x >= (xsize() - 6) ||
        y <= 6 || y >= (ysize() - 6) ||
        z <= 6 || z >= (zsize() - 6)) {
      continue;
    }

    REQUIRE_NEAR(sampled_vol_1[i], sampled_vol_2[i], 10);
  }
}

TEST_CASE_METHOD(VolumeBSplineFixture, "ctor_vecs_func_get_resolution")
{
  auto dims_in = std::vector<int>{
    (int)test_vol_.xsize(),
    (int)test_vol_.ysize(),
    (int)test_vol_.zsize()};
  auto res_in = std::vector<float>{
    test_vol_.xdim(),
    test_vol_.ydim(),
    test_vol_.zdim()};
  auto origin_in = std::vector<float>{0, 0, 0};
  auto my_vol = MMORF::VolumeBSpline(dims_in,res_in,origin_in);
  auto res_out = my_vol.get_resolution();
  REQUIRE(test_vol_.xdim() == res_out.at(0));
  REQUIRE(test_vol_.ydim() == res_out.at(1));
  REQUIRE(test_vol_.zdim() == res_out.at(2));
}

TEST_CASE_METHOD(VolumeBSplineFixture, "ctor_string_func_get_original_sample_positions")
{
  auto my_vol = MMORF::VolumeBSpline(filename_);
  auto test_points = my_vol.get_original_sample_positions();
  auto true_samples = my_vol.sample(sample_points_4mm_);
  auto test_samples = my_vol.sample(test_points);
  REQUIRE(test_points == sample_points_4mm_);
  REQUIRE(test_samples == true_samples);
}

TEST_CASE_METHOD(VolumeBSplineFixture, "ctor_string_func_crop")
{
  auto my_vol = MMORF::VolumeBSpline(filename_);
  auto old_dimensions = my_vol.get_dimensions();

  // bounds give bounding box of volume (i.e. voxel edges)
  // but volume extents are aligned with voxel centres, so
  // we offset by 2 (as voxels are 4mm iso)
  float xlo, ylo, zlo, xhi, yhi, zhi;
  xlo = bounds_4mm_[0][1] + 2;
  ylo = bounds_4mm_[1][0] + 2;
  zlo = bounds_4mm_[2][0] + 2;
  xhi = bounds_4mm_[0][0] - 2;
  yhi = bounds_4mm_[1][1] - 2;
  zhi = bounds_4mm_[2][1] - 2;

  // should not change dimensions
  auto extents_big = std::make_pair(std::vector<float>({xlo - 64, ylo - 64, zlo - 64}),
                                    std::vector<float>({xhi + 64, yhi + 64, zhi + 64}));
  auto cropped_vol = my_vol.crop(extents_big);
  auto new_dimensions = cropped_vol.get_dimensions();
  REQUIRE(old_dimensions == new_dimensions);

  // should not change dimensions
  auto extents_exact = std::make_pair(std::vector<float>({xlo, ylo, zlo}),
                                      std::vector<float>({xhi, yhi, zhi}));
  cropped_vol = my_vol.crop(extents_exact);
  new_dimensions = cropped_vol.get_dimensions();
  REQUIRE(old_dimensions == new_dimensions);

  // original image is 4mm, so expected
  // here is -4 voxels
  auto extents_less = std::make_pair(std::vector<float>({xlo + 10, ylo + 10, zlo + 10}),
                                     std::vector<float>({xhi - 10, yhi - 10, zhi - 10}));
  my_vol.crop(extents_less);
  auto expected_dimensions = old_dimensions;
  for (auto& dimension : expected_dimensions) dimension = dimension - 4;
  cropped_vol = my_vol.crop(extents_less);
  new_dimensions = cropped_vol.get_dimensions();
  REQUIRE(expected_dimensions == new_dimensions);
}

} // namespace test_VolumeBSpline
