// C++ wrapper around core CubicInterpolationCUDA routines.

#include <ctime>
#include <string>
#include <sstream>
#include <sys/time.h>
#include <assert.h>
#include <vector>
#include <internal/cutil_math_bugfixes.h>

#include "CubicInterp.cuh"

#include "cuda_runtime.h"

namespace CubicInterpCUDA {

#include <memcpy.cu>
#include <cubicPrefilter3D.cu>
#include <cubicTex3D.cu>

template void CubicBSplinePrefilter3D(float* volume, uint pitch, uint width, uint height, uint depth);

template __device__ float cubicTex3D<float, float>(cudaTextureObject_t tex, float3 coord);
template __device__ float cubicTex3D_1st_derivative_x<float, float>(cudaTextureObject_t tex, float3 coord);
template __device__ float cubicTex3D_1st_derivative_y<float, float>(cudaTextureObject_t tex, float3 coord);
template __device__ float cubicTex3D_1st_derivative_z<float, float>(cudaTextureObject_t tex, float3 coord);
}
