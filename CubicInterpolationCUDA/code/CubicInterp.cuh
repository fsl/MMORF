// C++ wrapper around core CubicInterpolationCUDA routines.

#ifndef CUBIC_INTERP_CUH
#define CUBIC_INTERP_CUH

#include "cuda_runtime.h"

namespace CubicInterpCUDA {

cudaPitchedPtr CopyVolumeHostToDevice(const float* host, uint width, uint height, uint depth);

void CopyVolumeDeviceToHost(float* host, const cudaPitchedPtr device, uint width, uint height, uint depth);

template<class floatN>
void CubicBSplinePrefilter3D(floatN* volume, uint pitch, uint width, uint height, uint depth);

template<typename dtype, typename floatN>
__device__ floatN cubicTex3D(cudaTextureObject_t tex, float3 coord);
template<typename dtype, typename floatN>
__device__ floatN cubicTex3D_1st_derivative_x(cudaTextureObject_t tex, float3 coord);
template<typename dtype, typename floatN>
__device__ floatN cubicTex3D_1st_derivative_y(cudaTextureObject_t tex, float3 coord);
template<typename dtype, typename floatN>
__device__ floatN cubicTex3D_1st_derivative_z(cudaTextureObject_t tex, float3 coord);

}

#endif // CUBIC_INTERP_CUH
