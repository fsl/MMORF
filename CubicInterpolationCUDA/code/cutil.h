#ifndef _CUTIL_MINI_H_
#define _CUTIL_MINI_H_

#include <assert.h>
#include <vector>

static inline bool CUT_DEVICE_INIT(int argc, char** argv)
{
	cudaError_t cudaStatus = cudaSetDevice(0);
    if (cudaStatus != cudaSuccess)
	{
        fprintf(stderr, "cudaSetDevice failed!  Do you have a CUDA-capable GPU installed?");
        return false;
    }

	return true;
}

static inline void CUDA_SAFE_CALL(cudaError_t err)
{
	assert(err == cudaSuccess);
}

enum CUTBoolean
{
    CUTFalse = 0,
    CUTTrue = 1
};

static inline void CUT_SAFE_CALL(CUTBoolean result)
{
	assert(result == CUTTrue);
}

static inline void CUT_CHECK_ERROR(const char* errorMessage)
{
	cudaError_t err = cudaGetLastError();
    if (cudaSuccess != err)
	{
        fprintf(stderr, "Cuda error: %s: %s.\n", errorMessage, cudaGetErrorString(err) );
    }
}

#endif //_CUTIL_MINI_H_
