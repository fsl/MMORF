# Makefile for MMORF

# As we are using FSL classes, this line is needed
include ${FSLCONFDIR}/default.mk

PROJNAME = mmorf

BUILD_DIR = build
TEST_DIR  = test
INC_DIR   = inc
SRC_DIR   = src

# MMORF provides these executables. All binary
# files are directed into a sub-directory called
# "build".
XFILES = ${BUILD_DIR}/mmorf ${BUILD_DIR}/tensor_average

# Danny Ruijters' Cubic Interpolation (CI) directory
CI_DIR     = CubicInterpolationCUDA
CI_INC_DIR = CubicInterpolationCUDA/code
CI_LIB     = ${BUILD_DIR}/libCubicInterp.a

USRINCFLAGS  = -I$(INC_DIR) -I${CI_INC_DIR}
USRCPPFLAGS  = -DARMA_DONT_PRINT_ERRORS -DEXPOSE_TREACHEROUS
USRCXXFLAGS  = -fopenmp
USRNVCCFLAGS = --relocatable-device-code true
TESTCPPFLAGS =  -DMMORF_TEST_DIRECTORY=\"$(realpath ${TEST_DIR})\"
LIBS         = -lfsl-newmeshclass -lfsl-basisfield -lfsl-newimage -lfsl-miscmaths \
               -lfsl-NewNifti -lfsl-utils -lfsl-znz -lgomp
CUDALIBS     = -lcurand

OBJS = BiasFieldBSpline.o \
       Config.o \
       CostFxnBendingEnergy.o \
       CostFxnCompoundVarianceScaled.o\
       CostFxnDefinitions.o \
       CostFxnLogJacobianSingularValuesUtils.o \
       CostFxnLogJacobianSingularValues.o \
       CostFxnPointCloudWarpField.o \
       CostFxnSplineUtils.o \
       CostFxnSSDBiasFieldMaskedExcluded.o \
       CostFxnSSDWarpFieldSymmetricMaskedExcludedUtils.o \
       CostFxnSSDWarpFieldSymmetricMaskedExcluded.o \
       CostFxnTensorL2WarpFieldSymmetricMaskedExcludedUtils.o \
       CostFxnTensorL2WarpFieldSymmetricMaskedExcluded.o \
       IntensityMapperPolynomial.o \
       LinearSolverCG.o \
       OptimiserLevenberg.o \
       OptimiserLevenbergMarquardt.o \
       OptimiserScaledConjugateGradient.o \
       RegistrationCoordinatorMultimodal.o \
       SparseDiagonalMatrixTiled.o \
       TextureHandle.o \
       VolumeBSpline.o \
       VolumeTensor.o \
       WarpFieldBSpline.o \
       WarpUtils.o

OBJS := $(OBJS:%.o=${BUILD_DIR}/%.o)

TESTOBJS  = $(patsubst ${TEST_DIR}/%.cpp,${TEST_DIR}/%.o,$(wildcard ${TEST_DIR}/*.cpp))
TESTOBJS += $(patsubst ${TEST_DIR}/%.cu, ${TEST_DIR}/%.o,$(wildcard ${TEST_DIR}/*.cu))

all: $(XFILES)

clean:
	make -C ${CI_DIR} clean
	rm -rf ${BUILD_DIR} depend.mk ${TEST_DIR}/*.o  ${TEST_DIR}/run_tests

${CI_LIB}:
	@mkdir -p ${BUILD_DIR}
	${MAKE} -C ${CI_DIR}
	cp ${CI_DIR}/libCubicInterp.a $@

${BUILD_DIR}/%.o: $(SRC_DIR)/%.cu
	@mkdir -p ${BUILD_DIR}
	${NVCC} ${NVCCFLAGS} -c -o $@ $<

${BUILD_DIR}/%.o: $(SRC_DIR)/%.cpp
	@mkdir -p ${BUILD_DIR}
	$(CXX) $(CXXFLAGS) -c -o $@ $<

${BUILD_DIR}/tensor_average: ${BUILD_DIR}/tensor_average.o \
                             ${BUILD_DIR}/tensor_fxns.o
	$(CXX) $(CXXFLAGS) -o $@ $^ ${LDFLAGS}

${BUILD_DIR}/mmorf: ${BUILD_DIR}/mmorf.o ${OBJS} ${CI_LIB}
	${NVCC} ${NVCCFLAGS} -o $@ $^ ${NVCCLDFLAGS}

${TEST_DIR}/%.o: $(TEST_DIR)/%.cu
	${NVCC} ${NVCCFLAGS} ${TESTCPPFLAGS} -c -o $@ $<

${TEST_DIR}/%.o: $(TEST_DIR)/%.cpp
	$(CXX) $(CXXFLAGS) ${TESTCPPFLAGS} -c -o $@ $<

tests: ${OBJS} ${TESTOBJS} ${CI_LIB}
	${NVCC} ${NVCCFLAGS} -o test/run_tests $^ ${NVCCLDFLAGS}

# Print a varable name with "make print-VARIABLE"
print-%  : ; @echo $* = $($*)
