/// MMORF Config class, for storage of/access to global configuration settings.
#include "Config.h"

namespace MMORF {

  const Config& Config::getInstance(
    bool         debug,
    unsigned int nthreads)
  {
    static Config instance(debug, nthreads);
    return instance;
  }

  bool         Config::debug()    const {return debug_;}
  unsigned int Config::nthreads() const {return nthreads_;}

  Config::Config(
    bool         debug,
    unsigned int nthreads)
    : debug_(debug)
    , nthreads_(nthreads)
  {}
}
