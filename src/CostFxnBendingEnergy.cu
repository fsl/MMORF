//////////////////////////////////////////////////////////////////////////////////////////////
/// \file
/// \brief Cost function based on the bending energy of a warp / bias field.
/// \details The field is defined by b-spline field coefficients, one set per dimension in the
///          volume
/// \author Frederik Lange
/// \date April 2018
/// \copyright Copyright (C) 2018 University of Oxford
//////////////////////////////////////////////////////////////////////////////////////////////
#include "CostFxnBendingEnergy.h"
#include "CostFxnSplineUtils.h"
#include "VolumeBSpline.h"
#include "WarpFieldBSpline.h"
#include "BiasFieldBSpline.h"
#include "SparseDiagonalMatrixTiled.h"
#include "CostFxnHelpers.cuh"
#include "CostFxnKernels.cuh"
#include "helper_cuda.cuh"

#include "basisfield/fsl_splines.h"

#include <armadillo>

#include <thrust/device_vector.h>
#include <thrust/host_vector.h>

#include <memory>
#include <vector>
#include <utility>
#include <cmath>
#include <limits>
#include <algorithm>
#include <numeric>

namespace MMORF
{
////////////////////////////////////////////////////////////////////////////////
// PIMPL Class
////////////////////////////////////////////////////////////////////////////////
  template<typename Field>
  class CostFxnBendingEnergy<Field>::Impl
  {
    public:
      /// Construct by passing in a fully constructed warpfield
      Impl(
          std::shared_ptr<Field> field,
          int sampling_frequency)
        : field_(field)
        , sampling_frequency_(sampling_frequency)
        , spline_1D_(3,sampling_frequency)
        , bkk_(field_->get_parameter_size().second,
               field_->get_parameter_size().second)
        , sparse_bkk_(0,0,0,std::vector<int>(0))
        , norm_factor_(1.0f)
      {
        // Calculate sample resolution
        auto field_dims = field_->get_dimensions();
        auto field_extents = field_->get_extents();
        for (auto i = 0; i < field_dims.size(); ++i){
          auto dim_res =
            field_->get_knot_spacing() / static_cast<float>(sampling_frequency);
          sample_resolution_.push_back(dim_res);
        }
        // Make bkk matrix
        make_bkk_();
        // Calculate warp sample resolution
        for (auto dim :field_dims){
          norm_factor_ /= (dim*sampling_frequency);
        }
      }
      /// Get the current value of the parameters
      std::vector<std::vector<float> > get_parameters() const
      {
        return field_->get_parameters();
      }
      /// Set the current value of the parameters
      void set_parameters(
          const std::vector<std::vector<float> >& parameters)
      {
        field_->set_parameters(parameters);
      }

      /// Get hess under current parameterisation
      MMORF::SparseDiagonalMatrixTiled hess() const
      {
        auto hess = sparse_bkk_;
        hess *= (2*norm_factor_);
        return hess;
      }

      // Separate cost() / grad() implementations for WarpFieldBSpline and
      // BiasFieldBSpline are provided below this class definition.
      float cost() const;
      arma::fvec grad() const;

    private:
      /// Calculate the large matrix central to bending energy calculations.
      /// Separate implementations for make_bkk_() are provided below the
      /// class definition.
      void make_bkk_();
      void make_bkk_(int ndims)
      {
        // Create sparse tiled matrix for storing Bkk
        sparse_bkk_ = create_empty_bkk_(field_->get_dimensions());
        for (auto dim_0 = 0; dim_0 < ndims; ++dim_0){
          for (auto dim_1 = 0; dim_1 < ndims; ++dim_1){
            // Check if this is a unique combination - we will keep the main diagonal and below
            if (dim_1 > dim_0) continue;
            // Generate differentiated splines
            auto x_deriv_order = 0;
            auto y_deriv_order = 0;
            auto z_deriv_order = 0;
            x_deriv_order = dim_0 == 0 ? x_deriv_order + 1 : x_deriv_order;
            x_deriv_order = dim_1 == 0 ? x_deriv_order + 1 : x_deriv_order;
            y_deriv_order = dim_0 == 1 ? y_deriv_order + 1 : y_deriv_order;
            y_deriv_order = dim_1 == 1 ? y_deriv_order + 1 : y_deriv_order;
            z_deriv_order = dim_0 == 2 ? z_deriv_order + 1 : z_deriv_order;
            z_deriv_order = dim_1 == 2 ? z_deriv_order + 1 : z_deriv_order;
            auto spline_x = BASISFIELD::Spline1D<float>(
                spline_1D_.Order(),
                spline_1D_.KnotSpacing(),
                x_deriv_order);
            auto spline_y = BASISFIELD::Spline1D<float>(
                spline_1D_.Order(),
                spline_1D_.KnotSpacing(),
                y_deriv_order);
            auto spline_z = BASISFIELD::Spline1D<float>(
                spline_1D_.Order(),
                spline_1D_.KnotSpacing(),
                z_deriv_order);
            auto spline_x_vals = CostFxnSplineUtils::spline_as_vec(
                spline_x,
                sample_resolution_[0],
                x_deriv_order);
            auto spline_y_vals = CostFxnSplineUtils::spline_as_vec(
                spline_y,
                sample_resolution_[1],
                y_deriv_order);
            auto spline_z_vals = CostFxnSplineUtils::spline_as_vec(
                spline_z,
                sample_resolution_[2],
                z_deriv_order);
            // Call kernel
            // Is this a unique sub-bkk? (i.e. same dimension differentiated)
            // Otherwise, we need to add this sub-bkk twice (once for dim_0 > dim_1, and once
            // for dim_0 < dim_1 which give the same result)
            calculate_sub_bkk_(
              // Input
              spline_x_vals,
              spline_y_vals,
              spline_z_vals,
              field_->get_dimensions(),
              std::vector<int>(3, static_cast<int>(spline_1D_.KnotSpacing())),
              dim_0 == dim_1,
              sparse_bkk_);
          }
        }
      }

      // Create an empty Bkk matrix with the correct sparsity pattern.
      // Separate implementations for create_empty_bkk_(coef_sz) are
      // provided below the class definition.
      MMORF::SparseDiagonalMatrixTiled create_empty_bkk_(
        const std::vector<int>& coef_sz) const;
      MMORF::SparseDiagonalMatrixTiled create_empty_bkk_(
        const std::vector<int>& coef_sz, unsigned int ntiles) const
      {
        auto offsets = CostFxnSplineUtils::calculate_sparse_diag_offsets(coef_sz);
        unsigned int max_diagonal = coef_sz[0]*coef_sz[1]*coef_sz[2];
        MMORF::SparseDiagonalMatrixTiled r_matrix(max_diagonal, max_diagonal, ntiles, offsets);
        return r_matrix;
      }
      // Calclulate sub-matrix of Bkk for symmetrical splines
      void calculate_sub_bkk_(
          const std::vector<float>& spline_x,
          const std::vector<float>& spline_y,
          const std::vector<float>& spline_z,
          const std::vector<int>& coef_sz,
          const std::vector<int>& ksp,
          const bool dims_equal,
          MMORF::SparseDiagonalMatrixTiled& sparse_bkk) const
      {
        // Make everything a device vector using thrust::
        auto sparse_bkk_offsets_dev = thrust::device_vector<int>(sparse_bkk.get_offsets());
        auto spline_x_dev = thrust::device_vector<float>(spline_x);
        auto spline_y_dev = thrust::device_vector<float>(spline_y);
        auto spline_z_dev = thrust::device_vector<float>(spline_z);
        // Get all the raw pointers ready for the Kernel
        float *spline_x_raw = thrust::raw_pointer_cast(spline_x_dev.data());
        float *spline_y_raw = thrust::raw_pointer_cast(spline_y_dev.data());
        float *spline_z_raw = thrust::raw_pointer_cast(spline_z_dev.data());
        int *sparse_bkk_offsets_raw = thrust::raw_pointer_cast(
            sparse_bkk_offsets_dev.data());
        float *sparse_bkk_raw = sparse_bkk.get_raw_pointer(0,0);
        // Calculate parameters for running kernel
        int min_grid_size;
        int threads;
        cudaOccupancyMaxPotentialBlockSize(
            &min_grid_size,
            &threads,
            MMORF::kernel_make_bkk,
            0,
            0);
        unsigned int blocks = 172;
        unsigned int chunks = static_cast<unsigned int>(
            std::ceil(float(coef_sz.at(0)*coef_sz.at(1)*coef_sz.at(2))/float(threads)));
        dim3 blocks_2d(blocks,chunks);
        unsigned int smem = (spline_x.size()+spline_y.size()+spline_z.size())*sizeof(float);
        // Call CUDA Kernel
      MMORF::kernel_make_bkk<<<blocks_2d,threads,smem>>>(
            // Input
            spline_x_raw,
            spline_y_raw,
            spline_z_raw,
            ksp.at(0),
            ksp.at(1),
            ksp.at(2),
            coef_sz.at(0),
            coef_sz.at(1),
            coef_sz.at(2),
            sparse_bkk_offsets_raw,
            dims_equal,

            // Output
            sparse_bkk_raw);
        /// \todo Add error checking to cudaDeviceSynchronize
        checkCudaErrors(cudaDeviceSynchronize());
      }
      // Private datamembers
      std::shared_ptr<Field>           field_;
      BASISFIELD::Spline1D<float>      spline_1D_;
      int                              sampling_frequency_;
      arma::sp_fmat                    bkk_;
      MMORF::SparseDiagonalMatrixTiled sparse_bkk_;
      float                            norm_factor_;
      std::vector<float>               sample_resolution_;
  };

  /// CostFxn.cost() implementation for WarpFieldBSpline
  template <> float CostFxnBendingEnergy<WarpFieldBSpline>::Impl::cost() const {
    // Calculate the values of the local Jacobian at all sample points
    auto sample_positions = field_->get_robust_sample_positions(
      sampling_frequency_);
    // Check to make sure that the field hasn't gone non-diffeomorphic. If any Jacobian
    // determinant is negative, return the cost as infinity
    auto jacobian_determinants = field_->jacobian_determinants(sample_positions);
    auto min_element_it = std::min_element(
      jacobian_determinants.begin(),
      jacobian_determinants.end());
    std::cout << "Minimum Jacobian is: " << *min_element_it << std::endl;
    if (*min_element_it < 0.0f){
      return std::numeric_limits<float>::infinity();
    }
    // If we're sure everything is still diffeomorphic, then calculate the cost
    else{
      auto col_param = arma::fvec();
      // Concatenate warp parameters
      for (const auto& warp_params : field_->get_parameters()){
        col_param = arma::join_cols(col_param,arma::fvec(warp_params));
      }
      arma::fvec rhs_mult = bkk_*col_param;
      float lhs_mult = arma::dot(col_param,rhs_mult);
      auto cost = lhs_mult*norm_factor_;
      return cost;
    }
  }

  /// CostFxn.cost() implementation for BiasFieldBSpline
  template <> float CostFxnBendingEnergy<BiasFieldBSpline>::Impl::cost() const
  {
    auto col_param = arma::fvec(field_->get_parameters()[0]);
    col_param = col_param - 1.0f;
    //arma::fvec rhs_mult = bkk_*col_param;
    //float lhs_mult = arma::dot(col_param,rhs_mult);
    //auto cost = lhs_mult*norm_factor_;
    float cost = norm_factor_*arma::as_scalar(col_param.t()*bkk_*col_param);
    return cost;
  }

  // CostFxn.grad() implementation for WarpFieldBSpline
  template <> arma::fvec CostFxnBendingEnergy<WarpFieldBSpline>::Impl::grad() const
  {
    auto col_param = arma::fvec();
    // Concatenate warp parameters
    for (const auto& warp_params : field_->get_parameters()){
      col_param = arma::join_cols(col_param,arma::fvec(warp_params));
    }
    arma::fvec grad = 2*bkk_*col_param*norm_factor_;
    return grad;
  }

  // CostFxn.grad() implementation for BiasFieldBSpline
  template <> arma::fvec CostFxnBendingEnergy<BiasFieldBSpline>::Impl::grad() const
  {
    auto col_param = arma::fvec(field_->get_parameters()[0]);
    col_param = col_param - 1.0f;
    arma::fvec grad = 2*norm_factor_*bkk_*col_param;
    return grad;
  }

  // Impl.create_empty_bkk_ implementation for WarpFieldBSpline
  template <> MMORF::SparseDiagonalMatrixTiled
  CostFxnBendingEnergy<WarpFieldBSpline>::Impl::create_empty_bkk_(
    const std::vector<int>& coef_sz) const
  {
    return create_empty_bkk_(coef_sz, 3);
  }

  // Impl.create_empty_bkk_ implementation for BiasFieldBSpline
  template <> MMORF::SparseDiagonalMatrixTiled
  CostFxnBendingEnergy<BiasFieldBSpline>::Impl::create_empty_bkk_(
    const std::vector<int>& coef_sz) const
  {
    return create_empty_bkk_(coef_sz, 1);
  }

  // Impl.make_bkk_ implementation for WarpFieldBSpline
  template <> void CostFxnBendingEnergy<WarpFieldBSpline>::Impl::make_bkk_()
  {
    // Loop over all field directions, assuming this corresponds
    // to dimensionality of the warped volume
    make_bkk_(field_->get_parameter_size().first);
    // Now copy the sub-matrix within the sparse tiled matrix
    sparse_bkk_.copy_submatrix(0,0,1,1);
    sparse_bkk_.copy_submatrix(0,0,2,2);
    // Create armadillo matrix
    bkk_ = sparse_bkk_.convert_to_csc();
  }

  // Impl.make_bkk_ implementation for BiasFieldBSpline
  template <> void CostFxnBendingEnergy<BiasFieldBSpline>::Impl::make_bkk_()
  {
    // Assume this is a 3D only volume
    make_bkk_(3);
    // Create armadillo matrix
    bkk_ = sparse_bkk_.convert_to_csc();
  }

////////////////////////////////////////////////////////////////////////////////
// Main Class
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Rule of 5
////////////////////////////////////////////////////////////////////////////////
  /// Default dtor
  template<typename Field>
  CostFxnBendingEnergy<Field>::~CostFxnBendingEnergy() = default;
  /// Move ctor
  template<typename Field>
  CostFxnBendingEnergy<Field>::CostFxnBendingEnergy(CostFxnBendingEnergy<Field>&& rhs) = default;
  /// Move assignment operator
  template<typename Field>
  CostFxnBendingEnergy<Field>& CostFxnBendingEnergy<Field>::operator=(CostFxnBendingEnergy<Field>&& rhs) = default;
  /// Copy ctor
  template<typename Field>
  CostFxnBendingEnergy<Field>::CostFxnBendingEnergy(const CostFxnBendingEnergy<Field>& rhs)
    : pimpl_(nullptr)
  {
    if (rhs.pimpl_){
      pimpl_ = std::make_unique<Impl>(*rhs.pimpl_);
    }
  }
  /// Copy assignment operator
  template<typename Field>
  CostFxnBendingEnergy<Field>& CostFxnBendingEnergy<Field>::operator=(const CostFxnBendingEnergy<Field>& rhs)
  {
    if (!rhs.pimpl_){
      pimpl_.reset();
    }
    else if (!pimpl_){
      pimpl_ = std::make_unique<Impl>(*rhs.pimpl_);
    }
    else{
      *pimpl_ = *rhs.pimpl_;
    }
    return *this;
  }
////////////////////////////////////////////////////////////////////////////////
// Class Specific Functions
////////////////////////////////////////////////////////////////////////////////
  /// Construct by passing in a fully constructed warpfield
  template<typename Field>
  CostFxnBendingEnergy<Field>::CostFxnBendingEnergy(
      std::shared_ptr<Field> field,
      int sampling_frequency)
    : pimpl_(std::make_unique<Impl>(field, sampling_frequency))
  {}
  /// Get the current value of the parameters
  template<typename Field>
  std::vector<std::vector<float> > CostFxnBendingEnergy<Field>::get_parameters() const
  {
    return pimpl_->get_parameters();
  }
  /// Set the current value of the parameters
  template<typename Field>
  void CostFxnBendingEnergy<Field>::set_parameters(
      const std::vector<std::vector<float> >& parameters)
  {
    pimpl_->set_parameters(parameters);
  }
  /// Get cost under current parameterisation
  template<typename Field>
  float CostFxnBendingEnergy<Field>::cost() const
  {
    return pimpl_->cost();
  }
  /// Get Jte under current parameterisation
  template<typename Field>
  arma::fvec CostFxnBendingEnergy<Field>::grad() const
  {
    return pimpl_->grad();
  }
  /// Get JtJ under current parameterisation
  template<typename Field>
  MMORF::SparseDiagonalMatrixTiled CostFxnBendingEnergy<Field>::hess() const
  {
    return pimpl_->hess();
  }

  // Template specialisations for WarpFieldBSpline and BiasFieldBSpline
  template class CostFxnBendingEnergy<WarpFieldBSpline>;
  template class CostFxnBendingEnergy<BiasFieldBSpline>;
} // MMORF
