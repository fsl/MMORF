//////////////////////////////////////////////////////////////////////////////////////////////
/// \file CostFxnLogJacobianSingularValuesUtils.cuh
/// \brief Utility functions used by jacobian-based CostFxns.
/// \details
/// \author Frederik Lange
/// \date February 2024
/// \copyright Copyright (C) 2018 University of Oxford
//////////////////////////////////////////////////////////////////////////////////////////////


#include "CostFxnLogJacobianSingularValuesUtils.h"
#include "CostFxnSplineUtils.h"
#include "CostFxnHelpers.cuh"
#include "CostFxnKernels.cuh"
#include "CostFxn.h"

#include "helper_cuda.cuh"

#include "WarpFieldBSpline.h"
#include "basisfield/fsl_splines.h"

#include <thrust/host_vector.h>
#include <thrust/device_vector.h>

#include <armadillo>

#include <memory>
#include <vector>


namespace MMORF {

  namespace CostFxnLogJacobianSingularValuesUtils {

    /// Get cost under current parameterisation
    /// \details The cost in this case is equal to the sum of the square of the log of the
    ///          singular values at each point in the volume
    float cost(
        const std::shared_ptr<MMORF::WarpFieldBSpline> warp_field,
        const int                                      sampling_frequency,
        const float                                    norm_factor)
    {
      auto cost_out = 0.0f;
      // Calculate the values of the local Jacobian at all sample points
      auto sample_positions = warp_field->get_robust_sample_positions(
          sampling_frequency);
      // Check to make sure that the field hasn't gone non-diffeomorphic. If any Jacobian
      // determinant is negative, return the cost as infinity
      auto jacobian_determinants = warp_field->jacobian_determinants(
          sample_positions);
      auto min_element_it = std::min_element(
          jacobian_determinants.begin(),
          jacobian_determinants.end());
      std::cout << "Minimum Jacobian is: " << *min_element_it << std::endl;
      if (*min_element_it < 1e-3f || !std::isfinite(*min_element_it)){
        cost_out= std::numeric_limits<float>::infinity();
      }
      // If we're sure everything is still diffeomorphic, then calculate the cost
      else{
        auto jacobian_elements = warp_field->get_jacobian_elements(sample_positions);
        auto cost_per_sample = calculate_cost_per_sample(jacobian_elements);
        auto cost = std::accumulate(cost_per_sample.begin(), cost_per_sample.end(), 0.0f);
        cost_out = norm_factor*cost;
      }
      return cost_out;
    }

    /// Get grad under current parameterisation
    arma::fvec grad(
        const std::shared_ptr<MMORF::WarpFieldBSpline> warp_field,
        const BASISFIELD::Spline1D<float>&             spline_1D,
        const int                                      sampling_frequency,
        const std::vector<float>&                      sample_resolution,
        const float                                    norm_factor)
    {
      // Calculate grad per jacobian element
      auto sample_positions = warp_field->get_robust_sample_positions(
          sampling_frequency);
      auto jacobian_elements = warp_field->get_jacobian_elements(sample_positions);
      auto grad_per_jacobian_element = calculate_grad_per_jacobian_element(
          jacobian_elements);
      // Calculate grad per spline
      auto grad_per_spline = calculate_grad_per_spline(
          warp_field,
          spline_1D,
          sampling_frequency,
          sample_resolution,
          grad_per_jacobian_element);
      return norm_factor*grad_per_spline;
    }

    /// Get hess under current parameterisation
    MMORF::SparseDiagonalMatrixTiled hess(
        const std::shared_ptr<MMORF::WarpFieldBSpline> warp_field,
        const BASISFIELD::Spline1D<float>&             spline_1D,
        const int                                      sampling_frequency,
        const std::vector<float>&                      sample_resolution,
        float                                          norm_factor,
        const MMORF::CostFxn::HessType                 hess_type)
    {
        // Calculate cost & grad per jacobian element
        auto sample_positions = warp_field->get_robust_sample_positions(
            sampling_frequency);
        auto jacobian_elements = warp_field->get_jacobian_elements(sample_positions);
        auto cost_per_sample = calculate_cost_per_sample(jacobian_elements);
        auto grad_per_jacobian_element = calculate_grad_per_jacobian_element(
            jacobian_elements);

        auto calc_hess_func =
          hess_type == MMORF::CostFxn::HESS_FULL
          ? calculate_hess_per_spline
          : calculate_hess_per_spline_diag_hess;

        auto hess_per_spline = calc_hess_func(
            cost_per_sample,
            grad_per_jacobian_element,
            warp_field,
            spline_1D,
            sampling_frequency,
            sample_resolution);
        hess_per_spline *= norm_factor;

        return hess_per_spline;
    }


    /// Calculate the contribution to the cost per sample position
    std::vector<float> calculate_cost_per_sample(
        const std::vector<std::vector<float> >& jacobian_elements)
    {
      auto n_samples = jacobian_elements[0].size();
      // Create device vectors using thrust::
      auto j11_dev = thrust::device_vector<float>(jacobian_elements[0]);
      auto j12_dev = thrust::device_vector<float>(jacobian_elements[1]);
      auto j13_dev = thrust::device_vector<float>(jacobian_elements[2]);
      auto j21_dev = thrust::device_vector<float>(jacobian_elements[3]);
      auto j22_dev = thrust::device_vector<float>(jacobian_elements[4]);
      auto j23_dev = thrust::device_vector<float>(jacobian_elements[5]);
      auto j31_dev = thrust::device_vector<float>(jacobian_elements[6]);
      auto j32_dev = thrust::device_vector<float>(jacobian_elements[7]);
      auto j33_dev = thrust::device_vector<float>(jacobian_elements[8]);
      auto cost_per_sample_dev = thrust::device_vector<float>(n_samples);
      // Get raw pointers to device data
      auto j11_raw = thrust::raw_pointer_cast(j11_dev.data());
      auto j12_raw = thrust::raw_pointer_cast(j12_dev.data());
      auto j13_raw = thrust::raw_pointer_cast(j13_dev.data());
      auto j21_raw = thrust::raw_pointer_cast(j21_dev.data());
      auto j22_raw = thrust::raw_pointer_cast(j22_dev.data());
      auto j23_raw = thrust::raw_pointer_cast(j23_dev.data());
      auto j31_raw = thrust::raw_pointer_cast(j31_dev.data());
      auto j32_raw = thrust::raw_pointer_cast(j32_dev.data());
      auto j33_raw = thrust::raw_pointer_cast(j33_dev.data());
      auto cost_per_sample_raw = thrust::raw_pointer_cast(cost_per_sample_dev.data());
      // Calculate kernel parameters
      int min_grid_size;
      int block_size;
      cudaOccupancyMaxPotentialBlockSize(
          &min_grid_size,
          &block_size,
          MMORF::kernel_log_jacobian_singular_values_cost_3d,
          0,
          0);
      unsigned int grid_size = (n_samples + block_size - 1)/block_size;
      // Call CUDA Kernel
      MMORF::kernel_log_jacobian_singular_values_cost_3d<<<grid_size, block_size>>>(
          // Input
          n_samples,
          j11_raw,
          j12_raw,
          j13_raw,
          j21_raw,
          j22_raw,
          j23_raw,
          j31_raw,
          j32_raw,
          j33_raw,
          // Output
          cost_per_sample_raw);
      checkCudaErrors(cudaDeviceSynchronize());
      // Copy calculated cost data back to host
      auto cost_per_sample_host = thrust::host_vector<float>(cost_per_sample_dev);
      // Return cost data
      auto cost_per_sample_return = std::vector<float>(cost_per_sample_host.size());
      thrust::copy(
          cost_per_sample_host.begin(),
          cost_per_sample_host.end(),
          cost_per_sample_return.begin());
      return cost_per_sample_return;
    }


    /// Calculate the gradient of the cost per jacobian element
    std::vector<std::vector<float> > calculate_grad_per_jacobian_element(
        const std::vector<std::vector<float> >& jacobian_elements)
    {
      auto n_samples = jacobian_elements[0].size();
      // Create device vectors using thrust::
      auto j11_dev = thrust::device_vector<float>(jacobian_elements[0]);
      auto j12_dev = thrust::device_vector<float>(jacobian_elements[1]);
      auto j13_dev = thrust::device_vector<float>(jacobian_elements[2]);
      auto j21_dev = thrust::device_vector<float>(jacobian_elements[3]);
      auto j22_dev = thrust::device_vector<float>(jacobian_elements[4]);
      auto j23_dev = thrust::device_vector<float>(jacobian_elements[5]);
      auto j31_dev = thrust::device_vector<float>(jacobian_elements[6]);
      auto j32_dev = thrust::device_vector<float>(jacobian_elements[7]);
      auto j33_dev = thrust::device_vector<float>(jacobian_elements[8]);
      auto d_cost_d_j11_dev = thrust::device_vector<float>(n_samples);
      auto d_cost_d_j12_dev = thrust::device_vector<float>(n_samples);
      auto d_cost_d_j13_dev = thrust::device_vector<float>(n_samples);
      auto d_cost_d_j21_dev = thrust::device_vector<float>(n_samples);
      auto d_cost_d_j22_dev = thrust::device_vector<float>(n_samples);
      auto d_cost_d_j23_dev = thrust::device_vector<float>(n_samples);
      auto d_cost_d_j31_dev = thrust::device_vector<float>(n_samples);
      auto d_cost_d_j32_dev = thrust::device_vector<float>(n_samples);
      auto d_cost_d_j33_dev = thrust::device_vector<float>(n_samples);
      // Get raw pointers to device data
      auto j11_raw = thrust::raw_pointer_cast(j11_dev.data());
      auto j12_raw = thrust::raw_pointer_cast(j12_dev.data());
      auto j13_raw = thrust::raw_pointer_cast(j13_dev.data());
      auto j21_raw = thrust::raw_pointer_cast(j21_dev.data());
      auto j22_raw = thrust::raw_pointer_cast(j22_dev.data());
      auto j23_raw = thrust::raw_pointer_cast(j23_dev.data());
      auto j31_raw = thrust::raw_pointer_cast(j31_dev.data());
      auto j32_raw = thrust::raw_pointer_cast(j32_dev.data());
      auto j33_raw = thrust::raw_pointer_cast(j33_dev.data());
      auto d_cost_d_j11_raw = thrust::raw_pointer_cast(d_cost_d_j11_dev.data());
      auto d_cost_d_j12_raw = thrust::raw_pointer_cast(d_cost_d_j12_dev.data());
      auto d_cost_d_j13_raw = thrust::raw_pointer_cast(d_cost_d_j13_dev.data());
      auto d_cost_d_j21_raw = thrust::raw_pointer_cast(d_cost_d_j21_dev.data());
      auto d_cost_d_j22_raw = thrust::raw_pointer_cast(d_cost_d_j22_dev.data());
      auto d_cost_d_j23_raw = thrust::raw_pointer_cast(d_cost_d_j23_dev.data());
      auto d_cost_d_j31_raw = thrust::raw_pointer_cast(d_cost_d_j31_dev.data());
      auto d_cost_d_j32_raw = thrust::raw_pointer_cast(d_cost_d_j32_dev.data());
      auto d_cost_d_j33_raw = thrust::raw_pointer_cast(d_cost_d_j33_dev.data());
      // Calculate kernel parameters
      int min_grid_size;
      int block_size;
      cudaOccupancyMaxPotentialBlockSize(
          &min_grid_size,
          &block_size,
          MMORF::kernel_log_jacobian_singular_values_grad_jxx_3d,
          0,
          0);
      unsigned int grid_size = (n_samples + block_size - 1)/block_size;
      // Call CUDA Kernel
      MMORF::kernel_log_jacobian_singular_values_grad_jxx_3d<<<grid_size, block_size>>>(
          // Input
          n_samples,
          j11_raw,
          j12_raw,
          j13_raw,
          j21_raw,
          j22_raw,
          j23_raw,
          j31_raw,
          j32_raw,
          j33_raw,
          // Output
          d_cost_d_j11_raw,
          d_cost_d_j12_raw,
          d_cost_d_j13_raw,
          d_cost_d_j21_raw,
          d_cost_d_j22_raw,
          d_cost_d_j23_raw,
          d_cost_d_j31_raw,
          d_cost_d_j32_raw,
          d_cost_d_j33_raw);
      checkCudaErrors(cudaDeviceSynchronize());
      // Copy calculated grad data back to host
      auto d_cost_d_j11_host = thrust::host_vector<float>(d_cost_d_j11_dev);
      auto d_cost_d_j12_host = thrust::host_vector<float>(d_cost_d_j12_dev);
      auto d_cost_d_j13_host = thrust::host_vector<float>(d_cost_d_j13_dev);
      auto d_cost_d_j21_host = thrust::host_vector<float>(d_cost_d_j21_dev);
      auto d_cost_d_j22_host = thrust::host_vector<float>(d_cost_d_j22_dev);
      auto d_cost_d_j23_host = thrust::host_vector<float>(d_cost_d_j23_dev);
      auto d_cost_d_j31_host = thrust::host_vector<float>(d_cost_d_j31_dev);
      auto d_cost_d_j32_host = thrust::host_vector<float>(d_cost_d_j32_dev);
      auto d_cost_d_j33_host = thrust::host_vector<float>(d_cost_d_j33_dev);
      // Return cost data
      auto grad_per_jacobian_element = std::vector<std::vector<float> >(
          9,
          std::vector<float>(n_samples));
      thrust::copy(
        d_cost_d_j11_host.begin(),
        d_cost_d_j11_host.end(),
        grad_per_jacobian_element[0].begin());
      thrust::copy(
        d_cost_d_j12_host.begin(),
        d_cost_d_j12_host.end(),
        grad_per_jacobian_element[1].begin());
      thrust::copy(
        d_cost_d_j13_host.begin(),
        d_cost_d_j13_host.end(),
        grad_per_jacobian_element[2].begin());
      thrust::copy(
        d_cost_d_j21_host.begin(),
        d_cost_d_j21_host.end(),
        grad_per_jacobian_element[3].begin());
      thrust::copy(
        d_cost_d_j22_host.begin(),
        d_cost_d_j22_host.end(),
        grad_per_jacobian_element[4].begin());
      thrust::copy(
        d_cost_d_j23_host.begin(),
        d_cost_d_j23_host.end(),
        grad_per_jacobian_element[5].begin());
      thrust::copy(
        d_cost_d_j31_host.begin(),
        d_cost_d_j31_host.end(),
        grad_per_jacobian_element[6].begin());
      thrust::copy(
        d_cost_d_j32_host.begin(),
        d_cost_d_j32_host.end(),
        grad_per_jacobian_element[7].begin());
      thrust::copy(
        d_cost_d_j33_host.begin(),
        d_cost_d_j33_host.end(),
        grad_per_jacobian_element[8].begin());
      return grad_per_jacobian_element;
    }


    /// Calculate the gradient of the cost per spline coefficient
    arma::fvec calculate_grad_per_spline(
        const std::shared_ptr<MMORF::WarpFieldBSpline> warp_field,
        const BASISFIELD::Spline1D<float>&             spline_1D,
        const int                                      sampling_frequency,
        const std::vector<float>&                      sample_resolution,
        const std::vector<std::vector<float> >&        grad_per_jacobian_element)
    {
      // Calculate jte_sz
      auto warp_sz = warp_field->get_parameter_size();
      auto jte_sz = warp_sz.first * warp_sz.second;
      auto sample_dimensions = warp_field->get_robust_sample_dimensions(
          sampling_frequency);
      // Make a vector for storing the result
      auto jte_vec = std::vector<float>(jte_sz,0.0f);
      // Loop over all positions in the local Jacobian
      for (auto row = 0; row < warp_sz.first; ++row){
        for (auto col = 0; col < warp_sz.first; ++col){
          auto element_id = (row * warp_sz.first) + col;
          // Generate the correct differentiated splines
          auto x_deriv_order = col == 0 ? 1 : 0;
          auto y_deriv_order = col == 1 ? 1 : 0;
          auto z_deriv_order = col == 2 ? 1 : 0;
          auto spline_x = BASISFIELD::Spline1D<float>(
              spline_1D.Order(),
              spline_1D.KnotSpacing(),
              x_deriv_order);
          auto spline_y = BASISFIELD::Spline1D<float>(
              spline_1D.Order(),
              spline_1D.KnotSpacing(),
              y_deriv_order);
          auto spline_z = BASISFIELD::Spline1D<float>(
              spline_1D.Order(),
              spline_1D.KnotSpacing(),
              z_deriv_order);
          auto spline_x_vals = CostFxnSplineUtils::spline_as_vec(
              spline_x,
              sample_resolution[0],
              x_deriv_order);
          auto spline_y_vals = CostFxnSplineUtils::spline_as_vec(
              spline_y,
              sample_resolution[1],
              y_deriv_order);
          auto spline_z_vals = CostFxnSplineUtils::spline_as_vec(
              spline_z,
              sample_resolution[2],
              z_deriv_order);
          auto temp_jte_vec = CostFxnSplineUtils::calculate_sub_grad(
              grad_per_jacobian_element[element_id],
              sample_dimensions,
              spline_x_vals,
              spline_y_vals,
              spline_z_vals,
              warp_field->get_dimensions(),
              std::vector<int>(3, static_cast<int>(spline_1D.KnotSpacing())));
          // Add subgrad to appropriate position
          auto jte_vec_begin_it = jte_vec.begin() + row * warp_sz.second;
          std::transform(
              temp_jte_vec.begin(),
              temp_jte_vec.end(),
              jte_vec_begin_it,
              jte_vec_begin_it,
              std::plus<float>());
        }
      }
      // Return result
      return arma::fvec(jte_vec.data(), jte_vec.size());
    }

    MMORF::SparseDiagonalMatrixTiled calculate_hess_per_spline(
        const std::vector<float>&                      cost_per_sample,
        const std::vector<std::vector<float> >&        grad_per_jacobian_element,
        const std::shared_ptr<MMORF::WarpFieldBSpline> warp_field,
        const BASISFIELD::Spline1D<float>&             spline_1D,
        const int                                      sampling_frequency,
        const std::vector<float>&                      sample_resolution)
    {
      // Create sparse tiled matrix to store sub-Hessian
      auto sparse_jtj = CostFxnSplineUtils::create_empty_jtj(warp_field, CostFxn::HESS_FULL, 3);
      // Get size of warp field
      auto warp_sz = warp_field->get_parameter_size();
      // Get dimensions of samples
      auto sample_dimensions = warp_field->get_robust_sample_dimensions(
          sampling_frequency);
      // Calculate the possible combinations of both gradient images and splines. There
      // should be 9 combinations for each of the 9 "sub-Hessians" leading to 81 in total.
      // There is however symmetry involved here. 3 of each of the 9s are redundant (or
      // are the transpose of each other) and therefore there should only be 36 unique
      // sub-hessians to calculate (which is still admittedly a lot).
      //
      // Loop over all warp directions
      for (auto row_hess = 0; row_hess < warp_sz.first; ++row_hess){
        // Loop over all warp directions again
        for (auto col_hess = 0; col_hess < warp_sz.first; ++col_hess){
          // Check if this is a unique sub-jtj - we will keep the main diagonal and below
          //if (col_hess > row_hess) continue;
          // Loop over the 3 Jacobian elements that contribute to the row gradient
          for (auto row_elem = 0; row_elem < warp_sz.first; ++row_elem){
            // Loop over the 3 Jabians elements that contribute to the col gradient
            for (auto col_elem = 0; col_elem < warp_sz.first; ++col_elem){
              // Check if this is a unique combination of elements - we will keep the main
              // diagonal and below
              // if (col_elem > row_elem) continue;
              // Generate the correct differentiated splines
              // Note that the modulo (%) operator converts the "elem" value to the column
              // position of that element in the Jacobian matrix
              auto x_deriv_order_row = row_elem == 0 ? 1 : 0;
              auto y_deriv_order_row = row_elem == 1 ? 1 : 0;
              auto z_deriv_order_row = row_elem == 2 ? 1 : 0;

              auto x_deriv_order_col = col_elem == 0 ? 1 : 0;
              auto y_deriv_order_col = col_elem == 1 ? 1 : 0;
              auto z_deriv_order_col = col_elem == 2 ? 1 : 0;

              auto spline_x_row = BASISFIELD::Spline1D<float>(
                  spline_1D.Order(),
                  spline_1D.KnotSpacing(),
                  x_deriv_order_row);
              auto spline_y_row = BASISFIELD::Spline1D<float>(
                  spline_1D.Order(),
                  spline_1D.KnotSpacing(),
                  y_deriv_order_row);
              auto spline_z_row = BASISFIELD::Spline1D<float>(
                  spline_1D.Order(),
                  spline_1D.KnotSpacing(),
                  z_deriv_order_row);

              auto spline_x_col = BASISFIELD::Spline1D<float>(
                  spline_1D.Order(),
                  spline_1D.KnotSpacing(),
                  x_deriv_order_col);
              auto spline_y_col = BASISFIELD::Spline1D<float>(
                  spline_1D.Order(),
                  spline_1D.KnotSpacing(),
                  y_deriv_order_col);
              auto spline_z_col = BASISFIELD::Spline1D<float>(
                  spline_1D.Order(),
                  spline_1D.KnotSpacing(),
                  z_deriv_order_col);

              auto spline_x_row_vals = CostFxnSplineUtils::spline_as_vec(
                  spline_x_row,
                  sample_resolution[0],
                  x_deriv_order_row);
              auto spline_y_row_vals = CostFxnSplineUtils::spline_as_vec(
                  spline_y_row,
                  sample_resolution[1],
                  y_deriv_order_row);
              auto spline_z_row_vals = CostFxnSplineUtils::spline_as_vec(
                  spline_z_row,
                  sample_resolution[2],
                  z_deriv_order_row);

              auto spline_x_col_vals = CostFxnSplineUtils::spline_as_vec(
                  spline_x_col,
                  sample_resolution[0],
                  x_deriv_order_col);
              auto spline_y_col_vals = CostFxnSplineUtils::spline_as_vec(
                  spline_y_col,
                  sample_resolution[1],
                  y_deriv_order_col);
              auto spline_z_col_vals = CostFxnSplineUtils::spline_as_vec(
                  spline_z_col,
                  sample_resolution[2],
                  z_deriv_order_col);

              // Pre-multiply the gradient_per_jacobian_element_fields
              auto field_pre_mult = std::vector<float>(cost_per_sample.size());
              std::transform(
                  grad_per_jacobian_element[row_hess*warp_sz.first + row_elem].begin(),
                  grad_per_jacobian_element[row_hess*warp_sz.first + row_elem].end(),
                  grad_per_jacobian_element[col_hess*warp_sz.first + col_elem].begin(),
                  field_pre_mult.begin(),
                  std::multiplies<float>()
                  );
              // Pre-divide by double the cost_per_sample
              /// \todo Replace this double operation with a single functor
              std::transform(
                  field_pre_mult.begin(),
                  field_pre_mult.end(),
                  cost_per_sample.begin(),
                  field_pre_mult.begin(),
                  [](float a, float b) -> float { return a/(2.0f*b); }
                  );
              // Call kernel for this combination of elements
              CostFxnSplineUtils::calculate_sub_jtj_non_symmetrical(
                // Input
                field_pre_mult,
                sample_dimensions,
                spline_x_row_vals,
                spline_y_row_vals,
                spline_z_row_vals,
                spline_x_col_vals,
                spline_y_col_vals,
                spline_z_col_vals,
                warp_field->get_dimensions(),
                std::vector<int>(3, static_cast<int>(spline_1D.KnotSpacing())),
                row_hess,
                col_hess,
                sparse_jtj);
            }
          }
          // Save above diagonal sub-jtj
          //if (row_hess > col_hess){
          //  sparse_jtj.copy_submatrix(row_hess, col_hess, col_hess, row_hess);
          //  sparse_jtj.transpose_submatrix(col_hess, row_hess);
          //}
        }
      }
      return sparse_jtj;
    }

    MMORF::SparseDiagonalMatrixTiled calculate_hess_per_spline_diag_hess(
        const std::vector<float>&                      cost_per_sample,
        const std::vector<std::vector<float> >&        grad_per_jacobian_element,
        const std::shared_ptr<MMORF::WarpFieldBSpline> warp_field,
        const BASISFIELD::Spline1D<float>&             spline_1D,
        const int                                      sampling_frequency,
        const std::vector<float>&                      sample_resolution)
    {
      // Determine sizes of splines etc.
      auto spline_sz = spline_1D.KernelSize();
      auto field_sz = cost_per_sample.size();
      auto sparse_jtj = CostFxnSplineUtils::create_empty_jtj(warp_field, CostFxn::HESS_DIAG, 3);
      // Get size of warp field
      auto warp_sz = warp_field->get_parameter_size();
      // Get dimensions of samples
      auto sample_dimensions = warp_field->get_robust_sample_dimensions(
          sampling_frequency);
      // Calculate the possible combinations of both gradient images and splines. There
      // should be 9 combinations for each of the 9 "sub-Hessians" leading to 81 in total.
      // There is however symmetry involved here. 3 of each of the 9s are redundant (or
      // are the transpose of each other) and therefore there should only be 36 unique
      // sub-hessians to calculate (which is still admittedly a lot).
      //
      //auto field_pre_mult = std::vector<std::vector<float> >(
      //    warp_sz.first*warp_sz.first*warp_sz.first*warp_sz.first,
      //    std::vector<float>(cost_per_sample.size(), 0));
      // Loop over all warp directions
      for (auto row_hess = 0; row_hess < warp_sz.first; ++row_hess){
        // Loop over all warp directions again
        for (auto col_hess = 0; col_hess < warp_sz.first; ++col_hess){
          // Check if this is a unique sub-jtj - we will keep the main diagonal and below
          //if (col_hess > row_hess) continue;
          auto field_pre_mult = std::vector<std::vector<float> >(9,
              std::vector<float>(field_sz, 0));

          auto spline_x_row_vals = std::vector<float>(
              warp_sz.first*warp_sz.first*spline_sz, 0);
          auto spline_y_row_vals = std::vector<float>(
              warp_sz.first*warp_sz.first*spline_sz, 0);
          auto spline_z_row_vals = std::vector<float>(
              warp_sz.first*warp_sz.first*spline_sz, 0);

          auto spline_x_col_vals = std::vector<float>(
              warp_sz.first*warp_sz.first*spline_sz, 0);
          auto spline_y_col_vals = std::vector<float>(
              warp_sz.first*warp_sz.first*spline_sz, 0);
          auto spline_z_col_vals = std::vector<float>(
              warp_sz.first*warp_sz.first*spline_sz, 0);

          auto loop_count = 0;
          // Loop over the 3 Jacobian elements that contribute to the row gradient
          for (auto row_elem = 0; row_elem < warp_sz.first; ++row_elem){
            // Loop over the 3 Jacobian elements that contribute to the col gradient
            for (auto col_elem = 0; col_elem < warp_sz.first; ++col_elem){
              // Check if this is a unique combination of elements - we will keep the main
              // diagonal and below
              // if (col_elem > row_elem) continue;
              // Generate the correct differentiated splines
              // Note that the modulo (%) operator converts the "elem" value to the column
              // position of that element in the Jacobian matrix
              auto x_deriv_order_row = row_elem == 0 ? 1 : 0;
              auto y_deriv_order_row = row_elem == 1 ? 1 : 0;
              auto z_deriv_order_row = row_elem == 2 ? 1 : 0;

              auto x_deriv_order_col = col_elem == 0 ? 1 : 0;
              auto y_deriv_order_col = col_elem == 1 ? 1 : 0;
              auto z_deriv_order_col = col_elem == 2 ? 1 : 0;

              auto spline_x_row = BASISFIELD::Spline1D<float>(
                  spline_1D.Order(),
                  spline_1D.KnotSpacing(),
                  x_deriv_order_row);
              auto spline_y_row = BASISFIELD::Spline1D<float>(
                  spline_1D.Order(),
                  spline_1D.KnotSpacing(),
                  y_deriv_order_row);
              auto spline_z_row = BASISFIELD::Spline1D<float>(
                  spline_1D.Order(),
                  spline_1D.KnotSpacing(),
                  z_deriv_order_row);

              auto spline_x_col = BASISFIELD::Spline1D<float>(
                  spline_1D.Order(),
                  spline_1D.KnotSpacing(),
                  x_deriv_order_col);
              auto spline_y_col = BASISFIELD::Spline1D<float>(
                  spline_1D.Order(),
                  spline_1D.KnotSpacing(),
                  y_deriv_order_col);
              auto spline_z_col = BASISFIELD::Spline1D<float>(
                  spline_1D.Order(),
                  spline_1D.KnotSpacing(),
                  z_deriv_order_col);

              auto spline_x_row_vals_tmp = CostFxnSplineUtils::spline_as_vec(
                  spline_x_row,
                  sample_resolution[0],
                  x_deriv_order_row);
              auto spline_y_row_vals_tmp = CostFxnSplineUtils::spline_as_vec(
                  spline_y_row,
                  sample_resolution[1],
                  y_deriv_order_row);
              auto spline_z_row_vals_tmp = CostFxnSplineUtils::spline_as_vec(
                  spline_z_row,
                  sample_resolution[2],
                  z_deriv_order_row);

              auto spline_x_col_vals_tmp = CostFxnSplineUtils::spline_as_vec(
                  spline_x_col,
                  sample_resolution[0],
                  x_deriv_order_col);
              auto spline_y_col_vals_tmp = CostFxnSplineUtils::spline_as_vec(
                  spline_y_col,
                  sample_resolution[1],
                  y_deriv_order_col);
              auto spline_z_col_vals_tmp = CostFxnSplineUtils::spline_as_vec(
                  spline_z_col,
                  sample_resolution[2],
                  z_deriv_order_col);

              std::copy(
                  spline_x_row_vals_tmp.begin(),
                  spline_x_row_vals_tmp.end(),
                  std::next(spline_x_row_vals.begin(), loop_count*spline_sz));
              std::copy(
                  spline_y_row_vals_tmp.begin(),
                  spline_y_row_vals_tmp.end(),
                  std::next(spline_y_row_vals.begin(), loop_count*spline_sz));
              std::copy(
                  spline_z_row_vals_tmp.begin(),
                  spline_z_row_vals_tmp.end(),
                  std::next(spline_z_row_vals.begin(), loop_count*spline_sz));

              std::copy(
                  spline_x_col_vals_tmp.begin(),
                  spline_x_col_vals_tmp.end(),
                  std::next(spline_x_col_vals.begin(), loop_count*spline_sz));
              std::copy(
                  spline_y_col_vals_tmp.begin(),
                  spline_y_col_vals_tmp.end(),
                  std::next(spline_y_col_vals.begin(), loop_count*spline_sz));
              std::copy(
                  spline_z_col_vals_tmp.begin(),
                  spline_z_col_vals_tmp.end(),
                  std::next(spline_z_col_vals.begin(), loop_count*spline_sz));

              // Pre-multiply the gradient_per_jacobian_element_fields
              std::transform(
                  grad_per_jacobian_element[row_hess*warp_sz.first + row_elem].begin(),
                  grad_per_jacobian_element[row_hess*warp_sz.first + row_elem].end(),
                  grad_per_jacobian_element[col_hess*warp_sz.first + col_elem].begin(),
                  field_pre_mult[loop_count].begin(),
                  std::multiplies<float>()
                  );
              // Pre-divide by double the cost_per_sample
              std::transform(
                  field_pre_mult[loop_count].begin(),
                  field_pre_mult[loop_count].end(),
                  cost_per_sample.begin(),
                  field_pre_mult[loop_count].begin(),
                  [](float a, float b) -> float { return a/(2.0f*b); }
                  );

              // Increase loop counter
              ++loop_count;
            }
          }
          // Call kernel for this combination of elements
          CostFxnSplineUtils::calculate_sub_jtj_non_symmetrical_diag_hess(
            // Input
            field_pre_mult,
            sample_dimensions,
            spline_x_row_vals,
            spline_y_row_vals,
            spline_z_row_vals,
            spline_x_col_vals,
            spline_y_col_vals,
            spline_z_col_vals,
            warp_field->get_dimensions(),
            std::vector<int>(3, static_cast<int>(spline_1D.KnotSpacing())),
            row_hess,
            col_hess,
            sparse_jtj);

          // Save above diagonal sub-jtj
          //if (row_hess > col_hess){
          //  sparse_jtj.copy_submatrix(row_hess, col_hess, col_hess, row_hess);
          //  sparse_jtj.transpose_submatrix(col_hess, row_hess);
          //}
        }
      }
      return sparse_jtj;
    }
  } // namespace CostFxnLogJacobianSingularValuesUtils
} // namespace MMORF
