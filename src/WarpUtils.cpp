//////////////////////////////////////////////////////////////////////////////////////////////
/// \file WarpUtils.cpp
/// \brief Affine transformation utility functions
/// \author Frederik Lange
/// \date April 2024
/// \copyright Copyright (C) 2018 University of Oxford
//////////////////////////////////////////////////////////////////////////////////////////////


#include "WarpUtils.h"
#include "WarpField.h"

#include <armadillo>

#include <cassert>
#include <vector>

namespace MMORF
{
  namespace WarpUtils
  {
    /// Affine transform a given set of positions
    std::vector<std::vector<float> > apply_affine_transform(
        const std::vector<std::vector<float> >& positions,
        const arma::fmat&                       affine_mat)
    {
      auto positions_out = std::vector<std::vector<float> >(positions.size());
      // Apply affine transform
      // NB!!! Because armadillo is column major, we use a column vector for each vector of
      // positions for a particular dimension, and therefore the transpose of the affine_
      // matrix
      arma::fmat augmented_positions(positions[0].size(), positions.size() + 1);
      { // Scope limiting
        auto vec_dim = 0;
        for (const auto& vec : positions){
          augmented_positions.col(vec_dim) = arma::fvec(vec);
          ++vec_dim;
        }
        augmented_positions.col(vec_dim) = arma::fvec(
          positions[0].size(),
          arma::fill::ones);
        augmented_positions = augmented_positions * affine_mat.t();
      } // Scope limiting
      { // Scope limiting
        auto vec_dim = 0;
        for (auto& vec : positions_out){
          vec = arma::conv_to<std::vector<float> >::from(augmented_positions.col(vec_dim));
          ++vec_dim;
        }
      } // Scope limiting
      return positions_out;
    }

    /// Given a set of positions, return the warped positions
    std::vector<std::vector<float> > apply_nonlinear_transform(
        const std::vector<std::vector<float> >& positions,
        const MMORF::WarpField&                 field)
    {
      auto offsets       = field.sample_warp(positions);
      auto positions_out = add_positions(positions, offsets);
      return positions_out;
    }

    /// Apply both an affine transform and a warp to a given set of positions
    /// \details This is necessary when the affine transform is acting only on the original
    ///          sample positions, and independent of the warp field
    std::vector<std::vector<float> > apply_affine_then_nonlinear_transform(
        const std::vector<std::vector<float> >& positions,
        const arma::fmat&                       affine_mat,
        const MMORF::WarpField&                 field)
    {
      auto affine_positions = apply_affine_transform(positions, affine_mat);
      auto warp_offsets     = field.sample_warp(positions);
      auto final_positions  = add_positions(affine_positions, warp_offsets);
      return final_positions;
    }

    /// Warp the given positions and then apply the affine transform.
    /// \details This is necessary in a number of situations, such as when finding the
    ///          derivative of a volume in a space reached via the given affine transform
    std::vector<std::vector<float> > apply_nonlinear_then_affine_transform(
        const std::vector<std::vector<float> >& positions,
        const arma::fmat&                       affine_mat,
        const MMORF::WarpField&                 field)
    {
      auto warped_positions = apply_nonlinear_transform(positions, field);
      auto affine_positions = apply_affine_transform(warped_positions, affine_mat);
      return affine_positions;
    }


    /// Add two arrays of positions together
    std::vector<std::vector<float> > add_positions(
        const std::vector<std::vector<float> >& positions_first,
        const std::vector<std::vector<float> >& positions_second)
    {
      // Check positions have the same dimensions
      assert(positions_first.size() == positions_second.size()
          && positions_first[0].size() == positions_second[0].size());
      auto positions_out = positions_first;
      for (auto i = 0; i < positions_out.size(); ++i){
        for (auto j = 0; j < positions_out[0].size(); ++j){
          positions_out[i][j] += positions_second[i][j];
        }
      }
      return positions_out;
    }

  } // namespace WarpUtils
} // namespace MMORF
