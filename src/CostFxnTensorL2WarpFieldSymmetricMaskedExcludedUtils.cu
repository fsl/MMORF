//////////////////////////////////////////////////////////////////////////////////////////////
/// \file CostFxnTensorL2WarpFieldSymmetricMaskedExcludedUtils.cuh
/// \brief Utility functions used by tensor CostFxns.
/// \details
/// \author Frederik Lange
/// \date February 2024
/// \copyright Copyright (C) 2018 University of Oxford
//////////////////////////////////////////////////////////////////////////////////////////////

#include "CostFxnTensorL2WarpFieldSymmetricMaskedExcludedUtils.h"
#include "CostFxn.h"
#include "CostFxnSplineUtils.h"
#include "CostFxnKernels.cuh"
#include "Volume.h"
#include "VolumeBSpline.h"
#include "VolumeTensor.h"
#include "WarpFieldBSpline.h"
#include "WarpUtils.h"
#include "helper_cuda.cuh"

#include "basisfield/fsl_splines.h"

#include <thrust/device_vector.h>
#include <thrust/host_vector.h>

#include <armadillo>

#include <iostream>
#include <memory>
#include <vector>

namespace MMORF
{
  namespace CostFxnTensorL2WarpFieldSymmetricMaskedExcludedUtils
  {

    /// Get cost under current parameterisation
    /// \details There are several non-intuitive factors influencing the cost. The first is
    ///          that the warp space is not necessarily the same as the reference image
    ///          space. Therefore we need reorient the reference samples according to
    ///          affine_ref_. This is similarly true for the moving image and affine_mov_.
    ///          Finally, the warp induces local rotations on the reference image which need
    ///          to be accounted for on a voxel by voxel basis. Only once all of these
    ///          reorientations have been performed correctly can the cost be properly
    ///          calculated.
    float cost(
        const std::shared_ptr<MMORF::VolumeTensor>     vol_ref,
        const std::shared_ptr<MMORF::VolumeTensor>     vol_mov,
        const std::shared_ptr<MMORF::Volume>           mask_ref,
        const std::shared_ptr<MMORF::Volume>           mask_mov,
        const arma::fmat                               affine_ref,
        const arma::fmat                               affine_mov,
        const std::shared_ptr<MMORF::WarpFieldBSpline> warp_field,
        const std::vector<std::vector<float> >         sample_positions_ref,
        const std::vector<std::vector<float> >         sample_positions_warp)
    {
      // Calculate rotational effect of warp
      auto rotations = calculate_rotation(
          warp_field,
          sample_positions_warp);
      // Sample reference volume and reorient
      auto samples_ref = vol_ref->sample(sample_positions_ref);
      affine_reorient_samples(samples_ref, affine_ref.i());
      nonlin_reorient_samples(samples_ref, rotations);
      // Sample moving volume and reorient
      auto sample_positions_warped = WarpUtils::apply_nonlinear_then_affine_transform(
          sample_positions_warp,
          affine_mov,
          *warp_field);
      auto samples_mov = vol_mov->sample(sample_positions_warped);
      affine_reorient_samples(samples_mov, affine_mov.i());
      // Calculate Jacobian determinant for current parametrisation
      auto samples_jac_det = warp_field->jacobian_determinants(sample_positions_warp);
      // Calculate cost from samples
      auto samples_error = std::vector<float>(samples_ref[0].size());
      auto cost = 0.0f;
      for (auto i = 0; i < 9; ++i){
        std::transform(
            samples_mov[i].begin(),
            samples_mov[i].end(),
            samples_ref[i].begin(),
            samples_error.begin(),
            std::minus<float>()
            );
        std::transform(
            samples_error.begin(),
            samples_error.end(),
            samples_error.begin(),
            samples_error.begin(),
            std::multiplies<float>()
            );
        std::transform(
            samples_error.begin(),
            samples_error.end(),
            samples_jac_det.begin(),
            samples_error.begin(),
            [](float a, float b) -> float { return 0.5f*(1.0f + b)*a; }
            );
        if (mask_ref){
          auto samples_mask_ref = mask_ref->sample(sample_positions_ref);
          std::transform(
              samples_error.begin(),
              samples_error.end(),
              samples_mask_ref.begin(),
              samples_error.begin(),
              std::multiplies<float>()
              );
        }
        if (mask_mov){
          auto samples_mask_mov = mask_mov->sample(sample_positions_warped);
          std::transform(
              samples_error.begin(),
              samples_error.end(),
              samples_mask_mov.begin(),
              samples_error.begin(),
              std::multiplies<float>()
              );
        }
        cost = std::accumulate(
            samples_error.begin(),
            samples_error.end(),
            cost
            );
      }
        // Avergage cost
      cost /= samples_ref[0].size();
      return cost;
    }

    /// Get Jte under current parameterisation
    arma::fvec grad(
        const std::shared_ptr<MMORF::VolumeTensor>     vol_ref,
        const std::shared_ptr<MMORF::VolumeTensor>     vol_mov,
        const std::shared_ptr<MMORF::Volume>           mask_ref,
        const std::shared_ptr<MMORF::Volume>           mask_mov,
        const arma::fmat                               affine_ref,
        const arma::fmat                               affine_mov,
        const std::shared_ptr<MMORF::WarpFieldBSpline> warp_field,
        const std::vector<std::vector<float> >         sample_positions_ref,
        const std::vector<std::vector<float> >         sample_positions_warp,
        const BASISFIELD::Spline1D<float>              spline_1D,
        const std::vector<int>                         sample_dimensions,
        const std::vector<float>                       sample_resolution)
    {
      // Calculate rotational effect of warp
      auto rotations = calculate_rotation(warp_field, sample_positions_warp);
      // Calculate jte_sz
      auto warp_sz = warp_field->get_parameter_size();
      auto jte_sz = warp_sz.first * warp_sz.second;
      // Make a vector for storing the result
      auto jte_vec = std::vector<float>(jte_sz, 0);
      // Create and sample error volume
      // Sample reference and moving volumes
      auto samples_ref = vol_ref->sample(sample_positions_ref);
      affine_reorient_samples(samples_ref, affine_ref.i());
      // Keep an un-nonlinear-rotated copy of the reference for rotation gradient calc
      auto samples_ref_unrotated = samples_ref;
      nonlin_reorient_samples(samples_ref, rotations);
      // Sample moving volume and reorient
      auto sample_positions_warped = WarpUtils::apply_nonlinear_then_affine_transform(
          sample_positions_warp,
          affine_mov,
          *warp_field);
      auto samples_mov = vol_mov->sample(sample_positions_warped);
      affine_reorient_samples(samples_mov, affine_mov.i());
      // Calculate Jacobian determinant for current parametrisation
      auto samples_jac_det = warp_field->jacobian_determinants(sample_positions_warp);
      // Calculate current sample error
      auto samples_error = std::vector<std::vector<float> >(
          9, std::vector<float>(samples_ref[0].size()));
      for (auto i = 0; i < 9; ++i){
        std::transform(
            samples_mov[i].begin(),
            samples_mov[i].end(),
            samples_ref[i].begin(),
            samples_error[i].begin(),
            std::minus<float>()
            );
        if (mask_ref){
          auto samples_mask_ref = mask_ref->sample(sample_positions_ref);
          std::transform(
              samples_error[i].begin(),
              samples_error[i].end(),
              samples_mask_ref.begin(),
              samples_error[i].begin(),
              std::multiplies<float>()
              );
        }
        if (mask_mov){
          auto samples_mask_mov = mask_mov->sample(sample_positions_warped);
          std::transform(
              samples_error[i].begin(),
              samples_error[i].end(),
              samples_mask_mov.begin(),
              samples_error[i].begin(),
              std::multiplies<float>()
              );
        }
      }
      // Convert spline to vec
      auto spline_vec = CostFxnSplineUtils::spline_as_vec(spline_1D);
      /////////////////////////////////////////////////////////////////////////////////////
      /// Gradient due to displacement
      /////////////////////////////////////////////////////////////////////////////////////
      // Calculate derivative samples
      auto samples_mov_deriv_vec = calculate_derivatives_mov(
          vol_mov,
          sample_positions_warped,
          affine_mov);
      // Loop over all warp directions
      for (auto warp_dim = 0; warp_dim < 3; ++warp_dim){
        // Pre-multiply the error and derivative samples
        auto samples_pre_mult = std::vector<float>(samples_error[0].size(), 0.0f);
        // Loop over all elements of the tensor
        for (auto tensor_elem = 0; tensor_elem < 9; ++tensor_elem){
          auto tmp_pre_mult = std::vector<float>(samples_error[0].size());
          std::transform(
              samples_error[tensor_elem].begin(),
              samples_error[tensor_elem].end(),
              samples_mov_deriv_vec[warp_dim][tensor_elem].begin(),
              tmp_pre_mult.begin(),
              std::multiplies<float>()
              );
          std::transform(
              samples_pre_mult.begin(),
              samples_pre_mult.end(),
              tmp_pre_mult.begin(),
              samples_pre_mult.begin(),
              std::plus<float>()
              );
        }
        std::transform(
            samples_pre_mult.begin(),
            samples_pre_mult.end(),
            samples_jac_det.begin(),
            samples_pre_mult.begin(),
            [](float a, float b) -> float { return 0.5f*(1.0f + b)*a; }
            );
        auto sub_jte = CostFxnSplineUtils::calculate_sub_jte(
            // Input
            samples_pre_mult,
            sample_dimensions,
            spline_vec,
            spline_vec,
            spline_vec,
            warp_field->get_dimensions(),
            std::vector<int>(3, spline_1D.KnotSpacing()));
        // Get correct index into jte_dev (x, then y, then z)
        std::copy(sub_jte.begin(), sub_jte.end(),
                  jte_vec.begin() + warp_dim * warp_sz.second);
      }
      /////////////////////////////////////////////////////////////////////////////////////
      /// Gradient due to rotation
      /////////////////////////////////////////////////////////////////////////////////////
      // Calculate dG/dJ
      // First calculate "S"
      auto s_mats = calculate_s(warp_field, sample_positions_warp);
      auto grad_per_jacobian_element = calculate_grad_per_jacobian_element(
          samples_ref_unrotated,
          rotations,
          s_mats);
      auto grad_per_spline = calculate_grad_per_spline(
          mask_ref,
          mask_mov,
          warp_field,
          sample_positions_ref,
          sample_positions_warp,
          spline_1D,
          sample_dimensions,
          sample_resolution,
          grad_per_jacobian_element,
          samples_error,
          sample_positions_warped);
      // Return result
      auto jte_return = arma::fvec(jte_vec.data(), jte_vec.size());
      /// \todo Check the sign (i.e., do we add or subtract this?)
      jte_return = jte_return + grad_per_spline;
      jte_return = 2.0f * jte_return/samples_ref[0].size();
      return jte_return;
    }

    /// Get JtJ under current parameterisation
    MMORF::SparseDiagonalMatrixTiled hess(
        const std::shared_ptr<MMORF::VolumeTensor>     vol_ref,
        const std::shared_ptr<MMORF::VolumeTensor>     vol_mov,
        const std::shared_ptr<MMORF::Volume>           mask_ref,
        const std::shared_ptr<MMORF::Volume>           mask_mov,
        const arma::fmat                               affine_ref,
        const arma::fmat                               affine_mov,
        const std::shared_ptr<MMORF::WarpFieldBSpline> warp_field,
        const std::vector<std::vector<float> >         sample_positions_ref,
        const std::vector<std::vector<float> >         sample_positions_warp,
        const BASISFIELD::Spline1D<float>              spline_1D,
        const std::vector<int>                         sample_dimensions,
        const std::vector<float>                       sample_resolution,
        enum MMORF::CostFxn::HessType                  hess_type)
    {
      // Calculate rotational effect of warp
      auto rotations = calculate_rotation(warp_field, sample_positions_warp);
      // Get size of warp field
      auto warp_sz = warp_field->get_parameter_size();
      // Apply warp field to sample positions
      auto sample_positions_warped = WarpUtils::apply_nonlinear_then_affine_transform(
          sample_positions_warp,
          affine_mov,
          *warp_field);
      // Sample derivative of moving volume in all directions and store in vector
      auto samples_mov_deriv_vec = calculate_derivatives_mov(
          vol_mov,
          sample_positions_warped,
          affine_mov);
      // Calculate Jacobian determinant for current parametrisation
      auto samples_jac_det = warp_field->jacobian_determinants(sample_positions_warp);
      /////////////////////////////////////////////////////////////////////////////////////
      /// Hessian due to displacement
      /////////////////////////////////////////////////////////////////////////////////////
      // Create sparse tiled matrix to store Hessian
      auto sparse_jtj = CostFxnSplineUtils::create_empty_jtj(warp_field, hess_type, 3);
      // Convert spline to vec
      auto spline_vec = CostFxnSplineUtils::spline_as_vec(spline_1D);
      // Loop over all warp directions
      for (auto row = 0; row < warp_sz.first; ++row){
        // Loop over all warp directions again
        for (auto col = 0; col < warp_sz.first; ++col){
          // Check if this is a unique sub-jtj - we will keep the main diagonal and below
          if (col > row) continue;
          // Pre-multiply the derivative samples
          auto samples_pre_mult = std::vector<float>(
              samples_mov_deriv_vec[0][0].size(), 0.0f);
          // Loop over all elements of the tensor
          for (auto tensor_elem = 0; tensor_elem < 9; ++tensor_elem){
            auto tmp_pre_mult = std::vector<float>(samples_pre_mult.size());
            std::transform(
                samples_mov_deriv_vec[row][tensor_elem].begin(),
                samples_mov_deriv_vec[row][tensor_elem].end(),
                samples_mov_deriv_vec[col][tensor_elem].begin(),
                tmp_pre_mult.begin(),
                std::multiplies<float>()
                );
            std::transform(
                samples_pre_mult.begin(),
                samples_pre_mult.end(),
                tmp_pre_mult.begin(),
                samples_pre_mult.begin(),
                std::plus<float>()
                );
          }
          // Pre-multiply by the Jacobian modulation
          std::transform(
              samples_pre_mult.begin(),
              samples_pre_mult.end(),
              samples_jac_det.begin(),
              samples_pre_mult.begin(),
              [](float a, float b) -> float { return 0.5f*(1.0f + b)*a; }
              );
          if (mask_ref){
            auto samples_mask_ref = mask_ref->sample(sample_positions_ref);
            std::transform(
                samples_pre_mult.begin(),
                samples_pre_mult.end(),
                samples_mask_ref.begin(),
                samples_pre_mult.begin(),
                std::multiplies<float>()
                );
          }
          if (mask_mov){
            auto samples_mask_mov = mask_mov->sample(sample_positions_warped);
            std::transform(
                samples_pre_mult.begin(),
                samples_pre_mult.end(),
                samples_mask_mov.begin(),
                samples_pre_mult.begin(),
                std::multiplies<float>()
                );
          }
          // Call kernel
          auto calc_sub_jtj =
            hess_type == CostFxn::HESS_FULL
            ? CostFxnSplineUtils::calculate_sub_jtj_symmetrical
            : CostFxnSplineUtils::calculate_sub_jtj_symmetrical_diag_hess;
          calc_sub_jtj(
            // Input
            samples_pre_mult,
            sample_dimensions,
            spline_vec,
            spline_vec,
            spline_vec,
            warp_field->get_dimensions(),
            std::vector<int>(3, spline_1D.KnotSpacing()),
            row,
            col,
            sparse_jtj);

          // Save above diagonal sub-jtj
          if (hess_type == CostFxn::HESS_FULL && row > col) {
            sparse_jtj.copy_submatrix(row, col, col, row);
          }
        }
      }
      /////////////////////////////////////////////////////////////////////////////////////
      /// Hessian due to rotation
      /////////////////////////////////////////////////////////////////////////////////////
      // Calculate grad_per_jacobian_element
      auto samples_ref = vol_ref->sample(sample_positions_ref);
      affine_reorient_samples(samples_ref, affine_ref.i());
      auto s_mats = calculate_s(
          warp_field,
          sample_positions_warp);
      auto grad_per_jacobian_element = calculate_grad_per_jacobian_element(
          samples_ref,
          rotations,
          s_mats);
      // Calculate hess_per_spline
      auto calc_hess = hess_type == CostFxn::HESS_FULL
        ? calculate_hess_per_spline
        : calculate_hess_per_spline_diag_hess;
      auto hess_per_spline = calc_hess(
        mask_ref,
        mask_mov,
        warp_field,
        sample_positions_ref,
        sample_positions_warp,
        spline_1D,
        sample_dimensions,
        sample_resolution,
        grad_per_jacobian_element,
        sample_positions_warped);
      sparse_jtj += hess_per_spline;
      sparse_jtj *= (2.0f / static_cast<float>(samples_mov_deriv_vec[0][0].size()));
      // Return completed matrix
      return sparse_jtj;
    }

    /// Calculate the rotational effect of the warp field in the warp space
    std::vector<arma::fmat> calculate_rotation(
        const std::shared_ptr<MMORF::WarpFieldBSpline> warp_field,
        const std::vector<std::vector<float> >&        sample_positions)
    {
      auto rotation_matrices = std::vector<arma::fmat>(sample_positions[0].size());
      // Begin by getting the jacobian matrices
      auto jacobian_elements = warp_field->get_jacobian_elements(sample_positions);
      auto jacobian_matrices_arma = vector_to_arma(jacobian_elements);
      // Calculate Finite Strain rotation matrices as follows:
      //
      //        If J = UR where U is a scaling matrix and R is a rotation, then
      //
      //        R = ((JJ^T)^-1/2)J
#pragma omp parallel for
      for (auto i = 0; i < sample_positions[0].size(); ++i){
        if (arma::sqrtmat_sympd(
              rotation_matrices[i],
              jacobian_matrices_arma[i]*(jacobian_matrices_arma[i].t()))){
          rotation_matrices[i] = rotation_matrices[i].i() * jacobian_matrices_arma[i];
        }
        else{
          rotation_matrices[i].eye(3,3);
          std::cout << "WARNING: sqrtmat_sympd failed at location " << i << std::endl;
        }
      }
      return rotation_matrices;
    }

    /// Calculate the following for each position:
    ///
    ///         S = (JJ^T)^1/2
    // warp_field_
    std::vector<arma::fmat> calculate_s(
        const std::shared_ptr<MMORF::WarpFieldBSpline> warp_field,
        const std::vector<std::vector<float> >&        sample_positions)
    {
      auto s_matrices = std::vector<arma::fmat>(sample_positions[0].size());
      // Begin by getting the jacobian matrices
      auto jacobian_elements = warp_field->get_jacobian_elements(sample_positions);
      auto jacobian_matrices_arma = vector_to_arma(jacobian_elements);
#pragma omp parallel for
      for (auto i = 0; i < sample_positions[0].size(); ++i){
        if (!arma::sqrtmat_sympd(
              s_matrices[i],
              jacobian_matrices_arma[i]*(jacobian_matrices_arma[i].t()))){
          s_matrices[i].eye(3,3);
          std::cout << "WARNING: sqrtmat_sympd failed at location " << i << std::endl;
        }
      }
      return s_matrices;
    }

    std::vector<arma::fmat> vector_to_arma(
        const std::vector<std::vector<float> >& vector_rm)
    {
      auto matrices = std::vector<arma::fmat>(vector_rm[0].size());
#pragma omp parallel for
      for (auto i = 0; i < vector_rm[0].size(); ++i){
        matrices[i] =
            arma::fmat{
              {vector_rm[0][i], vector_rm[1][i], vector_rm[2][i]},
              {vector_rm[3][i], vector_rm[4][i], vector_rm[5][i]},
              {vector_rm[6][i], vector_rm[7][i], vector_rm[8][i]}};
      }
      return matrices;
    }


    /// Convert a 3x3 armadillo matrix to row-major vectorised format
    std::vector<std::vector<float> > arma_to_vector(
        const std::vector<arma::fmat>& matrices)
    {
      // Note that vector is row-major flattened, but armadillo matrices are column major.
      auto vectors = std::vector<std::vector<float> >(
          9, std::vector<float>(matrices.size()));
      for (auto i = 0; i < matrices.size(); ++i){
        vectors[0][i] = matrices[i][0];
        vectors[1][i] = matrices[i][3];
        vectors[2][i] = matrices[i][6];
        vectors[3][i] = matrices[i][1];
        vectors[4][i] = matrices[i][4];
        vectors[5][i] = matrices[i][7];
        vectors[6][i] = matrices[i][2];
        vectors[7][i] = matrices[i][5];
        vectors[8][i] = matrices[i][8];
      }
      return vectors;
    }

    /// Rotate tensors by calculating the rotational effect of the affine matrix as follows:
    ///
    ///     R  = ((JJ^T)^-1/2)J
    ///     D' = R*D*R^T
    void affine_reorient_samples(
        std::vector<std::vector<float> >& tensors,
        const arma::fmat&                 affine_mat)
    {
      arma::fmat affine_submat = affine_mat.submat(0,0,2,2);
      arma::fmat r_mat = arma::sqrtmat_sympd(
         (affine_submat * (affine_submat.t())).i()) * affine_submat;
      auto d_mats = vector_to_arma(tensors);
#pragma omp parallel for
      for (auto i = 0; i < d_mats.size(); ++i){
        d_mats[i] = r_mat*d_mats[i]*(r_mat.t());
      }
      tensors = arma_to_vector(d_mats);
    }

    /// Rotate tensors as follows
    ///
    ///     D' = R*D*R^T
    void nonlin_reorient_samples(
        std::vector<std::vector<float> >& tensors,
        const std::vector<arma::fmat>&    rotations)
    {
      /// \todo replace assert with exception
      assert(tensors[0].size() == rotations.size());
      auto rotated_tensors_arma = std::vector<arma::fmat>(rotations.size());
      auto tensor_matrices_arma = vector_to_arma(tensors);
#pragma omp parallel for
      for (auto i = 0; i < rotations.size(); ++i){
        rotated_tensors_arma[i] =
            rotations[i]*tensor_matrices_arma[i]*(rotations[i].t());
      }
      tensors = arma_to_vector(rotated_tensors_arma);
    }

    std::vector<std::vector<std::vector<float> > > calculate_derivatives_mov(
        const std::shared_ptr<MMORF::VolumeTensor> vol_mov,
        const std::vector<std::vector<float> >&    positions,
        const arma::fmat&                          affine_mat)
    {
      /// \todo replace assert with exception
      assert(positions.size() == sample_dimensions_.size());
      auto sample_sz = positions[0].size();
      // Create an armadillo matrix to store the samples as this will make life easier when
      // doing the affine transform
      /// \todo change dims from [3][9][n] to [9][3][n]
      auto derivatives_vec = vol_mov->sample_gradient(positions);
      auto derivatives = std::vector<std::vector<std::vector<float> > >(
          3,
          std::vector<std::vector<float> >(
            9,
            std::vector<float>(
              sample_sz)));
#pragma omp parallel for
      for (auto i = 0; i < 3; ++i){
        for (auto j = 0; j < 9; ++j){
          for (auto k = 0; k < sample_sz; ++k){
            derivatives[i][j][k] = derivatives_vec[j][i][k];
          }
        }
      }
      // Transform derivative through affine
#pragma omp parallel for
      for (auto i = 0; i < 9; ++ i){
        auto derivatives_arma = arma::fmat(positions[0].size(), 3);
        derivatives_arma.col(0) = arma::fvec(derivatives[0][i]);
        derivatives_arma.col(1) = arma::fvec(derivatives[1][i]);
        derivatives_arma.col(2) = arma::fvec(derivatives[2][i]);
        derivatives_arma = derivatives_arma * affine_mat.submat(0,0,2,2);
        derivatives[0][i] = arma::conv_to<std::vector<float> >::from(
            derivatives_arma.col(0));
        derivatives[1][i] = arma::conv_to<std::vector<float> >::from(
            derivatives_arma.col(1));
        derivatives[2][i] = arma::conv_to<std::vector<float> >::from(
            derivatives_arma.col(2));
      }
      // Rotate tensors through affine
#pragma omp parallel for
      for (auto i = 0; i < 3; ++i){
        affine_reorient_samples(derivatives[i], affine_mat.i());
      }
      return derivatives;
    }

    std::vector<std::vector<std::vector<float> > > calculate_grad_per_jacobian_element(
        const std::vector<std::vector<float> >& g_samples,
        const std::vector<arma::fmat>&          r_mats,
        const std::vector<arma::fmat>&          s_mats)
    {
      // Flatten r_mats and s_mats
      auto r_samples = arma_to_vector(r_mats);
      auto s_samples = arma_to_vector(s_mats);

      // Create device vectors using thrust::
      auto n_samples = g_samples[0].size();

      auto g11_dev = thrust::device_vector<float>(g_samples[0]);
      auto g12_dev = thrust::device_vector<float>(g_samples[1]);
      auto g13_dev = thrust::device_vector<float>(g_samples[2]);
      auto g22_dev = thrust::device_vector<float>(g_samples[4]);
      auto g23_dev = thrust::device_vector<float>(g_samples[5]);
      auto g33_dev = thrust::device_vector<float>(g_samples[8]);

      auto r11_dev = thrust::device_vector<float>(r_samples[0]);
      auto r12_dev = thrust::device_vector<float>(r_samples[1]);
      auto r13_dev = thrust::device_vector<float>(r_samples[2]);
      auto r21_dev = thrust::device_vector<float>(r_samples[3]);
      auto r22_dev = thrust::device_vector<float>(r_samples[4]);
      auto r23_dev = thrust::device_vector<float>(r_samples[5]);
      auto r31_dev = thrust::device_vector<float>(r_samples[6]);
      auto r32_dev = thrust::device_vector<float>(r_samples[7]);
      auto r33_dev = thrust::device_vector<float>(r_samples[8]);

      auto s11_dev = thrust::device_vector<float>(s_samples[0]);
      auto s12_dev = thrust::device_vector<float>(s_samples[1]);
      auto s13_dev = thrust::device_vector<float>(s_samples[2]);
      auto s22_dev = thrust::device_vector<float>(s_samples[4]);
      auto s23_dev = thrust::device_vector<float>(s_samples[5]);
      auto s33_dev = thrust::device_vector<float>(s_samples[8]);

      auto d_g11_d_j11_dev = thrust::device_vector<float>(n_samples);
      auto d_g11_d_j12_dev = thrust::device_vector<float>(n_samples);
      auto d_g11_d_j13_dev = thrust::device_vector<float>(n_samples);
      auto d_g11_d_j21_dev = thrust::device_vector<float>(n_samples);
      auto d_g11_d_j22_dev = thrust::device_vector<float>(n_samples);
      auto d_g11_d_j23_dev = thrust::device_vector<float>(n_samples);
      auto d_g11_d_j31_dev = thrust::device_vector<float>(n_samples);
      auto d_g11_d_j32_dev = thrust::device_vector<float>(n_samples);
      auto d_g11_d_j33_dev = thrust::device_vector<float>(n_samples);

      auto d_g12_d_j11_dev = thrust::device_vector<float>(n_samples);
      auto d_g12_d_j12_dev = thrust::device_vector<float>(n_samples);
      auto d_g12_d_j13_dev = thrust::device_vector<float>(n_samples);
      auto d_g12_d_j21_dev = thrust::device_vector<float>(n_samples);
      auto d_g12_d_j22_dev = thrust::device_vector<float>(n_samples);
      auto d_g12_d_j23_dev = thrust::device_vector<float>(n_samples);
      auto d_g12_d_j31_dev = thrust::device_vector<float>(n_samples);
      auto d_g12_d_j32_dev = thrust::device_vector<float>(n_samples);
      auto d_g12_d_j33_dev = thrust::device_vector<float>(n_samples);

      auto d_g13_d_j11_dev = thrust::device_vector<float>(n_samples);
      auto d_g13_d_j12_dev = thrust::device_vector<float>(n_samples);
      auto d_g13_d_j13_dev = thrust::device_vector<float>(n_samples);
      auto d_g13_d_j21_dev = thrust::device_vector<float>(n_samples);
      auto d_g13_d_j22_dev = thrust::device_vector<float>(n_samples);
      auto d_g13_d_j23_dev = thrust::device_vector<float>(n_samples);
      auto d_g13_d_j31_dev = thrust::device_vector<float>(n_samples);
      auto d_g13_d_j32_dev = thrust::device_vector<float>(n_samples);
      auto d_g13_d_j33_dev = thrust::device_vector<float>(n_samples);

      auto d_g21_d_j11_dev = thrust::device_vector<float>(n_samples);
      auto d_g21_d_j12_dev = thrust::device_vector<float>(n_samples);
      auto d_g21_d_j13_dev = thrust::device_vector<float>(n_samples);
      auto d_g21_d_j21_dev = thrust::device_vector<float>(n_samples);
      auto d_g21_d_j22_dev = thrust::device_vector<float>(n_samples);
      auto d_g21_d_j23_dev = thrust::device_vector<float>(n_samples);
      auto d_g21_d_j31_dev = thrust::device_vector<float>(n_samples);
      auto d_g21_d_j32_dev = thrust::device_vector<float>(n_samples);
      auto d_g21_d_j33_dev = thrust::device_vector<float>(n_samples);

      auto d_g22_d_j11_dev = thrust::device_vector<float>(n_samples);
      auto d_g22_d_j12_dev = thrust::device_vector<float>(n_samples);
      auto d_g22_d_j13_dev = thrust::device_vector<float>(n_samples);
      auto d_g22_d_j21_dev = thrust::device_vector<float>(n_samples);
      auto d_g22_d_j22_dev = thrust::device_vector<float>(n_samples);
      auto d_g22_d_j23_dev = thrust::device_vector<float>(n_samples);
      auto d_g22_d_j31_dev = thrust::device_vector<float>(n_samples);
      auto d_g22_d_j32_dev = thrust::device_vector<float>(n_samples);
      auto d_g22_d_j33_dev = thrust::device_vector<float>(n_samples);

      auto d_g23_d_j11_dev = thrust::device_vector<float>(n_samples);
      auto d_g23_d_j12_dev = thrust::device_vector<float>(n_samples);
      auto d_g23_d_j13_dev = thrust::device_vector<float>(n_samples);
      auto d_g23_d_j21_dev = thrust::device_vector<float>(n_samples);
      auto d_g23_d_j22_dev = thrust::device_vector<float>(n_samples);
      auto d_g23_d_j23_dev = thrust::device_vector<float>(n_samples);
      auto d_g23_d_j31_dev = thrust::device_vector<float>(n_samples);
      auto d_g23_d_j32_dev = thrust::device_vector<float>(n_samples);
      auto d_g23_d_j33_dev = thrust::device_vector<float>(n_samples);

      auto d_g31_d_j11_dev = thrust::device_vector<float>(n_samples);
      auto d_g31_d_j12_dev = thrust::device_vector<float>(n_samples);
      auto d_g31_d_j13_dev = thrust::device_vector<float>(n_samples);
      auto d_g31_d_j21_dev = thrust::device_vector<float>(n_samples);
      auto d_g31_d_j22_dev = thrust::device_vector<float>(n_samples);
      auto d_g31_d_j23_dev = thrust::device_vector<float>(n_samples);
      auto d_g31_d_j31_dev = thrust::device_vector<float>(n_samples);
      auto d_g31_d_j32_dev = thrust::device_vector<float>(n_samples);
      auto d_g31_d_j33_dev = thrust::device_vector<float>(n_samples);

      auto d_g32_d_j11_dev = thrust::device_vector<float>(n_samples);
      auto d_g32_d_j12_dev = thrust::device_vector<float>(n_samples);
      auto d_g32_d_j13_dev = thrust::device_vector<float>(n_samples);
      auto d_g32_d_j21_dev = thrust::device_vector<float>(n_samples);
      auto d_g32_d_j22_dev = thrust::device_vector<float>(n_samples);
      auto d_g32_d_j23_dev = thrust::device_vector<float>(n_samples);
      auto d_g32_d_j31_dev = thrust::device_vector<float>(n_samples);
      auto d_g32_d_j32_dev = thrust::device_vector<float>(n_samples);
      auto d_g32_d_j33_dev = thrust::device_vector<float>(n_samples);

      auto d_g33_d_j11_dev = thrust::device_vector<float>(n_samples);
      auto d_g33_d_j12_dev = thrust::device_vector<float>(n_samples);
      auto d_g33_d_j13_dev = thrust::device_vector<float>(n_samples);
      auto d_g33_d_j21_dev = thrust::device_vector<float>(n_samples);
      auto d_g33_d_j22_dev = thrust::device_vector<float>(n_samples);
      auto d_g33_d_j23_dev = thrust::device_vector<float>(n_samples);
      auto d_g33_d_j31_dev = thrust::device_vector<float>(n_samples);
      auto d_g33_d_j32_dev = thrust::device_vector<float>(n_samples);
      auto d_g33_d_j33_dev = thrust::device_vector<float>(n_samples);

      // Get raw pointers to device data
      auto g11_raw = thrust::raw_pointer_cast(g11_dev.data());
      auto g12_raw = thrust::raw_pointer_cast(g12_dev.data());
      auto g13_raw = thrust::raw_pointer_cast(g13_dev.data());
      auto g22_raw = thrust::raw_pointer_cast(g22_dev.data());
      auto g23_raw = thrust::raw_pointer_cast(g23_dev.data());
      auto g33_raw = thrust::raw_pointer_cast(g33_dev.data());

      auto r11_raw = thrust::raw_pointer_cast(r11_dev.data());
      auto r12_raw = thrust::raw_pointer_cast(r12_dev.data());
      auto r13_raw = thrust::raw_pointer_cast(r13_dev.data());
      auto r21_raw = thrust::raw_pointer_cast(r21_dev.data());
      auto r22_raw = thrust::raw_pointer_cast(r22_dev.data());
      auto r23_raw = thrust::raw_pointer_cast(r23_dev.data());
      auto r31_raw = thrust::raw_pointer_cast(r31_dev.data());
      auto r32_raw = thrust::raw_pointer_cast(r32_dev.data());
      auto r33_raw = thrust::raw_pointer_cast(r33_dev.data());

      auto s11_raw = thrust::raw_pointer_cast(s11_dev.data());
      auto s12_raw = thrust::raw_pointer_cast(s12_dev.data());
      auto s13_raw = thrust::raw_pointer_cast(s13_dev.data());
      auto s22_raw = thrust::raw_pointer_cast(s22_dev.data());
      auto s23_raw = thrust::raw_pointer_cast(s23_dev.data());
      auto s33_raw = thrust::raw_pointer_cast(s33_dev.data());

      auto d_g11_d_j11_raw = thrust::raw_pointer_cast(d_g11_d_j11_dev.data());
      auto d_g11_d_j12_raw = thrust::raw_pointer_cast(d_g11_d_j12_dev.data());
      auto d_g11_d_j13_raw = thrust::raw_pointer_cast(d_g11_d_j13_dev.data());
      auto d_g11_d_j21_raw = thrust::raw_pointer_cast(d_g11_d_j21_dev.data());
      auto d_g11_d_j22_raw = thrust::raw_pointer_cast(d_g11_d_j22_dev.data());
      auto d_g11_d_j23_raw = thrust::raw_pointer_cast(d_g11_d_j23_dev.data());
      auto d_g11_d_j31_raw = thrust::raw_pointer_cast(d_g11_d_j31_dev.data());
      auto d_g11_d_j32_raw = thrust::raw_pointer_cast(d_g11_d_j32_dev.data());
      auto d_g11_d_j33_raw = thrust::raw_pointer_cast(d_g11_d_j33_dev.data());

      auto d_g12_d_j11_raw = thrust::raw_pointer_cast(d_g12_d_j11_dev.data());
      auto d_g12_d_j12_raw = thrust::raw_pointer_cast(d_g12_d_j12_dev.data());
      auto d_g12_d_j13_raw = thrust::raw_pointer_cast(d_g12_d_j13_dev.data());
      auto d_g12_d_j21_raw = thrust::raw_pointer_cast(d_g12_d_j21_dev.data());
      auto d_g12_d_j22_raw = thrust::raw_pointer_cast(d_g12_d_j22_dev.data());
      auto d_g12_d_j23_raw = thrust::raw_pointer_cast(d_g12_d_j23_dev.data());
      auto d_g12_d_j31_raw = thrust::raw_pointer_cast(d_g12_d_j31_dev.data());
      auto d_g12_d_j32_raw = thrust::raw_pointer_cast(d_g12_d_j32_dev.data());
      auto d_g12_d_j33_raw = thrust::raw_pointer_cast(d_g12_d_j33_dev.data());

      auto d_g13_d_j11_raw = thrust::raw_pointer_cast(d_g13_d_j11_dev.data());
      auto d_g13_d_j12_raw = thrust::raw_pointer_cast(d_g13_d_j12_dev.data());
      auto d_g13_d_j13_raw = thrust::raw_pointer_cast(d_g13_d_j13_dev.data());
      auto d_g13_d_j21_raw = thrust::raw_pointer_cast(d_g13_d_j21_dev.data());
      auto d_g13_d_j22_raw = thrust::raw_pointer_cast(d_g13_d_j22_dev.data());
      auto d_g13_d_j23_raw = thrust::raw_pointer_cast(d_g13_d_j23_dev.data());
      auto d_g13_d_j31_raw = thrust::raw_pointer_cast(d_g13_d_j31_dev.data());
      auto d_g13_d_j32_raw = thrust::raw_pointer_cast(d_g13_d_j32_dev.data());
      auto d_g13_d_j33_raw = thrust::raw_pointer_cast(d_g13_d_j33_dev.data());

      auto d_g21_d_j11_raw = thrust::raw_pointer_cast(d_g21_d_j11_dev.data());
      auto d_g21_d_j12_raw = thrust::raw_pointer_cast(d_g21_d_j12_dev.data());
      auto d_g21_d_j13_raw = thrust::raw_pointer_cast(d_g21_d_j13_dev.data());
      auto d_g21_d_j21_raw = thrust::raw_pointer_cast(d_g21_d_j21_dev.data());
      auto d_g21_d_j22_raw = thrust::raw_pointer_cast(d_g21_d_j22_dev.data());
      auto d_g21_d_j23_raw = thrust::raw_pointer_cast(d_g21_d_j23_dev.data());
      auto d_g21_d_j31_raw = thrust::raw_pointer_cast(d_g21_d_j31_dev.data());
      auto d_g21_d_j32_raw = thrust::raw_pointer_cast(d_g21_d_j32_dev.data());
      auto d_g21_d_j33_raw = thrust::raw_pointer_cast(d_g21_d_j33_dev.data());

      auto d_g22_d_j11_raw = thrust::raw_pointer_cast(d_g22_d_j11_dev.data());
      auto d_g22_d_j12_raw = thrust::raw_pointer_cast(d_g22_d_j12_dev.data());
      auto d_g22_d_j13_raw = thrust::raw_pointer_cast(d_g22_d_j13_dev.data());
      auto d_g22_d_j21_raw = thrust::raw_pointer_cast(d_g22_d_j21_dev.data());
      auto d_g22_d_j22_raw = thrust::raw_pointer_cast(d_g22_d_j22_dev.data());
      auto d_g22_d_j23_raw = thrust::raw_pointer_cast(d_g22_d_j23_dev.data());
      auto d_g22_d_j31_raw = thrust::raw_pointer_cast(d_g22_d_j31_dev.data());
      auto d_g22_d_j32_raw = thrust::raw_pointer_cast(d_g22_d_j32_dev.data());
      auto d_g22_d_j33_raw = thrust::raw_pointer_cast(d_g22_d_j33_dev.data());

      auto d_g23_d_j11_raw = thrust::raw_pointer_cast(d_g23_d_j11_dev.data());
      auto d_g23_d_j12_raw = thrust::raw_pointer_cast(d_g23_d_j12_dev.data());
      auto d_g23_d_j13_raw = thrust::raw_pointer_cast(d_g23_d_j13_dev.data());
      auto d_g23_d_j21_raw = thrust::raw_pointer_cast(d_g23_d_j21_dev.data());
      auto d_g23_d_j22_raw = thrust::raw_pointer_cast(d_g23_d_j22_dev.data());
      auto d_g23_d_j23_raw = thrust::raw_pointer_cast(d_g23_d_j23_dev.data());
      auto d_g23_d_j31_raw = thrust::raw_pointer_cast(d_g23_d_j31_dev.data());
      auto d_g23_d_j32_raw = thrust::raw_pointer_cast(d_g23_d_j32_dev.data());
      auto d_g23_d_j33_raw = thrust::raw_pointer_cast(d_g23_d_j33_dev.data());

      auto d_g31_d_j11_raw = thrust::raw_pointer_cast(d_g31_d_j11_dev.data());
      auto d_g31_d_j12_raw = thrust::raw_pointer_cast(d_g31_d_j12_dev.data());
      auto d_g31_d_j13_raw = thrust::raw_pointer_cast(d_g31_d_j13_dev.data());
      auto d_g31_d_j21_raw = thrust::raw_pointer_cast(d_g31_d_j21_dev.data());
      auto d_g31_d_j22_raw = thrust::raw_pointer_cast(d_g31_d_j22_dev.data());
      auto d_g31_d_j23_raw = thrust::raw_pointer_cast(d_g31_d_j23_dev.data());
      auto d_g31_d_j31_raw = thrust::raw_pointer_cast(d_g31_d_j31_dev.data());
      auto d_g31_d_j32_raw = thrust::raw_pointer_cast(d_g31_d_j32_dev.data());
      auto d_g31_d_j33_raw = thrust::raw_pointer_cast(d_g31_d_j33_dev.data());

      auto d_g32_d_j11_raw = thrust::raw_pointer_cast(d_g32_d_j11_dev.data());
      auto d_g32_d_j12_raw = thrust::raw_pointer_cast(d_g32_d_j12_dev.data());
      auto d_g32_d_j13_raw = thrust::raw_pointer_cast(d_g32_d_j13_dev.data());
      auto d_g32_d_j21_raw = thrust::raw_pointer_cast(d_g32_d_j21_dev.data());
      auto d_g32_d_j22_raw = thrust::raw_pointer_cast(d_g32_d_j22_dev.data());
      auto d_g32_d_j23_raw = thrust::raw_pointer_cast(d_g32_d_j23_dev.data());
      auto d_g32_d_j31_raw = thrust::raw_pointer_cast(d_g32_d_j31_dev.data());
      auto d_g32_d_j32_raw = thrust::raw_pointer_cast(d_g32_d_j32_dev.data());
      auto d_g32_d_j33_raw = thrust::raw_pointer_cast(d_g32_d_j33_dev.data());

      auto d_g33_d_j11_raw = thrust::raw_pointer_cast(d_g33_d_j11_dev.data());
      auto d_g33_d_j12_raw = thrust::raw_pointer_cast(d_g33_d_j12_dev.data());
      auto d_g33_d_j13_raw = thrust::raw_pointer_cast(d_g33_d_j13_dev.data());
      auto d_g33_d_j21_raw = thrust::raw_pointer_cast(d_g33_d_j21_dev.data());
      auto d_g33_d_j22_raw = thrust::raw_pointer_cast(d_g33_d_j22_dev.data());
      auto d_g33_d_j23_raw = thrust::raw_pointer_cast(d_g33_d_j23_dev.data());
      auto d_g33_d_j31_raw = thrust::raw_pointer_cast(d_g33_d_j31_dev.data());
      auto d_g33_d_j32_raw = thrust::raw_pointer_cast(d_g33_d_j32_dev.data());
      auto d_g33_d_j33_raw = thrust::raw_pointer_cast(d_g33_d_j33_dev.data());

      // Calculate kernel parameters
      int min_grid_size;
      int block_size;
      cudaOccupancyMaxPotentialBlockSize(
          &min_grid_size,
          &block_size,
          MMORF::kernel_rotation_gxx_grad_jxx_3d,
          0,
          0);
      unsigned int grid_size = (n_samples + block_size - 1)/block_size;
      // Call CUDA Kernel
      MMORF::kernel_rotation_gxx_grad_jxx_3d<<<grid_size, block_size>>>(
          // Input
          n_samples,
          g11_raw,
          g12_raw,
          g13_raw,
          g22_raw,
          g23_raw,
          g33_raw,
          r11_raw,
          r12_raw,
          r13_raw,
          r21_raw,
          r22_raw,
          r23_raw,
          r31_raw,
          r32_raw,
          r33_raw,
          s11_raw,
          s12_raw,
          s13_raw,
          s22_raw,
          s23_raw,
          s33_raw,
          // Output
          d_g11_d_j11_raw,
          d_g11_d_j12_raw,
          d_g11_d_j13_raw,
          d_g11_d_j21_raw,
          d_g11_d_j22_raw,
          d_g11_d_j23_raw,
          d_g11_d_j31_raw,
          d_g11_d_j32_raw,
          d_g11_d_j33_raw,
          d_g12_d_j11_raw,
          d_g12_d_j12_raw,
          d_g12_d_j13_raw,
          d_g12_d_j21_raw,
          d_g12_d_j22_raw,
          d_g12_d_j23_raw,
          d_g12_d_j31_raw,
          d_g12_d_j32_raw,
          d_g12_d_j33_raw,
          d_g13_d_j11_raw,
          d_g13_d_j12_raw,
          d_g13_d_j13_raw,
          d_g13_d_j21_raw,
          d_g13_d_j22_raw,
          d_g13_d_j23_raw,
          d_g13_d_j31_raw,
          d_g13_d_j32_raw,
          d_g13_d_j33_raw,
          d_g22_d_j11_raw,
          d_g22_d_j12_raw,
          d_g22_d_j13_raw,
          d_g22_d_j21_raw,
          d_g22_d_j22_raw,
          d_g22_d_j23_raw,
          d_g22_d_j31_raw,
          d_g22_d_j32_raw,
          d_g22_d_j33_raw,
          d_g23_d_j11_raw,
          d_g23_d_j12_raw,
          d_g23_d_j13_raw,
          d_g23_d_j21_raw,
          d_g23_d_j22_raw,
          d_g23_d_j23_raw,
          d_g23_d_j31_raw,
          d_g23_d_j32_raw,
          d_g23_d_j33_raw,
          d_g33_d_j11_raw,
          d_g33_d_j12_raw,
          d_g33_d_j13_raw,
          d_g33_d_j21_raw,
          d_g33_d_j22_raw,
          d_g33_d_j23_raw,
          d_g33_d_j31_raw,
          d_g33_d_j32_raw,
          d_g33_d_j33_raw);
      checkCudaErrors(cudaDeviceSynchronize());
      // Copy calculated grad data back to host
      auto d_g11_d_j11_host = thrust::host_vector<float>(d_g11_d_j11_dev);
      auto d_g11_d_j12_host = thrust::host_vector<float>(d_g11_d_j12_dev);
      auto d_g11_d_j13_host = thrust::host_vector<float>(d_g11_d_j13_dev);
      auto d_g11_d_j21_host = thrust::host_vector<float>(d_g11_d_j21_dev);
      auto d_g11_d_j22_host = thrust::host_vector<float>(d_g11_d_j22_dev);
      auto d_g11_d_j23_host = thrust::host_vector<float>(d_g11_d_j23_dev);
      auto d_g11_d_j31_host = thrust::host_vector<float>(d_g11_d_j31_dev);
      auto d_g11_d_j32_host = thrust::host_vector<float>(d_g11_d_j32_dev);
      auto d_g11_d_j33_host = thrust::host_vector<float>(d_g11_d_j33_dev);

      auto d_g12_d_j11_host = thrust::host_vector<float>(d_g12_d_j11_dev);
      auto d_g12_d_j12_host = thrust::host_vector<float>(d_g12_d_j12_dev);
      auto d_g12_d_j13_host = thrust::host_vector<float>(d_g12_d_j13_dev);
      auto d_g12_d_j21_host = thrust::host_vector<float>(d_g12_d_j21_dev);
      auto d_g12_d_j22_host = thrust::host_vector<float>(d_g12_d_j22_dev);
      auto d_g12_d_j23_host = thrust::host_vector<float>(d_g12_d_j23_dev);
      auto d_g12_d_j31_host = thrust::host_vector<float>(d_g12_d_j31_dev);
      auto d_g12_d_j32_host = thrust::host_vector<float>(d_g12_d_j32_dev);
      auto d_g12_d_j33_host = thrust::host_vector<float>(d_g12_d_j33_dev);

      auto d_g13_d_j11_host = thrust::host_vector<float>(d_g13_d_j11_dev);
      auto d_g13_d_j12_host = thrust::host_vector<float>(d_g13_d_j12_dev);
      auto d_g13_d_j13_host = thrust::host_vector<float>(d_g13_d_j13_dev);
      auto d_g13_d_j21_host = thrust::host_vector<float>(d_g13_d_j21_dev);
      auto d_g13_d_j22_host = thrust::host_vector<float>(d_g13_d_j22_dev);
      auto d_g13_d_j23_host = thrust::host_vector<float>(d_g13_d_j23_dev);
      auto d_g13_d_j31_host = thrust::host_vector<float>(d_g13_d_j31_dev);
      auto d_g13_d_j32_host = thrust::host_vector<float>(d_g13_d_j32_dev);
      auto d_g13_d_j33_host = thrust::host_vector<float>(d_g13_d_j33_dev);

      auto d_g22_d_j11_host = thrust::host_vector<float>(d_g22_d_j11_dev);
      auto d_g22_d_j12_host = thrust::host_vector<float>(d_g22_d_j12_dev);
      auto d_g22_d_j13_host = thrust::host_vector<float>(d_g22_d_j13_dev);
      auto d_g22_d_j21_host = thrust::host_vector<float>(d_g22_d_j21_dev);
      auto d_g22_d_j22_host = thrust::host_vector<float>(d_g22_d_j22_dev);
      auto d_g22_d_j23_host = thrust::host_vector<float>(d_g22_d_j23_dev);
      auto d_g22_d_j31_host = thrust::host_vector<float>(d_g22_d_j31_dev);
      auto d_g22_d_j32_host = thrust::host_vector<float>(d_g22_d_j32_dev);
      auto d_g22_d_j33_host = thrust::host_vector<float>(d_g22_d_j33_dev);

      auto d_g23_d_j11_host = thrust::host_vector<float>(d_g23_d_j11_dev);
      auto d_g23_d_j12_host = thrust::host_vector<float>(d_g23_d_j12_dev);
      auto d_g23_d_j13_host = thrust::host_vector<float>(d_g23_d_j13_dev);
      auto d_g23_d_j21_host = thrust::host_vector<float>(d_g23_d_j21_dev);
      auto d_g23_d_j22_host = thrust::host_vector<float>(d_g23_d_j22_dev);
      auto d_g23_d_j23_host = thrust::host_vector<float>(d_g23_d_j23_dev);
      auto d_g23_d_j31_host = thrust::host_vector<float>(d_g23_d_j31_dev);
      auto d_g23_d_j32_host = thrust::host_vector<float>(d_g23_d_j32_dev);
      auto d_g23_d_j33_host = thrust::host_vector<float>(d_g23_d_j33_dev);

      auto d_g33_d_j11_host = thrust::host_vector<float>(d_g33_d_j11_dev);
      auto d_g33_d_j12_host = thrust::host_vector<float>(d_g33_d_j12_dev);
      auto d_g33_d_j13_host = thrust::host_vector<float>(d_g33_d_j13_dev);
      auto d_g33_d_j21_host = thrust::host_vector<float>(d_g33_d_j21_dev);
      auto d_g33_d_j22_host = thrust::host_vector<float>(d_g33_d_j22_dev);
      auto d_g33_d_j23_host = thrust::host_vector<float>(d_g33_d_j23_dev);
      auto d_g33_d_j31_host = thrust::host_vector<float>(d_g33_d_j31_dev);
      auto d_g33_d_j32_host = thrust::host_vector<float>(d_g33_d_j32_dev);
      auto d_g33_d_j33_host = thrust::host_vector<float>(d_g33_d_j33_dev);
      // Return cost data
      auto dG_dJ = std::vector<std::vector<std::vector<float> > >(
          6, std::vector<std::vector<float> >(
            9, std::vector<float>(
              n_samples)));

      thrust::copy(
          d_g11_d_j11_host.begin(),
          d_g11_d_j11_host.end(),
          dG_dJ[0][0].begin());
      thrust::copy(
          d_g11_d_j12_host.begin(),
          d_g11_d_j12_host.end(),
          dG_dJ[0][1].begin());
      thrust::copy(
          d_g11_d_j13_host.begin(),
          d_g11_d_j13_host.end(),
          dG_dJ[0][2].begin());
      thrust::copy(
          d_g11_d_j21_host.begin(),
          d_g11_d_j21_host.end(),
          dG_dJ[0][3].begin());
      thrust::copy(
          d_g11_d_j22_host.begin(),
          d_g11_d_j22_host.end(),
          dG_dJ[0][4].begin());
      thrust::copy(
          d_g11_d_j23_host.begin(),
          d_g11_d_j23_host.end(),
          dG_dJ[0][5].begin());
      thrust::copy(
          d_g11_d_j31_host.begin(),
          d_g11_d_j31_host.end(),
          dG_dJ[0][6].begin());
      thrust::copy(
          d_g11_d_j32_host.begin(),
          d_g11_d_j32_host.end(),
          dG_dJ[0][7].begin());
      thrust::copy(
          d_g11_d_j33_host.begin(),
          d_g11_d_j33_host.end(),
          dG_dJ[0][8].begin());

      thrust::copy(
          d_g12_d_j11_host.begin(),
          d_g12_d_j11_host.end(),
          dG_dJ[1][0].begin());
      thrust::copy(
          d_g12_d_j12_host.begin(),
          d_g12_d_j12_host.end(),
          dG_dJ[1][1].begin());
      thrust::copy(
          d_g12_d_j13_host.begin(),
          d_g12_d_j13_host.end(),
          dG_dJ[1][2].begin());
      thrust::copy(
          d_g12_d_j21_host.begin(),
          d_g12_d_j21_host.end(),
          dG_dJ[1][3].begin());
      thrust::copy(
          d_g12_d_j22_host.begin(),
          d_g12_d_j22_host.end(),
          dG_dJ[1][4].begin());
      thrust::copy(
          d_g12_d_j23_host.begin(),
          d_g12_d_j23_host.end(),
          dG_dJ[1][5].begin());
      thrust::copy(
          d_g12_d_j31_host.begin(),
          d_g12_d_j31_host.end(),
          dG_dJ[1][6].begin());
      thrust::copy(
          d_g12_d_j32_host.begin(),
          d_g12_d_j32_host.end(),
          dG_dJ[1][7].begin());
      thrust::copy(
          d_g12_d_j33_host.begin(),
          d_g12_d_j33_host.end(),
          dG_dJ[1][8].begin());

      thrust::copy(
          d_g13_d_j11_host.begin(),
          d_g13_d_j11_host.end(),
          dG_dJ[2][0].begin());
      thrust::copy(
          d_g13_d_j12_host.begin(),
          d_g13_d_j12_host.end(),
          dG_dJ[2][1].begin());
      thrust::copy(
          d_g13_d_j13_host.begin(),
          d_g13_d_j13_host.end(),
          dG_dJ[2][2].begin());
      thrust::copy(
          d_g13_d_j21_host.begin(),
          d_g13_d_j21_host.end(),
          dG_dJ[2][3].begin());
      thrust::copy(
          d_g13_d_j22_host.begin(),
          d_g13_d_j22_host.end(),
          dG_dJ[2][4].begin());
      thrust::copy(
          d_g13_d_j23_host.begin(),
          d_g13_d_j23_host.end(),
          dG_dJ[2][5].begin());
      thrust::copy(
          d_g13_d_j31_host.begin(),
          d_g13_d_j31_host.end(),
          dG_dJ[2][6].begin());
      thrust::copy(
          d_g13_d_j32_host.begin(),
          d_g13_d_j32_host.end(),
          dG_dJ[2][7].begin());
      thrust::copy(
          d_g13_d_j33_host.begin(),
          d_g13_d_j33_host.end(),
          dG_dJ[2][8].begin());

      thrust::copy(
          d_g22_d_j11_host.begin(),
          d_g22_d_j11_host.end(),
          dG_dJ[3][0].begin());
      thrust::copy(
          d_g22_d_j12_host.begin(),
          d_g22_d_j12_host.end(),
          dG_dJ[3][1].begin());
      thrust::copy(
          d_g22_d_j13_host.begin(),
          d_g22_d_j13_host.end(),
          dG_dJ[3][2].begin());
      thrust::copy(
          d_g22_d_j21_host.begin(),
          d_g22_d_j21_host.end(),
          dG_dJ[3][3].begin());
      thrust::copy(
          d_g22_d_j22_host.begin(),
          d_g22_d_j22_host.end(),
          dG_dJ[3][4].begin());
      thrust::copy(
          d_g22_d_j23_host.begin(),
          d_g22_d_j23_host.end(),
          dG_dJ[3][5].begin());
      thrust::copy(
          d_g22_d_j31_host.begin(),
          d_g22_d_j31_host.end(),
          dG_dJ[3][6].begin());
      thrust::copy(
          d_g22_d_j32_host.begin(),
          d_g22_d_j32_host.end(),
          dG_dJ[3][7].begin());
      thrust::copy(
          d_g22_d_j33_host.begin(),
          d_g22_d_j33_host.end(),
          dG_dJ[3][8].begin());

      thrust::copy(
          d_g23_d_j11_host.begin(),
          d_g23_d_j11_host.end(),
          dG_dJ[4][0].begin());
      thrust::copy(
          d_g23_d_j12_host.begin(),
          d_g23_d_j12_host.end(),
          dG_dJ[4][1].begin());
      thrust::copy(
          d_g23_d_j13_host.begin(),
          d_g23_d_j13_host.end(),
          dG_dJ[4][2].begin());
      thrust::copy(
          d_g23_d_j21_host.begin(),
          d_g23_d_j21_host.end(),
          dG_dJ[4][3].begin());
      thrust::copy(
          d_g23_d_j22_host.begin(),
          d_g23_d_j22_host.end(),
          dG_dJ[4][4].begin());
      thrust::copy(
          d_g23_d_j23_host.begin(),
          d_g23_d_j23_host.end(),
          dG_dJ[4][5].begin());
      thrust::copy(
          d_g23_d_j31_host.begin(),
          d_g23_d_j31_host.end(),
          dG_dJ[4][6].begin());
      thrust::copy(
          d_g23_d_j32_host.begin(),
          d_g23_d_j32_host.end(),
          dG_dJ[4][7].begin());
      thrust::copy(
          d_g23_d_j33_host.begin(),
          d_g23_d_j33_host.end(),
          dG_dJ[4][8].begin());

      thrust::copy(
          d_g33_d_j11_host.begin(),
          d_g33_d_j11_host.end(),
          dG_dJ[5][0].begin());
      thrust::copy(
          d_g33_d_j12_host.begin(),
          d_g33_d_j12_host.end(),
          dG_dJ[5][1].begin());
      thrust::copy(
          d_g33_d_j13_host.begin(),
          d_g33_d_j13_host.end(),
          dG_dJ[5][2].begin());
      thrust::copy(
          d_g33_d_j21_host.begin(),
          d_g33_d_j21_host.end(),
          dG_dJ[5][3].begin());
      thrust::copy(
          d_g33_d_j22_host.begin(),
          d_g33_d_j22_host.end(),
          dG_dJ[5][4].begin());
      thrust::copy(
          d_g33_d_j23_host.begin(),
          d_g33_d_j23_host.end(),
          dG_dJ[5][5].begin());
      thrust::copy(
          d_g33_d_j31_host.begin(),
          d_g33_d_j31_host.end(),
          dG_dJ[5][6].begin());
      thrust::copy(
          d_g33_d_j32_host.begin(),
          d_g33_d_j32_host.end(),
          dG_dJ[5][7].begin());
      thrust::copy(
          d_g33_d_j33_host.begin(),
          d_g33_d_j33_host.end(),
          dG_dJ[5][8].begin());

      return dG_dJ;
    }

    /// Calculate the gradient of the cost per spline coefficient
    arma::fvec calculate_grad_per_spline(
        const std::shared_ptr<MMORF::Volume>                  mask_ref,
        const std::shared_ptr<MMORF::Volume>                  mask_mov,
        const std::shared_ptr<MMORF::WarpFieldBSpline>        warp_field,
        const std::vector<std::vector<float> >                sample_positions_ref,
        const std::vector<std::vector<float> >                sample_positions_warp,
        const BASISFIELD::Spline1D<float>                     spline_1D,
        const std::vector<int>                                sample_dimensions,
        const std::vector<float>                              sample_resolution,
        const std::vector<std::vector<std::vector<float> > >& grad_per_jacobian_element,
        const std::vector<std::vector<float> >&               samples_error,
        const std::vector<std::vector<float> >&               sample_positions_warped)
    {
      // Calculate Jacobian determinant for current parametrisation
      auto samples_jac_det = warp_field->jacobian_determinants(sample_positions_warp);
      // Calculate jte_sz
      auto warp_sz = warp_field->get_parameter_size();
      auto jte_sz = warp_sz.first * warp_sz.second;
      // Make a vector for storing the result
      auto jte_vec = std::vector<float>(jte_sz, 0.0f);
      // Loop over all positions in the local Jacobian
      for (auto row = 0; row < warp_sz.first; ++row){
        for (auto col = 0; col < warp_sz.first; ++col){
          auto element_id = (row * warp_sz.first) + col;
          auto samples_pre_mult = std::vector<float>(
              samples_error[0].size(), 0.0f);
          for (auto dt_elem = 0; dt_elem < 6; ++dt_elem){
            auto tmp_pre_mult = std::vector<float>(samples_pre_mult.size());
            // Pre-multiply the error and gradient terms
            auto tensor_elem = dt_elem; // First row of DT
            if (dt_elem > 4) tensor_elem += 3; // Third row of DT
            else if (dt_elem > 2) tensor_elem += 1; // Second row of DT
            std::transform(
                samples_error[tensor_elem].begin(),
                samples_error[tensor_elem].end(),
                grad_per_jacobian_element[dt_elem][element_id].begin(),
                tmp_pre_mult.begin(),
                std::multiplies<float>()
                );
            std::transform(
                samples_pre_mult.begin(),
                samples_pre_mult.end(),
                tmp_pre_mult.begin(),
                samples_pre_mult.begin(),
                std::plus<float>()
                );
            // Account for symmetry
            if (dt_elem == 1 || dt_elem == 2 || dt_elem == 4){
              std::transform(
                  samples_pre_mult.begin(),
                  samples_pre_mult.end(),
                  tmp_pre_mult.begin(),
                  samples_pre_mult.begin(),
                  std::plus<float>()
                  );
            }
          }
          // Pre-multiply by the Jacobian modulation
          std::transform(
              samples_pre_mult.begin(),
              samples_pre_mult.end(),
              samples_jac_det.begin(),
              samples_pre_mult.begin(),
              [](float a, float b) -> float { return 0.5f*(1.0f + b)*a; }
              );
          if (mask_ref){
            auto samples_mask_ref = mask_ref->sample(sample_positions_ref);
            std::transform(
                samples_pre_mult.begin(),
                samples_pre_mult.end(),
                samples_mask_ref.begin(),
                samples_pre_mult.begin(),
                std::multiplies<float>()
                );
          }
          if (mask_mov){
            auto samples_mask_mov = mask_mov->sample(sample_positions_warped);
            std::transform(
                samples_pre_mult.begin(),
                samples_pre_mult.end(),
                samples_mask_mov.begin(),
                samples_pre_mult.begin(),
                std::multiplies<float>()
                );
          }
          // Generate the correct differentiated splines
          auto x_deriv_order = col == 0 ? 1 : 0;
          auto y_deriv_order = col == 1 ? 1 : 0;
          auto z_deriv_order = col == 2 ? 1 : 0;
          auto spline_x = BASISFIELD::Spline1D<float>(
              spline_1D.Order(),
              spline_1D.KnotSpacing(),
              x_deriv_order);
          auto spline_y = BASISFIELD::Spline1D<float>(
              spline_1D.Order(),
              spline_1D.KnotSpacing(),
              y_deriv_order);
          auto spline_z = BASISFIELD::Spline1D<float>(
              spline_1D.Order(),
              spline_1D.KnotSpacing(),
              z_deriv_order);
          auto spline_x_vals = CostFxnSplineUtils::spline_as_vec(
              spline_x,
              sample_resolution[0],
              x_deriv_order);
          auto spline_y_vals = CostFxnSplineUtils::spline_as_vec(
              spline_y,
              sample_resolution[1],
              y_deriv_order);
          auto spline_z_vals = CostFxnSplineUtils::spline_as_vec(
              spline_z,
              sample_resolution[2],
              z_deriv_order);
          auto temp_jte_vec = CostFxnSplineUtils::calculate_sub_grad(
              samples_pre_mult,
              sample_dimensions,
              spline_x_vals,
              spline_y_vals,
              spline_z_vals,
              warp_field->get_dimensions(),
              std::vector<int>(3, spline_1D.KnotSpacing()));
          // Add subgrad to appropriate position
          auto jte_vec_begin_it = jte_vec.begin() + row * warp_sz.second;
          std::transform(
              temp_jte_vec.begin(),
              temp_jte_vec.end(),
              jte_vec_begin_it,
              jte_vec_begin_it,
              std::plus<float>());
        }
      }
      // Return result
      return arma::fvec(jte_vec.data(), jte_vec.size());
    }

    MMORF::SparseDiagonalMatrixTiled calculate_hess_per_spline(
        const std::shared_ptr<MMORF::Volume>                  mask_ref,
        const std::shared_ptr<MMORF::Volume>                  mask_mov,
        const std::shared_ptr<MMORF::WarpFieldBSpline>        warp_field,
        const std::vector<std::vector<float> >                sample_positions_ref,
        const std::vector<std::vector<float> >                sample_positions_warp,
        const BASISFIELD::Spline1D<float>                     spline_1D,
        const std::vector<int>                                sample_dimensions,
        const std::vector<float>                              sample_resolution,
        const std::vector<std::vector<std::vector<float> > >& grad_per_jacobian_element,
        const std::vector<std::vector<float> >&               sample_positions_warped)
    {
      // Create sparse tiled matrix to store sub-Hessian
      auto sparse_jtj = CostFxnSplineUtils::create_empty_jtj(warp_field, CostFxn::HESS_FULL, 3);
      // Get size of warp field
      auto warp_sz = warp_field->get_parameter_size();
      // Calculate Jacobian determinant for current parametrisation
      auto samples_jac_det = warp_field->jacobian_determinants(sample_positions_warp);
      // Calculate the possible combinations of both gradient images and splines. There
      // should be 9 combinations for each of the 9 "sub-Hessians" leading to 81 in total.
      // There is however symmetry involved here. 3 of each of the 9s are redundant (or
      // are the transpose of each other) and therefore there should only be 36 unique
      // sub-hessians to calculate (which is still admittedly a lot).
      //
      // Loop over all warp directions
      for (auto row_hess = 0; row_hess < warp_sz.first; ++row_hess){
        // Loop over all warp directions again
        for (auto col_hess = 0; col_hess < warp_sz.first; ++col_hess){
          // Check if this is a unique sub-jtj - we will keep the main diagonal and below
          //if (col_hess > row_hess) continue;
          // Loop over the 3 Jacobian elements that contribute to the row gradient
          for (auto row_elem = 0; row_elem < warp_sz.first; ++row_elem){
            // Loop over the 3 Jabians elements that contribute to the col gradient
            for (auto col_elem = 0; col_elem < warp_sz.first; ++col_elem){
              // Check if this is a unique combination of elements - we will keep the main
              // diagonal and below
              // if (col_elem > row_elem) continue;
              // Generate the correct differentiated splines
              // Note that the modulo (%) operator converts the "elem" value to the column
              // position of that element in the Jacobian matrix
              auto x_deriv_order_row = row_elem == 0 ? 1 : 0;
              auto y_deriv_order_row = row_elem == 1 ? 1 : 0;
              auto z_deriv_order_row = row_elem == 2 ? 1 : 0;

              auto x_deriv_order_col = col_elem == 0 ? 1 : 0;
              auto y_deriv_order_col = col_elem == 1 ? 1 : 0;
              auto z_deriv_order_col = col_elem == 2 ? 1 : 0;

              auto spline_x_row = BASISFIELD::Spline1D<float>(
                  spline_1D.Order(),
                  spline_1D.KnotSpacing(),
                  x_deriv_order_row);
              auto spline_y_row = BASISFIELD::Spline1D<float>(
                  spline_1D.Order(),
                  spline_1D.KnotSpacing(),
                  y_deriv_order_row);
              auto spline_z_row = BASISFIELD::Spline1D<float>(
                  spline_1D.Order(),
                  spline_1D.KnotSpacing(),
                  z_deriv_order_row);

              auto spline_x_col = BASISFIELD::Spline1D<float>(
                  spline_1D.Order(),
                  spline_1D.KnotSpacing(),
                  x_deriv_order_col);
              auto spline_y_col = BASISFIELD::Spline1D<float>(
                  spline_1D.Order(),
                  spline_1D.KnotSpacing(),
                  y_deriv_order_col);
              auto spline_z_col = BASISFIELD::Spline1D<float>(
                  spline_1D.Order(),
                  spline_1D.KnotSpacing(),
                  z_deriv_order_col);

              auto spline_x_row_vals = CostFxnSplineUtils::spline_as_vec(
                  spline_x_row,
                  sample_resolution[0],
                  x_deriv_order_row);
              auto spline_y_row_vals = CostFxnSplineUtils::spline_as_vec(
                  spline_y_row,
                  sample_resolution[1],
                  y_deriv_order_row);
              auto spline_z_row_vals = CostFxnSplineUtils::spline_as_vec(
                  spline_z_row,
                  sample_resolution[2],
                  z_deriv_order_row);

              auto spline_x_col_vals = CostFxnSplineUtils::spline_as_vec(
                  spline_x_col,
                  sample_resolution[0],
                  x_deriv_order_col);
              auto spline_y_col_vals = CostFxnSplineUtils::spline_as_vec(
                  spline_y_col,
                  sample_resolution[1],
                  y_deriv_order_col);
              auto spline_z_col_vals = CostFxnSplineUtils::spline_as_vec(
                  spline_z_col,
                  sample_resolution[2],
                  z_deriv_order_col);

              // Pre-multiply the gradient_per_jacobian_element_fields
              auto field_pre_mult = std::vector<float>(
                  grad_per_jacobian_element[0][0].size(), 0.0f);
              // Loop over tensor elements
              for (auto dt_elem = 0; dt_elem < 6; ++dt_elem){
                auto tmp_pre_mult = std::vector<float>(
                    field_pre_mult.size());
                auto tensor_elem = dt_elem; // First row of DT
                if (dt_elem > 4) tensor_elem += 3; // Third row of DT
                else if (dt_elem > 2) tensor_elem += 1; // Second row of DT
                std::transform(
                    grad_per_jacobian_element[dt_elem][row_hess*warp_sz.first + row_elem].begin(),
                    grad_per_jacobian_element[dt_elem][row_hess*warp_sz.first + row_elem].end(),
                    grad_per_jacobian_element[dt_elem][col_hess*warp_sz.first + col_elem].begin(),
                    tmp_pre_mult.begin(),
                    std::multiplies<float>()
                    );
                std::transform(
                    field_pre_mult.begin(),
                    field_pre_mult.end(),
                    tmp_pre_mult.begin(),
                    field_pre_mult.begin(),
                    std::plus<float>()
                    );
                // Account for symmetry
                if (dt_elem == 1 || dt_elem == 2 || dt_elem == 4){
                  std::transform(
                      field_pre_mult.begin(),
                      field_pre_mult.end(),
                      tmp_pre_mult.begin(),
                      field_pre_mult.begin(),
                      std::plus<float>()
                      );
                }
              }
              // Pre-multiply by the Jacobian modulation
              std::transform(
                  field_pre_mult.begin(),
                  field_pre_mult.end(),
                  samples_jac_det.begin(),
                  field_pre_mult.begin(),
                  [](float a, float b) -> float { return 0.5f*(1.0f + b)*a; }
                  );
              if (mask_ref){
                auto samples_mask_ref = mask_ref->sample(sample_positions_ref);
                std::transform(
                    field_pre_mult.begin(),
                    field_pre_mult.end(),
                    samples_mask_ref.begin(),
                    field_pre_mult.begin(),
                    std::multiplies<float>()
                    );
              }
              if (mask_mov){
                auto samples_mask_mov = mask_mov->sample(sample_positions_warped);
                std::transform(
                    field_pre_mult.begin(),
                    field_pre_mult.end(),
                    samples_mask_mov.begin(),
                    field_pre_mult.begin(),
                    std::multiplies<float>()
                    );
              }
              // Call kernel for this combination of elements
              CostFxnSplineUtils::calculate_sub_jtj_non_symmetrical(
                // Input
                field_pre_mult,
                sample_dimensions,
                spline_x_row_vals,
                spline_y_row_vals,
                spline_z_row_vals,
                spline_x_col_vals,
                spline_y_col_vals,
                spline_z_col_vals,
                warp_field->get_dimensions(),
                std::vector<int>(3, spline_1D.KnotSpacing()),
                row_hess,
                col_hess,
                sparse_jtj);
            }
          }
        }
      }
      return sparse_jtj;
    }

    MMORF::SparseDiagonalMatrixTiled calculate_hess_per_spline_diag_hess(
        const std::shared_ptr<MMORF::Volume>                  mask_ref,
        const std::shared_ptr<MMORF::Volume>                  mask_mov,
        const std::shared_ptr<MMORF::WarpFieldBSpline>        warp_field,
        const std::vector<std::vector<float> >                sample_positions_ref,
        const std::vector<std::vector<float> >                sample_positions_warp,
        const BASISFIELD::Spline1D<float>                     spline_1D,
        const std::vector<int>                                sample_dimensions,
        const std::vector<float>                              sample_resolution,
        const std::vector<std::vector<std::vector<float> > >& grad_per_jacobian_element,
        const std::vector<std::vector<float> >&               sample_positions_warped)
    {
      // Determine sizes of splines etc.
      auto spline_sz = spline_1D.KernelSize();
      auto field_sz = grad_per_jacobian_element[0][0].size();
      // Create sparse tiled matrix to store sub-Hessian
      auto sparse_jtj = CostFxnSplineUtils::create_empty_jtj(warp_field, CostFxn::HESS_DIAG, 3);
      // Get size of warp field
      auto warp_sz = warp_field->get_parameter_size();
      // Calculate Jacobian determinant for current parametrisation
      auto samples_jac_det = warp_field->jacobian_determinants(sample_positions_warp);
      // Calculate the possible combinations of both gradient images and splines. There
      // should be 9 combinations for each of the 9 "sub-Hessians" leading to 81 in total.
      // There is however symmetry involved here. 3 of each of the 9s are redundant (or
      // are the transpose of each other) and therefore there should only be 36 unique
      // sub-hessians to calculate (which is still admittedly a lot).
      //
      // Loop over all warp directions
      for (auto row_hess = 0; row_hess < warp_sz.first; ++row_hess){
        // Loop over all warp directions again
        for (auto col_hess = 0; col_hess < warp_sz.first; ++col_hess){
          // Check if this is a unique sub-jtj - we will keep the main diagonal and below
          //if (col_hess > row_hess) continue;
          auto field_pre_mult = std::vector<std::vector<float> >(9,
              std::vector<float>(field_sz, 0));

          auto spline_x_row_vals = std::vector<float>(
              warp_sz.first*warp_sz.first*spline_sz, 0);
          auto spline_y_row_vals = std::vector<float>(
              warp_sz.first*warp_sz.first*spline_sz, 0);
          auto spline_z_row_vals = std::vector<float>(
              warp_sz.first*warp_sz.first*spline_sz, 0);

          auto spline_x_col_vals = std::vector<float>(
              warp_sz.first*warp_sz.first*spline_sz, 0);
          auto spline_y_col_vals = std::vector<float>(
              warp_sz.first*warp_sz.first*spline_sz, 0);
          auto spline_z_col_vals = std::vector<float>(
              warp_sz.first*warp_sz.first*spline_sz, 0);

          auto loop_count = 0;
          // Loop over the 3 Jacobian elements that contribute to the row gradient
          for (auto row_elem = 0; row_elem < warp_sz.first; ++row_elem){
            // Loop over the 3 Jabians elements that contribute to the col gradient
            for (auto col_elem = 0; col_elem < warp_sz.first; ++col_elem){
              // Check if this is a unique combination of elements - we will keep the main
              // diagonal and below
              // if (col_elem > row_elem) continue;
              // Generate the correct differentiated splines
              // Note that the modulo (%) operator converts the "elem" value to the column
              // position of that element in the Jacobian matrix
              auto x_deriv_order_row = row_elem == 0 ? 1 : 0;
              auto y_deriv_order_row = row_elem == 1 ? 1 : 0;
              auto z_deriv_order_row = row_elem == 2 ? 1 : 0;

              auto x_deriv_order_col = col_elem == 0 ? 1 : 0;
              auto y_deriv_order_col = col_elem == 1 ? 1 : 0;
              auto z_deriv_order_col = col_elem == 2 ? 1 : 0;

              auto spline_x_row = BASISFIELD::Spline1D<float>(
                  spline_1D.Order(),
                  spline_1D.KnotSpacing(),
                  x_deriv_order_row);
              auto spline_y_row = BASISFIELD::Spline1D<float>(
                  spline_1D.Order(),
                  spline_1D.KnotSpacing(),
                  y_deriv_order_row);
              auto spline_z_row = BASISFIELD::Spline1D<float>(
                  spline_1D.Order(),
                  spline_1D.KnotSpacing(),
                  z_deriv_order_row);

              auto spline_x_col = BASISFIELD::Spline1D<float>(
                  spline_1D.Order(),
                  spline_1D.KnotSpacing(),
                  x_deriv_order_col);
              auto spline_y_col = BASISFIELD::Spline1D<float>(
                  spline_1D.Order(),
                  spline_1D.KnotSpacing(),
                  y_deriv_order_col);
              auto spline_z_col = BASISFIELD::Spline1D<float>(
                  spline_1D.Order(),
                  spline_1D.KnotSpacing(),
                  z_deriv_order_col);

              auto spline_x_row_vals_tmp = CostFxnSplineUtils::spline_as_vec(
                  spline_x_row,
                  sample_resolution[0],
                  x_deriv_order_row);
              auto spline_y_row_vals_tmp = CostFxnSplineUtils::spline_as_vec(
                  spline_y_row,
                  sample_resolution[1],
                  y_deriv_order_row);
              auto spline_z_row_vals_tmp = CostFxnSplineUtils::spline_as_vec(
                  spline_z_row,
                  sample_resolution[2],
                  z_deriv_order_row);

              auto spline_x_col_vals_tmp = CostFxnSplineUtils::spline_as_vec(
                  spline_x_col,
                  sample_resolution[0],
                  x_deriv_order_col);
              auto spline_y_col_vals_tmp = CostFxnSplineUtils::spline_as_vec(
                  spline_y_col,
                  sample_resolution[1],
                  y_deriv_order_col);
              auto spline_z_col_vals_tmp = CostFxnSplineUtils::spline_as_vec(
                  spline_z_col,
                  sample_resolution[2],
                  z_deriv_order_col);

              std::copy(
                  spline_x_row_vals_tmp.begin(),
                  spline_x_row_vals_tmp.end(),
                  std::next(spline_x_row_vals.begin(), loop_count*spline_sz));
              std::copy(
                  spline_y_row_vals_tmp.begin(),
                  spline_y_row_vals_tmp.end(),
                  std::next(spline_y_row_vals.begin(), loop_count*spline_sz));
              std::copy(
                  spline_z_row_vals_tmp.begin(),
                  spline_z_row_vals_tmp.end(),
                  std::next(spline_z_row_vals.begin(), loop_count*spline_sz));

              std::copy(
                  spline_x_col_vals_tmp.begin(),
                  spline_x_col_vals_tmp.end(),
                  std::next(spline_x_col_vals.begin(), loop_count*spline_sz));
              std::copy(
                  spline_y_col_vals_tmp.begin(),
                  spline_y_col_vals_tmp.end(),
                  std::next(spline_y_col_vals.begin(), loop_count*spline_sz));
              std::copy(
                  spline_z_col_vals_tmp.begin(),
                  spline_z_col_vals_tmp.end(),
                  std::next(spline_z_col_vals.begin(), loop_count*spline_sz));

              // Pre-multiply the gradient_per_jacobian_element_fields
              // Loop over tensor elements
              for (auto dt_elem = 0; dt_elem < 6; ++dt_elem){
                auto tmp_pre_mult = std::vector<float>(
                    field_sz);
                auto tensor_elem = dt_elem; // First row of DT
                // Create temporary sparse tiled matrix for this tensor element
                if (dt_elem > 4) tensor_elem += 3; // Third row of DT
                else if (dt_elem > 2) tensor_elem += 1; // Second row of DT
                std::transform(
                    grad_per_jacobian_element[dt_elem][row_hess*warp_sz.first + row_elem].begin(),
                    grad_per_jacobian_element[dt_elem][row_hess*warp_sz.first + row_elem].end(),
                    grad_per_jacobian_element[dt_elem][col_hess*warp_sz.first + col_elem].begin(),
                    tmp_pre_mult.begin(),
                    std::multiplies<float>()
                    );
                std::transform(
                    tmp_pre_mult.begin(),
                    tmp_pre_mult.end(),
                    field_pre_mult[loop_count].begin(),
                    field_pre_mult[loop_count].begin(),
                    std::plus<float>()
                    );
                // Account for symmetry
                if (dt_elem == 1 || dt_elem == 2 || dt_elem == 4){
                  std::transform(
                      tmp_pre_mult.begin(),
                      tmp_pre_mult.end(),
                      field_pre_mult[loop_count].begin(),
                      field_pre_mult[loop_count].begin(),
                      std::plus<float>()
                      );
                }
              }
              // Pre-multiply by the Jacobian modulation
              // NB!!! NB!!! Note that a and b are swapped around here relative to all the
              // other places that we use this lambda!
              std::transform(
                  samples_jac_det.begin(),
                  samples_jac_det.end(),
                  field_pre_mult[loop_count].begin(),
                  field_pre_mult[loop_count].begin(),
                  [](float b, float a) -> float { return 0.5f*(1.0f + b)*a; }
                  );
              if (mask_ref){
                auto samples_mask_ref = mask_ref->sample(sample_positions_ref);
                std::transform(
                    samples_mask_ref.begin(),
                    samples_mask_ref.end(),
                    field_pre_mult[loop_count].begin(),
                    field_pre_mult[loop_count].begin(),
                    std::multiplies<float>()
                    );
              }
              if (mask_mov){
                auto samples_mask_mov = mask_mov->sample(sample_positions_warped);
                std::transform(
                    samples_mask_mov.begin(),
                    samples_mask_mov.end(),
                    field_pre_mult[loop_count].begin(),
                    field_pre_mult[loop_count].begin(),
                    std::multiplies<float>()
                    );
              }
              // Increase loop counter
              ++loop_count;
            }
          }
          // Call kernel for this combination of elements
          CostFxnSplineUtils::calculate_sub_jtj_non_symmetrical_diag_hess(
            // Input
            field_pre_mult,
            sample_dimensions,
            spline_x_row_vals,
            spline_y_row_vals,
            spline_z_row_vals,
            spline_x_col_vals,
            spline_y_col_vals,
            spline_z_col_vals,
            warp_field->get_dimensions(),
            std::vector<int>(3, spline_1D.KnotSpacing()),
            row_hess,
            col_hess,
            sparse_jtj);
        }
      }
      return sparse_jtj;
    }

  } // namespace CostFxnTensorL2WarpFieldSymmetricMaskedExcludedUtils
} // namespace MMORF
