//////////////////////////////////////////////////////////////////////////////////////////////
/// \file
/// \brief Cost function based on the log of the singular values of the local Jacobian of the
///        warp field
/// \details This cost function is designed primarily for use in regularising warps defined
///          by B-splines, and not as a stand-alone cost function. As the analytical forms of
///          the gradient and Hessian are rather complicated, the code to calculate them was
///          formulated with the help of the Matlab Symbolic Toolbox.
///          The hess_type template parameter controls whether the Hessian calculation uses an
///          approximation whereby it is represented by a single main diagonal made up of the
///          sum of absolute values of each row/column (which is the same thing as H is
///          symmetrical).
///
/// \author Frederik Lange
/// \date October 2018
/// \copyright Copyright (C) 2018 University of Oxford
//////////////////////////////////////////////////////////////////////////////////////////////


#include "CostFxnLogJacobianSingularValues.h"
#include "CostFxnLogJacobianSingularValuesUtils.h"
#include "WarpFieldBSpline.h"
#include "SparseDiagonalMatrixTiled.h"

#include "basisfield/fsl_splines.h"

#include <armadillo>

#include <functional>
#include <memory>
#include <vector>


namespace MMORF
{
////////////////////////////////////////////////////////////////////////////////
// PIMPL Class
////////////////////////////////////////////////////////////////////////////////
  template<MMORF::CostFxn::HessType hess_type>
  class CostFxnLogJacobianSingularValues<hess_type>::Impl
  {
    public:
      //////////////////////////////////////////////////
      // Public functions
      //////////////////////////////////////////////////
      /// Construct by passing in a fully constructed warpfield
      Impl(
          std::shared_ptr<MMORF::WarpFieldBSpline>  warp_field,
          int                                       sampling_frequency)
        : warp_field_(warp_field)
        , sampling_frequency_(sampling_frequency)
        , spline_1D_(3,sampling_frequency)
        , norm_factor_(1.0e3f)
      {
        // Calculate sample resolution
        auto warp_dims = warp_field_->get_dimensions();
        auto warp_extents = warp_field_->get_extents();
        for (auto i = 0; i < warp_dims.size(); ++i){
          auto dim_res =
            warp_field_->get_knot_spacing() / static_cast<float>(sampling_frequency);
          sample_resolution_.push_back(dim_res);
        }
        // Calculate warp sample resolution
        for (auto dim :warp_dims){
          norm_factor_ /= (dim*sampling_frequency);
        }
      }
      /// Get the current value of the parameters
      std::vector<std::vector<float> > get_parameters() const
      {
        return warp_field_->get_parameters();
      }
      /// Set the current value of the parameters
      void set_parameters(
          const std::vector<std::vector<float> >& parameters)
      {
        warp_field_->set_parameters(parameters);
      }
      /// Get cost under current parameterisation
      /// \details The cost in this case is equal to the sum of the square of the log of the
      ///          singular values at each point in the volume
      float cost() const
      {
        return CostFxnLogJacobianSingularValuesUtils::cost(
            warp_field_,
            sampling_frequency_,
            norm_factor_);
      }
      /// Get grad under current parameterisation
      arma::fvec grad() const
      {
        return CostFxnLogJacobianSingularValuesUtils::grad(
            warp_field_,
            spline_1D_,
            sampling_frequency_,
            sample_resolution_,
            norm_factor_);
      }
      /// Get hess under current parameterisation
      MMORF::SparseDiagonalMatrixTiled hess() const
      {
        return CostFxnLogJacobianSingularValuesUtils::hess(
            warp_field_,
            spline_1D_,
            sampling_frequency_,
            sample_resolution_,
            norm_factor_,
            hess_type);
      }

    private:
      //////////////////////////////////////////////////
      // Private datamembers
      //////////////////////////////////////////////////
      std::shared_ptr<MMORF::WarpFieldBSpline> warp_field_;
      BASISFIELD::Spline1D<float>              spline_1D_;
      int                                      sampling_frequency_;
      float                                    norm_factor_;
      std::vector<float>                       sample_resolution_;
  };

////////////////////////////////////////////////////////////////////////////////
// Main Class
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Rule of 5
////////////////////////////////////////////////////////////////////////////////

  /// Default dtor
  template<MMORF::CostFxn::HessType hess_type>
  CostFxnLogJacobianSingularValues<hess_type>::~CostFxnLogJacobianSingularValues() = default;

  /// Move ctor
  template<MMORF::CostFxn::HessType hess_type>
  CostFxnLogJacobianSingularValues<hess_type>::CostFxnLogJacobianSingularValues(
      CostFxnLogJacobianSingularValues<hess_type>&& rhs) = default;

  /// Move assignment operator
  template<MMORF::CostFxn::HessType hess_type>
  CostFxnLogJacobianSingularValues<hess_type>&
  CostFxnLogJacobianSingularValues<hess_type>::operator=(
      CostFxnLogJacobianSingularValues&& rhs) = default;

  /// Copy ctor
  template<MMORF::CostFxn::HessType hess_type>
  CostFxnLogJacobianSingularValues<hess_type>::CostFxnLogJacobianSingularValues(
      const CostFxnLogJacobianSingularValues<hess_type>& rhs)
    : pimpl_(nullptr)
  {
    if (rhs.pimpl_){
      pimpl_ = std::make_unique<Impl>(*rhs.pimpl_);
    }
  }

  /// Copy assignment operator
  template<MMORF::CostFxn::HessType hess_type>
  CostFxnLogJacobianSingularValues<hess_type>&
  CostFxnLogJacobianSingularValues<hess_type>::operator=(
      const CostFxnLogJacobianSingularValues<hess_type>& rhs)
  {
    if (!rhs.pimpl_){
      pimpl_.reset();
    }
    else if (!pimpl_){
      pimpl_ = std::make_unique<Impl>(*rhs.pimpl_);
    }
    else{
      *pimpl_ = *rhs.pimpl_;
    }
    return *this;
  }
////////////////////////////////////////////////////////////////////////////////
// Class Specific Functions
////////////////////////////////////////////////////////////////////////////////
  /// Construct by passing in a fully constructed warpfield
  template<MMORF::CostFxn::HessType hess_type>
  CostFxnLogJacobianSingularValues<hess_type>::CostFxnLogJacobianSingularValues(
      std::shared_ptr<MMORF::WarpFieldBSpline> warp_field,
      int                                      sampling_frequency)
    : pimpl_(std::make_unique<Impl>(
          warp_field,
          sampling_frequency))
  {}
  /// Get the current value of the parameters
  template<MMORF::CostFxn::HessType hess_type>
  std::vector<std::vector<float>>
  CostFxnLogJacobianSingularValues<hess_type>::get_parameters() const
  {
    return pimpl_->get_parameters();
  }
  /// Set the current value of the parameters
  template<MMORF::CostFxn::HessType hess_type>
  void CostFxnLogJacobianSingularValues<hess_type>::set_parameters(
      const std::vector<std::vector<float> >& parameters)
  {
    pimpl_->set_parameters(parameters);
  }
  /// Get cost under current parameterisation
  template<MMORF::CostFxn::HessType hess_type>
  float CostFxnLogJacobianSingularValues<hess_type>::cost() const
  {
    return pimpl_->cost();
  }
  /// Get Jte under current parameterisation
  template<MMORF::CostFxn::HessType hess_type>
  arma::fvec CostFxnLogJacobianSingularValues<hess_type>::grad() const
  {
    return pimpl_->grad();
  }
  /// Get JtJ under current parameterisation
  template<MMORF::CostFxn::HessType hess_type>
  MMORF::SparseDiagonalMatrixTiled
  CostFxnLogJacobianSingularValues<hess_type>::hess() const
  {
    return pimpl_->hess();
  }

  // Template specialisations for full/diag hessian calculation
  template class CostFxnLogJacobianSingularValues<MMORF::CostFxn::HESS_FULL>;
  template class CostFxnLogJacobianSingularValues<MMORF::CostFxn::HESS_DIAG>;
} // MMORF
