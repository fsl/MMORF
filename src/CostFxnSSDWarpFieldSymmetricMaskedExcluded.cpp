//////////////////////////////////////////////////////////////////////////////////////////////
/// \file
/// \brief Cost function based on the sum of squared differences between two volumes, one of
///        which is warped. Additionally, a mask in the ref/mov image domain is used to
///        constrain the area of interest during optimisaton. This is now extended to include
///        a greedy approximation to symmetrisation of the problem by multiplying the cost
///        by (1 + |J|).
/// \details The warp is defined by b-spline field coefficients, one set per dimension in the
///          volume
///          The hess_type template parameter controls whether the Hessian calculation uses
///          an approximation whereby it is represented by a single main diagonal made up of
///          the sum of absolute values of each row/column (which is the same thing as H is
///          symmetrical).
/// \author Frederik Lange
/// \date March 2021
/// \copyright Copyright (C) 2021 University of Oxford
//////////////////////////////////////////////////////////////////////////////////////////////
#include "CostFxnSSDWarpFieldSymmetricMaskedExcluded.h"
#include "CostFxnSSDWarpFieldSymmetricMaskedExcludedUtils.h"
#include "CostFxnSplineUtils.h"
#include "CostFxn.h"
#include "Volume.h"
#include "WarpUtils.h"
#include "WarpFieldBSpline.h"
#include "BiasFieldBSpline.h"
#include "SparseDiagonalMatrixTiled.h"
#include "IntensityMapperPolynomial.h"

#include "basisfield/fsl_splines.h"

#include <armadillo>

#include <memory>
#include <vector>
#include <utility>
#include <cmath>
#include <algorithm>
#include <functional>

namespace MMORF
{
////////////////////////////////////////////////////////////////////////////////
// PIMPL Class
////////////////////////////////////////////////////////////////////////////////
  template<MMORF::CostFxn::HessType hess_type>
  class CostFxnSSDWarpFieldSymmetricMaskedExcluded<hess_type>::Impl
  {
    public:
      //////////////////////////////////////////////////
      // Public functions
      //////////////////////////////////////////////////
      /// Construct by passing in fully constructed volumes, warp and (optional) bias field
      /// \param vol_ref Reference (stationary) volume
      /// \param vol_mov Transformed volume
      /// \param affine_ref Pre-calculated affine transform FROM a common space TO the
      ///                   reference space
      /// \param affine_mov Pre-calculated affine transform FROM a common space TO the
      ///                   moving space
      /// \param mask_ref 3D volume used to mask the reference volume
      /// \param mask_mov 3D volume used to mask the moving volume
      /// \param warp_field Shared pointer to fully formed warp field
      /// \param sampling_frequency How often to sample between B-spline knots. E.g. for:
      ///             knot_spacing        = 10mm
      ///             sampling_frequency  = 5
      ///        warp field will be sampled every 2mm
      /// \param bias_field Shared pointer to fully formed bias field
      Impl(
          std::shared_ptr<MMORF::Volume>           vol_ref,
          std::shared_ptr<MMORF::Volume>           vol_mov,
          const arma::fmat&                        affine_ref,
          const arma::fmat&                        affine_mov,
          std::shared_ptr<MMORF::Volume>           mask_ref,
          std::shared_ptr<MMORF::Volume>           mask_mov,
          std::shared_ptr<MMORF::WarpFieldBSpline> warp_field,
          int                                      sampling_frequency,
          std::shared_ptr<MMORF::BiasFieldBSpline> bias_field
          )
        : vol_ref_(vol_ref)
        , vol_mov_(vol_mov)
        , affine_ref_(affine_ref)
        , affine_mov_(affine_mov)
        , mask_ref_(mask_ref)
        , mask_mov_(mask_mov)
        , warp_field_(warp_field)
        , sample_dimensions_(warp_field_->get_robust_sample_dimensions(sampling_frequency))
        , spline_1D_(3,sampling_frequency) // Cubic spline with the correct no. samples
        , bias_field_(bias_field)
      {
        /// \todo Replace assert with exception
        assert(sampling_frequency > 0);
        sample_positions_warp_ = warp_field_->get_robust_sample_positions(sampling_frequency);
        sample_positions_ref_ = WarpUtils::apply_affine_transform(sample_positions_warp_, affine_ref_);
        // Calculate normalisation values for both volumes
        auto sample_positions_mov = WarpUtils::apply_affine_transform(sample_positions_warp_, affine_mov);
        auto ref_samples = vol_ref_->sample(sample_positions_ref_);
        auto mov_samples = vol_mov_->sample(sample_positions_mov);
        auto ref_mask_samples = std::vector<float>(ref_samples.size(), 1.0f);
        auto mov_mask_samples = std::vector<float>(mov_samples.size(), 1.0f);
        if(mask_ref_){
          ref_mask_samples = mask_ref_->sample(sample_positions_ref_);
        }
        if(mask_mov_){
          mov_mask_samples = mask_mov_->sample(sample_positions_mov);
        }
        norm_factor_ref_ = CostFxnSSDWarpFieldSymmetricMaskedExcludedUtils::calculate_robust_norm_factor(
          ref_samples,
          ref_mask_samples,
          mov_mask_samples
          );
        norm_factor_mov_ = CostFxnSSDWarpFieldSymmetricMaskedExcludedUtils::calculate_robust_norm_factor(
          mov_samples,
          ref_mask_samples,
          mov_mask_samples
          );
      }

      /// Get the current value of the parameters
      std::vector<std::vector<float> > get_parameters() const
      {
        return warp_field_->get_parameters();
      }

      /// Set the current value of the parameters
      void set_parameters(
          const std::vector<std::vector<float> >& parameters)
      {
        warp_field_->set_parameters(parameters);
      }

      /// Get cost under current parameterisation
      float cost() const
      {
        return CostFxnSSDWarpFieldSymmetricMaskedExcludedUtils::cost(
            vol_mov_,
            vol_ref_,
            mask_mov_,
            mask_ref_,
            bias_field_,
            warp_field_,
            affine_mov_,
            norm_factor_mov_,
            norm_factor_ref_,
            sample_positions_ref_,
            sample_positions_warp_);
      }

      /// Get Jte under current parameterisation
      arma::fvec grad() const
      {
        return CostFxnSSDWarpFieldSymmetricMaskedExcludedUtils::grad(
            vol_mov_,
            vol_ref_,
            mask_mov_,
            mask_ref_,
            bias_field_,
            warp_field_,
            affine_mov_,
            norm_factor_mov_,
            norm_factor_ref_,
            sample_positions_ref_,
            sample_positions_warp_,
            sample_dimensions_,
            spline_1D_);
      }
      /// Get JtJ under current parameterisation
      MMORF::SparseDiagonalMatrixTiled hess() const
      {
        return CostFxnSSDWarpFieldSymmetricMaskedExcludedUtils::hess(
            vol_mov_,
            mask_mov_,
            mask_ref_,
            warp_field_,
            affine_mov_,
            norm_factor_mov_,
            sample_positions_ref_,
            sample_positions_warp_,
            sample_dimensions_,
            spline_1D_,
            hess_type);
      }
    private:

      //////////////////////////////////////////////////
      // Private datamembers
      //////////////////////////////////////////////////
      std::shared_ptr<MMORF::Volume>           vol_ref_;
      std::shared_ptr<MMORF::Volume>           vol_mov_;
      arma::fmat                               affine_ref_;
      arma::fmat                               affine_mov_;
      std::shared_ptr<MMORF::Volume>           mask_ref_;
      std::shared_ptr<MMORF::Volume>           mask_mov_;
      std::shared_ptr<MMORF::WarpFieldBSpline> warp_field_;
      std::vector<int>                         sample_dimensions_;
      BASISFIELD::Spline1D<float>              spline_1D_;
      std::vector<std::vector<float> >         sample_positions_warp_;
      std::vector<std::vector<float> >         sample_positions_ref_;
      float                                    norm_factor_ref_;
      float                                    norm_factor_mov_;
      std::shared_ptr<MMORF::BiasFieldBSpline> bias_field_;
      MMORF::IntensityMapperPolynomial         intensity_mapper_;
  };
////////////////////////////////////////////////////////////////////////////////
// Main Class
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Rule of 5
////////////////////////////////////////////////////////////////////////////////
  /// Default dtor
  template<MMORF::CostFxn::HessType hess_type>
  CostFxnSSDWarpFieldSymmetricMaskedExcluded<hess_type>::~CostFxnSSDWarpFieldSymmetricMaskedExcluded() = default;
  /// Move ctor
  template<MMORF::CostFxn::HessType hess_type>
  CostFxnSSDWarpFieldSymmetricMaskedExcluded<hess_type>::CostFxnSSDWarpFieldSymmetricMaskedExcluded(
      CostFxnSSDWarpFieldSymmetricMaskedExcluded<hess_type>&& rhs) = default;
  /// Move assignment operator
  template<MMORF::CostFxn::HessType hess_type>
  CostFxnSSDWarpFieldSymmetricMaskedExcluded<hess_type>&
  CostFxnSSDWarpFieldSymmetricMaskedExcluded<hess_type>::operator=(
      CostFxnSSDWarpFieldSymmetricMaskedExcluded<hess_type>&& rhs) = default;
  /// Copy ctor
  template<MMORF::CostFxn::HessType hess_type>
  CostFxnSSDWarpFieldSymmetricMaskedExcluded<hess_type>::CostFxnSSDWarpFieldSymmetricMaskedExcluded(
      const CostFxnSSDWarpFieldSymmetricMaskedExcluded<hess_type>& rhs)
    : pimpl_(nullptr)
  {
    if (rhs.pimpl_){
      pimpl_ = std::make_unique<Impl>(*rhs.pimpl_);
    }
  }
  /// Copy assignment operator
  template<MMORF::CostFxn::HessType hess_type>
  CostFxnSSDWarpFieldSymmetricMaskedExcluded<hess_type>&
  CostFxnSSDWarpFieldSymmetricMaskedExcluded<hess_type>::operator=(
      const CostFxnSSDWarpFieldSymmetricMaskedExcluded<hess_type>& rhs)
  {
    if (!rhs.pimpl_){
      pimpl_.reset();
    }
    else if (!pimpl_){
      pimpl_ = std::make_unique<Impl>(*rhs.pimpl_);
    }
    else{
      *pimpl_ = *rhs.pimpl_;
    }
    return *this;
  }
////////////////////////////////////////////////////////////////////////////////
// Class Specific Functions
////////////////////////////////////////////////////////////////////////////////
  /// Construct by passing in fully constructed volumes, warp and (optional) bias field
  /// \param vol_ref Reference (stationary) volume
  /// \param vol_mov Transformed volume
  /// \param affine_ref Pre-calculated affine transform FROM a common space TO the
  ///                   reference space
  /// \param affine_mov Pre-calculated affine transform FROM a common space TO the
  ///                   moving space
  /// \param mask_ref 3D volume used to mask the reference volume
  /// \param mask_mov 3D volume used to mask the moving volume
  /// \param warp_field Shared pointer to fully formed warp field
  /// \param sampling_frequency How often to sample between B-spline knots. E.g. for:
  ///             knot_spacing        = 10mm
  ///             sampling_frequency  = 5
  ///        warp field will be sampled every 2mm
  /// \param bias_field Shared pointer to fully formed bias field
  template<MMORF::CostFxn::HessType hess_type>
  CostFxnSSDWarpFieldSymmetricMaskedExcluded<hess_type>::CostFxnSSDWarpFieldSymmetricMaskedExcluded(
          std::shared_ptr<MMORF::Volume>           vol_ref,
          std::shared_ptr<MMORF::Volume>           vol_mov,
          const arma::fmat&                        affine_ref,
          const arma::fmat&                        affine_mov,
          std::shared_ptr<MMORF::Volume>           mask_ref,
          std::shared_ptr<MMORF::Volume>           mask_mov,
          std::shared_ptr<MMORF::WarpFieldBSpline> warp_field,
          int                                      sampling_frequency,
          std::shared_ptr<MMORF::BiasFieldBSpline> bias_field
          )
    : pimpl_(std::make_unique<Impl>(
          vol_ref,
          vol_mov,
          affine_ref,
          affine_mov,
          mask_ref,
          mask_mov,
          warp_field,
          sampling_frequency,
          bias_field)
        )
  {}
  /// Get the current value of the parameters
  template<MMORF::CostFxn::HessType hess_type>
  std::vector<std::vector<float> >
  CostFxnSSDWarpFieldSymmetricMaskedExcluded<hess_type>::get_parameters() const
  {
    return pimpl_->get_parameters();
  }
  /// Set the current value of the parameters
  template<MMORF::CostFxn::HessType hess_type>
  void CostFxnSSDWarpFieldSymmetricMaskedExcluded<hess_type>::set_parameters(
      const std::vector<std::vector<float> >& parameters)
  {
    pimpl_->set_parameters(parameters);
  }
  /// Get cost under current parameterisation
  template<MMORF::CostFxn::HessType hess_type>
  float CostFxnSSDWarpFieldSymmetricMaskedExcluded<hess_type>::cost() const
  {
    return pimpl_->cost();
  }
  /// Get Jte under current parameterisation
  template<MMORF::CostFxn::HessType hess_type>
  arma::fvec CostFxnSSDWarpFieldSymmetricMaskedExcluded<hess_type>::grad() const
  {
    return pimpl_->grad();
  }
  /// Get JtJ under current parameterisation
  template<MMORF::CostFxn::HessType hess_type>
  MMORF::SparseDiagonalMatrixTiled
  CostFxnSSDWarpFieldSymmetricMaskedExcluded<hess_type>::hess() const
  {
    return pimpl_->hess();
  }

  // Template specialisations for full hession (false)/diag hessian
  // (true) calculation
  template class CostFxnSSDWarpFieldSymmetricMaskedExcluded<MMORF::CostFxn::HESS_FULL>;
  template class CostFxnSSDWarpFieldSymmetricMaskedExcluded<MMORF::CostFxn::HESS_DIAG>;

} // MMORF
