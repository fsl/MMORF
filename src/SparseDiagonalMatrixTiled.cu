//////////////////////////////////////////////////////////////////////////////////////////////
/// \file
/// \brief Stores a (lxm)x(lxn) sparse matrix as an lxl set of mxn sub-matrices with p
///        non-zero diagonals per sub-matrix.
/// \details This matrix makes no assumptions regarding symmetry or order of diagonals, but
///          these are important factors to consider when using the raw underlying values in
///          cuda kernels.
///
///          Example of how storage works for 3x3 matrix:
///
///               [1 2 3]    0 0[1 2 3]       [0 0 1 2 3]
///           A = [4 5 6] =>   0[4 5 6]0   => [0 4 5 6 0] + [-2 -1 0 1 2]
///               [7 8 9]       [7 8 9]0 0    [7 8 9 0 0]
///
///          I.e. the underlying data storage contains an mxp matrix where each column
///          contains the values of one of the diagonals, and the p-length vector contains
///          the offset of the diagonal into the original matrix, with 0 representing the main
///          diagonal, -ve for lower diagonals, and +ve for upper diagonals.
/// \author Frederik Lange
/// \date March 2018
/// \copyright Copyright (C) 2018 University of Oxford
//////////////////////////////////////////////////////////////////////////////////////////////
#undef NDEBUG
#include "SparseDiagonalMatrixTiled.h"
#include "helper_cuda.cuh"

#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include <thrust/transform.h>
#include <thrust/functional.h>
#include <thrust/iterator/constant_iterator.h>

#include <armadillo>

#include <vector>
#include <iterator>
#include <algorithm>

#include <memory>
#include <numeric>
#include <iostream>
#include <string>
#include <exception>
#include <fstream>
#include <functional>
#include <cmath>

namespace MMORF
{
////////////////////////////////////////////////////////////////////////////////
// __global__ functions
////////////////////////////////////////////////////////////////////////////////

  // Perform RHS matrix * vector multiplication
  // on one row of a sparse matrix tile
  __global__ void kernel_matmul_rhs_vector(
    // Input
    const unsigned int        n_rows,
    const unsigned int        n_cols,
    const unsigned int        n_diags,
    const unsigned int        n_tiles,
    const unsigned int        tile_i,
    const unsigned int        tile_j,
    const int*   __restrict__ offsets,
    const float* __restrict__ tile,
    const float* __restrict__ vec,

    // Output
    float* __restrict__       out
  )
  {
    // The kernel is called for each row of a sparse matrix tile
    auto tile_row   = blockIdx.x * blockDim.x + threadIdx.x;
    auto total_rows = n_rows * n_tiles;
    auto total_cols = n_cols * n_tiles;

    if (tile_row >= n_rows) {
      return;
    }

    for (auto diagi = 0; diagi < n_diags; diagi++) {

      auto tile_col = tile_row + offsets[diagi];
      int  full_row = tile_row + tile_i * n_rows;
      int  full_col = tile_col + tile_j * n_cols;

      if (full_row < 0           ||
          full_col < 0           ||
          full_row >= total_rows ||
          full_col >= total_cols) {
        continue;
      }

      auto mval      = tile[diagi * n_rows + tile_row];
      auto vval      = vec[full_col];
      out[full_row] += mval * vval;
    }
  }

////////////////////////////////////////////////////////////////////////////////
// PIMPL Class
////////////////////////////////////////////////////////////////////////////////
  class SparseDiagonalMatrixTiled::Impl
  {
    public:
      // Basic constructor
      Impl(
          unsigned int n_rows,
          unsigned int n_cols,
          unsigned int n_tiles,
          const std::vector<int>& offsets)
        :n_rows_(n_rows)
        ,n_cols_(n_cols)
        ,n_tiles_(n_tiles)
        ,offsets_(offsets)
        ,d_values_(
            n_tiles,
            std::vector<thrust::device_vector<float> >(
              n_tiles,
              thrust::device_vector<float>(
                offsets_.size()*n_rows_,
                0.0)))
        ,h_values_(
            n_tiles,
            std::vector<thrust::host_vector<float> >(
              n_tiles))
        ,gpu_needs_update_(false)
      {}

      // Create from an armadillo matrix (must be square)
      Impl(const arma::fmat&       mat,
           unsigned int            n_tiles,
           const std::vector<int>& offsets)
        : Impl(mat.n_rows / n_tiles,
               mat.n_cols / n_tiles,
               n_tiles,
               offsets)
      {

        if ((mat.n_rows % n_tiles != 0) ||
            (mat.n_cols % n_tiles != 0)) {
          throw std::runtime_error("Matrix cannot be tiled!");
        }

        auto n_diags = offsets_.size();

        for (auto tile_i = 0; tile_i < n_tiles; tile_i++) {
        for (auto tile_j = 0; tile_j < n_tiles; tile_j++) {

          // create tile on host, then copy to device afterwards
          thrust::host_vector<float> tile(n_diags * n_rows_, 0);

          for (auto tile_row = 0; tile_row < n_rows_; tile_row++) {

            for (auto diagi = 0; diagi < n_diags; diagi++) {

              int tile_col = tile_row + offsets_[diagi];
              int full_row = tile_row + tile_i * n_rows_;
              int full_col = tile_col + tile_j * n_cols_;

              if (full_row <  ( tile_i *      n_rows_) ||
                  full_col <  ( tile_j *      n_cols_) ||
                  full_row >= ((tile_i + 1) * n_rows_) ||
                  full_col >= ((tile_j + 1) * n_cols_)) {
                continue;
              }

              tile[diagi * n_rows_ + tile_row] = mat(full_row, full_col);
            }
          }
          // copy current tile from host to device
          d_values_[tile_i][tile_j] = tile;
        }
      }}

      unsigned int n_rows()  const { return n_rows_; }
      unsigned int n_cols()  const { return n_cols_; }
      unsigned int n_tiles() const { return n_tiles_; }

      /// Return vector of values along specified diagonal
      arma::fcolvec diag(int offset) const
      {

        if (offset != 0) {
          throw std::runtime_error(
              "Access to off-main diagonals not implemented yet!");
        }

        auto offi = std::find(
            offsets_.begin(),
            offsets_.end(),
            offset) - offsets_.begin();

        arma::fcolvec d(n_rows_ * n_tiles_ - std::abs(offset));

        for (auto tile_row = 0; tile_row < d_values_.size(); tile_row++) {

          auto tile   = d_values_[tile_row][tile_row];
          auto tstart = tile.begin() + n_rows_ * offi;
          auto tend   = tstart       + n_rows_;
          auto dstart = d.begin_row(tile_row * n_rows_);

          thrust::copy(tstart, tend, dstart);
        }

        return d;
      }

      /// Overload the += operator
      /// \todo Replace asserts with exceptions
      /// \todo Allow for matrices with different non-zero diagonals
      Impl& operator+=(const Impl& r_impl)
      {
        // Check gpu data are up to date for both matrices
        // Check matrix dimensions match
        assert(
            this->n_rows_ == r_impl.n_rows_
            && this->n_cols_ == r_impl.n_cols_
            && this->n_tiles_ == r_impl.n_tiles_);
        // Check diagonals are identical
        assert(this->offsets_.size() == r_impl.offsets_.size());
        auto diagonals_match = true;
        for (auto i = 0; i < this->offsets_.size(); ++i){
          if (this->offsets_[i] != r_impl.offsets_[i]){
            diagonals_match = false;
          }
        }
        assert(diagonals_match);
        // If all tests pass, add values
        if (this->gpu_needs_update_){
          this->update_gpu_();
        }
        if (r_impl.gpu_needs_update_){
          r_impl.update_gpu_();
        }
        // Copy left hand side matrix
        for (auto row = 0; row < this->d_values_.size(); ++row){
          for (auto col = 0; col < this->d_values_[row].size(); ++col){
          thrust::transform(
              this->d_values_[row][col].begin(),
              this->d_values_[row][col].end(),
              r_impl.d_values_[row][col].begin(),
              this->d_values_[row][col].begin(),
              thrust::plus<float>());
          }
        }
        return *this;
      }
      /// Overload the *= operator
      Impl& operator*=(const float r_float)
      {
        if (this->gpu_needs_update_){
          this->update_gpu_();
        }
        for (auto row = 0; row < this->d_values_.size(); ++row){
          for (auto col = 0; col < this->d_values_[row].size(); ++col){
            thrust::transform(
                this->d_values_[row][col].begin(),
                this->d_values_[row][col].end(),
                thrust::make_constant_iterator(r_float),
                this->d_values_[row][col].begin(),
                thrust::multiplies<float>());
          }
        }
        return *this;
      }
      /// Overload the /= operator
      Impl& operator/=(const float r_float)
      {
        if (this->gpu_needs_update_){
          this->update_gpu_();
        }
        for (auto row = 0; row < this->d_values_.size(); ++row){
          for (auto col = 0; col < this->d_values_[row].size(); ++col){
            thrust::transform(
                this->d_values_[row][col].begin(),
                this->d_values_[row][col].end(),
                thrust::make_constant_iterator(r_float),
                this->d_values_[row][col].begin(),
                thrust::divides<float>());
          }
        }
        return *this;
      }

      // Get a pointer to the matrix values
      float *get_raw_pointer(
          unsigned int tile_row,
          unsigned int tile_col)
      {
        if (gpu_needs_update_){
          update_gpu_();
        }
        /// \todo Replace assert with exception
        assert(tile_row < n_tiles_ && tile_col < n_tiles_);
        return thrust::raw_pointer_cast(
            d_values_[tile_row][tile_col].data());
      } // get_raw_pointer

      // Get a pointer to the matrix values
      const float *get_const_raw_pointer(
          unsigned int tile_row,
          unsigned int tile_col)
      {
        return get_raw_pointer(tile_row, tile_col);
      } // get_const_raw_pointer

      // Get the offsets of the diagonals into the matrix
      std::vector<int> get_offsets() const
      {
        return offsets_;
      } // get_offsets
      /// Convert to the armadillo SpMat compressed sparse column format
      arma::sp_fmat convert_to_csc() const
      {
        if (gpu_needs_update_){
          update_gpu_();
        }
        // Row, column, value triplets.
        auto col_ptrs_a = arma::uvec(n_cols_*n_tiles_ + 1);
        auto row_inds_a = arma::uvec(d_values_[0][0].size()*n_tiles_*n_tiles_);
        auto mat_vals_a = arma::fvec(d_values_[0][0].size()*n_tiles_*n_tiles_);
        // Initialise row pointers
        col_ptrs_a[0] = 0;
        auto val_count = arma::uword(0);
        // Loop through tile cols
        for (auto tile_col = 0; tile_col < n_tiles_; ++tile_col){
          // Copy device data to host
          auto d_values_h = std::vector<thrust::host_vector<float> >(n_tiles_);
          for (auto i = 0; i < n_tiles_; ++i){
            d_values_h[i] = d_values_[i][tile_col];
          }
          // Loop through cols
          for (auto c_i = 0; c_i < n_cols_; ++c_i){
            // Cumulative sum
            col_ptrs_a[tile_col*n_cols_ + c_i + 1] = col_ptrs_a[tile_col*n_cols_ + c_i];
            // Loop through tile rows
            for (auto tile_row = 0; tile_row < n_tiles_; ++tile_row){
              // Loop through diagonals BACKWARDS
              //for (auto d_i = offsets_.size() - 1; d_i >= 0; --d_i){
              for (auto d_i = 0; d_i < offsets_.size(); ++d_i){
                // Row index for this value in this diagonal
                auto d_i_mirror = offsets_.size() - d_i - 1;
                auto r_i = static_cast<int>(c_i) - offsets_[d_i_mirror];
                // If valid row
                if (r_i >= 0 && r_i < n_rows_){
                  // Increment col ptr
                  col_ptrs_a[tile_col*n_cols_ + c_i + 1] += 1;
                  // Store row index
                  row_inds_a[val_count] =
                    tile_row*n_rows_ + static_cast<arma::uword>(r_i);
                  // Store Value
                  mat_vals_a[val_count] =
                    d_values_h[tile_row][d_i_mirror*n_rows_ + static_cast<arma::uword>(r_i)];
                  ++val_count;
                }
              }
            }
          }
        }
        row_inds_a.resize(val_count);
        mat_vals_a.resize(val_count);
        // Actually create the matrix
        auto csc_mat = arma::sp_fmat(
            row_inds_a,
            col_ptrs_a,
            mat_vals_a,
            n_rows_*n_tiles_,
            n_cols_*n_tiles_);
        return csc_mat;
      } // convert_to_csc

      /// Convert to a full armadillo matrix (Only possible for small matrices)
      arma::fmat convert_to_arma() const {

        auto total_rows = n_rows_ * n_tiles_;
        auto total_cols = n_cols_ * n_tiles_;
        auto n_diags    = offsets_.size();

        arma::fmat fullmat(total_rows, total_cols);

        for (auto tile_i = 0; tile_i < n_tiles_; tile_i++) {
        for (auto tile_j = 0; tile_j < n_tiles_; tile_j++) {

          auto tile = d_values_[tile_i][tile_j];

          for (auto diagi = 0; diagi < n_diags; diagi++) {

            for (auto tile_row = 0; tile_row < n_rows_; tile_row++) {

              int tile_col = tile_row + offsets_[diagi];
              int full_row = tile_row + tile_i * n_rows_;
              int full_col = tile_col + tile_j * n_cols_;

              // I'm sure there must be a smarter
              // way to skip entire diagonals,
              // by determining whether a diagonal
              // intersects with a tile
              if (full_row <  ( tile_i *      n_rows_) ||
                  full_col <  ( tile_j *      n_cols_) ||
                  full_row >= ((tile_i + 1) * n_rows_) ||
                  full_col >= ((tile_j + 1) * n_cols_)) {
                continue;
              }

              fullmat(full_row, full_col) = tile[diagi * n_rows_ + tile_row];
            }
          }
        }}
        return fullmat;
      }

      /// Copy one submatrix's values to another
      void copy_submatrix(
          unsigned int from_row,
          unsigned int from_col,
          unsigned int to_row,
          unsigned int to_col)
      {
        if (gpu_needs_update_){
          update_gpu_();
        }
        /// \todo Replace with exception
        assert (
            from_row < n_tiles_ &&
            from_col < n_tiles_ &&
            to_row < n_tiles_ &&
            to_col < n_tiles_);
        d_values_[to_row][to_col] = d_values_[from_row][from_col];
      } // copy_submatrix
      /// Scale the value of a diagonal
      void scale_main_diagonal(const float scaling)
      {
        if (gpu_needs_update_){
          update_gpu_();
        }
        // Find which entry corrseponds to the main diagonal i.e. offset == 0
        auto offset_it = std::find(offsets_.begin(), offsets_.end(), 0);
        // If the main diagonal exists
        if (offset_it != offsets_.end()){
          // Convert from iterator to index
          auto offset_ind = std::distance(offsets_.begin(), offset_it);
          // Scale diagonal for each tiled matrix on the main diagonal
          // (i.e. row_tile == col_tile)
          for (auto tile = 0; tile < n_tiles_; ++tile){
            auto diag_begin = d_values_[tile][tile].begin();
            thrust::advance(diag_begin, offset_ind * n_rows_);
            auto diag_end = diag_begin;
            thrust::advance(diag_end, n_rows_);
            thrust::transform(
                diag_begin,
                diag_end,
                thrust::make_constant_iterator(scaling),
                diag_begin,
                thrust::multiplies<float>());
          }
        }
      }
      /// Add value to the main diagonal (for Levenberg optimisation)
      void add_to_main_diagonal(const float add_value)
      {
        if (gpu_needs_update_){
          update_gpu_();
        }
        // Find which entry corrseponds to the main diagonal i.e. offset == 0
        auto offset_it = std::find(offsets_.begin(), offsets_.end(), 0);
        // If the main diagonal exists
        if (offset_it != offsets_.end()){
          // Convert from iterator to index
          auto offset_ind = std::distance(offsets_.begin(), offset_it);
          // Add to main diagonal for each tiled matrix on the main diagonal
          // (i.e. row_tile == col_tile)
          for (auto tile = 0; tile < n_tiles_; ++tile){
            auto diag_begin = d_values_[tile][tile].begin();
            thrust::advance(diag_begin, offset_ind * n_rows_);
            auto diag_end = diag_begin;
            thrust::advance(diag_end, n_rows_);
            thrust::transform(
                diag_begin,
                diag_end,
                thrust::make_constant_iterator(add_value),
                diag_begin,
                thrust::plus<float>());
          }
        }
      }

      /// Read value from matrix using row, col access
      float get_element(
          unsigned int row,
          unsigned int col) const
      {
        if (!gpu_needs_update_){
          update_cpu_();
        }
        auto tile_row = row/n_rows_;
        auto tile_col = col/n_cols_;
        auto sub_row = row%n_rows_;
        auto sub_col = col%n_cols_;
        auto diag_offset = static_cast<int>(sub_col) - static_cast<int>(sub_row);
        auto diag_it = std::find(offsets_.begin(), offsets_.end(), diag_offset);
        assert (diag_it != offsets_.end());
        auto diag_index = static_cast<unsigned int>(diag_it - offsets_.begin());
        return h_values_[tile_row][tile_col][(diag_index*n_rows_) + sub_row];
      }
      /// Set value in matrix using row, col access
      void set_element(
          unsigned int row,
          unsigned int col,
          float        val)
      {
        if (!gpu_needs_update_){
          update_cpu_();
        }
        auto tile_row = row/n_rows_;
        auto tile_col = col/n_cols_;
        auto sub_row = row%n_rows_;
        auto sub_col = col%n_cols_;
        auto diag_offset = static_cast<int>(sub_col) - static_cast<int>(sub_row);
        auto diag_it = std::find(offsets_.begin(), offsets_.end(), diag_offset);
        assert (diag_it != offsets_.end());
        auto diag_index = static_cast<unsigned int>(diag_it - offsets_.begin());
        h_values_[tile_row][tile_col][(diag_index*n_rows_) + sub_row] = val;
      }

    private:

      /// Copy data from the CPU to GPU
      void update_gpu_() const
      {
        for (size_t row_i = 0; row_i < d_values_.size(); ++row_i){
          for (size_t col_i = 0; col_i < d_values_[row_i].size() ; ++col_i){
            d_values_[row_i][col_i] = h_values_[row_i][col_i];
          }
        }
        gpu_needs_update_ = false;
      }

      /// Copy data from the GPU to CPU
      void update_cpu_() const
      {
        for (size_t row_i = 0; row_i < d_values_.size(); ++row_i){
          for (size_t col_i = 0; col_i < d_values_[row_i].size() ; ++col_i){
            h_values_[row_i][col_i] = d_values_[row_i][col_i];
          }
        }
        gpu_needs_update_ = true;
      }


      //////////////////////////////////////////////////////////////////////////
      // Private datamembers
      //////////////////////////////////////////////////////////////////////////
      unsigned int                                              n_rows_;
      unsigned int                                              n_cols_;
      unsigned int                                              n_tiles_;
      /// The starting column for each diagonal
      std::vector<int>                                          offsets_;
      /// The values of the matrix stored on the GPU (device)
      mutable std::vector<std::vector<thrust::device_vector<float> > >  d_values_;
      /// The values of the matrix stored on the CPU (host)
      mutable std::vector<std::vector<thrust::host_vector<float> > >    h_values_;
      /// Keep track of whether the GPU data is up to date
      mutable bool                                                      gpu_needs_update_;
  };
////////////////////////////////////////////////////////////////////////////////
// Main Class
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Rule of 5
////////////////////////////////////////////////////////////////////////////////
  /// Default dtor
  SparseDiagonalMatrixTiled::~SparseDiagonalMatrixTiled() = default;
  /// Move ctor
  SparseDiagonalMatrixTiled::SparseDiagonalMatrixTiled(SparseDiagonalMatrixTiled&& rhs) = default;
  /// Move assignment operator
  SparseDiagonalMatrixTiled& SparseDiagonalMatrixTiled::operator=(SparseDiagonalMatrixTiled&& rhs) = default;
  /// Copy ctor
  SparseDiagonalMatrixTiled::SparseDiagonalMatrixTiled(const SparseDiagonalMatrixTiled& rhs)
  : pimpl_(nullptr)
  {
    if (rhs.pimpl_){
      pimpl_ = std::make_unique<Impl>(*rhs.pimpl_);
    }
  }
  /// Copy assignment operator
  SparseDiagonalMatrixTiled& SparseDiagonalMatrixTiled::operator=(const SparseDiagonalMatrixTiled& rhs)
  {
    if (!rhs.pimpl_){
      pimpl_.reset();
    }
    else if (!pimpl_){
      pimpl_ = std::make_unique<Impl>(*rhs.pimpl_);
    }
    else{
      *pimpl_ = *rhs.pimpl_;
    }
    return *this;
  }
////////////////////////////////////////////////////////////////////////////////
// Class Specific Functions
////////////////////////////////////////////////////////////////////////////////
  // Basic constructor
  SparseDiagonalMatrixTiled::SparseDiagonalMatrixTiled(
      unsigned int n_rows,
      unsigned int n_cols,
      unsigned int n_tiles,
      const std::vector<int>& offsets)
    : pimpl_(std::make_unique<Impl>(n_rows,n_cols,n_tiles,offsets))
  {}

  /// Create from an armadillo matrix (must be square)
  SparseDiagonalMatrixTiled::SparseDiagonalMatrixTiled(
      const arma::fmat&       mat,
      unsigned int            n_tiles,
      const std::vector<int>& offsets)
    : pimpl_(std::make_unique<Impl>(mat, n_tiles, offsets))
  {}

  unsigned int SparseDiagonalMatrixTiled::n_rows() const
  {
    return pimpl_->n_rows();
  }
  unsigned int SparseDiagonalMatrixTiled::n_cols() const
  {
    return pimpl_->n_cols();
  }
  unsigned int SparseDiagonalMatrixTiled::n_tiles() const
  {
    return pimpl_->n_tiles();
  }

  /// Return vector of values along specified diagonal
  arma::fcolvec SparseDiagonalMatrixTiled::diag(int offset) const
  {
    return pimpl_->diag(offset);
  }

  // Get a pointer to the matrix values
  float *SparseDiagonalMatrixTiled::get_raw_pointer(
      unsigned int tile_row,
      unsigned int tile_col)
  {
    return pimpl_->get_raw_pointer(tile_row, tile_col);
  } // get_raw_pointer

  // Get a pointer to the matrix values
  const float *SparseDiagonalMatrixTiled::get_const_raw_pointer(
      unsigned int tile_row,
      unsigned int tile_col) const
  {
    return pimpl_->get_const_raw_pointer(tile_row, tile_col);
  } // get_raw_pointer

  std::vector<int> SparseDiagonalMatrixTiled::get_offsets() const
  {
    return pimpl_->get_offsets();
  } // get_offsets
  /// Convert to the armadillo SpMat compressed sparse column format
  arma::sp_fmat SparseDiagonalMatrixTiled::convert_to_csc() const
  {
    return pimpl_->convert_to_csc();
  }

  arma::fmat SparseDiagonalMatrixTiled::convert_to_arma() const
  {
    return pimpl_->convert_to_arma();
  }

  /// Copy one submatrix's values to another
  void SparseDiagonalMatrixTiled::copy_submatrix(
      unsigned int from_row,
      unsigned int from_col,
      unsigned int to_row,
      unsigned int to_col)
  {
    pimpl_->copy_submatrix(from_row, from_col, to_row, to_col);
  } // copy_submatrix
  /// Scale the value of a diagonal
  void SparseDiagonalMatrixTiled::scale_main_diagonal(const float scaling)
  {
    pimpl_->scale_main_diagonal(scaling);
  }
  void SparseDiagonalMatrixTiled::add_to_main_diagonal(const float add_value)
  {
    pimpl_->add_to_main_diagonal(add_value);
  }
  /// Read value from matrix using row, col access
  float SparseDiagonalMatrixTiled::get_element(
      unsigned int row,
      unsigned int col) const
  {
    return pimpl_->get_element(row, col);
  }
  /// Set value in matrix using row, col access
  void SparseDiagonalMatrixTiled::set_element(
      unsigned int row,
      unsigned int col,
      float        val)
  {
    pimpl_->set_element(row, col, val);
  }
////////////////////////////////////////////////////////////////////////////////
// Operator overloads
////////////////////////////////////////////////////////////////////////////////
  /// Overload the += operator
  SparseDiagonalMatrixTiled& SparseDiagonalMatrixTiled::operator+=(
      const SparseDiagonalMatrixTiled& r_mat)
  {
    *pimpl_ += *r_mat.pimpl_;
    return *this;
  }
  /// Overload the *= operator
  SparseDiagonalMatrixTiled& SparseDiagonalMatrixTiled::operator*=(
      const float r_float)
  {
    *pimpl_ *= r_float;
    return *this;
  }
  /// Overload the /= operator
  SparseDiagonalMatrixTiled& SparseDiagonalMatrixTiled::operator/=(
      const float r_float)
  {
    *pimpl_ /= r_float;
    return *this;
  }

  /// Overload the + operator
  SparseDiagonalMatrixTiled operator+(
      const SparseDiagonalMatrixTiled& l_mat,
      const SparseDiagonalMatrixTiled& r_mat)
  {
    auto tmp_mat = SparseDiagonalMatrixTiled(l_mat);
    *tmp_mat.pimpl_ += *r_mat.pimpl_;
    return tmp_mat;
  }
  /// Overload the * operator for right hand side float
  SparseDiagonalMatrixTiled operator*(
      const SparseDiagonalMatrixTiled& l_mat,
      const float r_float)
  {
    auto tmp_mat = SparseDiagonalMatrixTiled(l_mat);
    *tmp_mat.pimpl_ *= r_float;
    return tmp_mat;
  }
  /// Overload the * operator for left hand side float
  SparseDiagonalMatrixTiled operator*(
      const float l_float,
      const SparseDiagonalMatrixTiled& r_mat)
  {
    auto tmp_mat = SparseDiagonalMatrixTiled(r_mat);
    *tmp_mat.pimpl_ *= l_float;
    return tmp_mat;
  }
  /// Overload the / operator for right hand side float
  SparseDiagonalMatrixTiled operator/(
      const SparseDiagonalMatrixTiled& l_mat,
      const float r_float)
  {
    auto tmp_mat = SparseDiagonalMatrixTiled(l_mat);
    *tmp_mat.pimpl_ /= r_float;
    return tmp_mat;
  }
  /// Overload the / operator for left hand side float
  SparseDiagonalMatrixTiled operator/(
      const float l_float,
      const SparseDiagonalMatrixTiled& r_mat)
  {
    auto tmp_mat = SparseDiagonalMatrixTiled(r_mat);
    *tmp_mat.pimpl_ /= l_float;
    return tmp_mat;
  }

  /// Overload the * operator for right-hand
  /// side matrix*vector multiplication
  template<typename F>
  arma::fcolvec matmul_rhs_vector(
      const SparseDiagonalMatrixTiled& mat,
      const arma::Mat<F>&              vec)

  {
    auto offsets    = mat.get_offsets();
    auto n_rows     = mat.n_rows();
    auto n_cols     = mat.n_cols();
    auto n_tiles    = mat.n_tiles();
    auto n_diags    = offsets.size();
    auto total_rows = n_rows * n_tiles;

    assert(vec.n_rows == total_rows);

    thrust::device_vector<float> dev_vec(total_rows, 0);
    thrust::device_vector<float> dev_out(total_rows, 0);
    thrust::device_vector<int>   dev_offsets(offsets);

    thrust::copy(vec.begin(), vec.end(), dev_vec.begin());

    float* vec_ptr     = thrust::raw_pointer_cast(dev_vec.data());
    float* out_ptr     = thrust::raw_pointer_cast(dev_out.data());
    int*   offsets_ptr = thrust::raw_pointer_cast(dev_offsets.data());

    // Calculate kernel parameters
    int min_grid_size;
    int block_size;
    cudaOccupancyMaxPotentialBlockSize(
        &min_grid_size,
        &block_size,
        MMORF::kernel_matmul_rhs_vector,
        0,
        0);

    unsigned int grid_size = (n_rows + block_size - 1) / block_size;

    // loop over tiles
    for (auto tile_i = 0; tile_i < n_tiles; tile_i++) {
    for (auto tile_j = 0; tile_j < n_tiles; tile_j++) {

      thrust::device_ptr<const float> tile = thrust::device_pointer_cast(
         mat.get_const_raw_pointer(tile_i, tile_j));
      const float* const tile_ptr = tile.get();

      MMORF::kernel_matmul_rhs_vector<<<grid_size, block_size>>>(
        n_rows,
        n_cols,
        n_diags,
        n_tiles,
        tile_i,
        tile_j,
        offsets_ptr,
        tile_ptr,
        vec_ptr,
        out_ptr);
    }}

    arma::fvec result(total_rows);

    thrust::copy(dev_out.begin(), dev_out.end(), result.begin());

    return result;
  }

  arma::fcolvec operator*(
      const SparseDiagonalMatrixTiled& mat,
      const arma::Mat<double>&         vec)
  {
    return matmul_rhs_vector(mat, vec);
  }

  arma::fcolvec operator*(
      const SparseDiagonalMatrixTiled& mat,
      const arma::Mat<float>&          vec)
  {
    return matmul_rhs_vector(mat, vec);
  }

} // namespace MMORF
