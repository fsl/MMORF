//////////////////////////////////////////////////////////////////////////////////////////////
/// \file
/// \brief Cost function based on the sum of squared differences between two volumes, one of
///        which is warped. Additionally, a mask in the reference image domain is used to
///        constrain the area of interest during optimisaton
/// \details The warp is defined by b-spline field coefficients, one set per dimension in the
///          volume
/// \author Frederik Lange
/// \date August 2018
/// \copyright Copyright (C) 2018 University of Oxford
//////////////////////////////////////////////////////////////////////////////////////////////
#include "CostFxnSSDBiasFieldMaskedExcluded.h"
#include "CostFxnSplineUtils.h"
#include "Volume.h"
#include "WarpUtils.h"
#include "WarpFieldBSpline.h"
#include "BiasFieldBSpline.h"
#include "SparseDiagonalMatrixTiled.h"
#include "IntensityMapperPolynomial.h"
#include "Config.h"

#include "basisfield/fsl_splines.h"

#include <armadillo>

#include <memory>
#include <vector>
#include <utility>
#include <cmath>
#include <algorithm>
#include <functional>

namespace MMORF
{
////////////////////////////////////////////////////////////////////////////////
// PIMPL Class
////////////////////////////////////////////////////////////////////////////////
  class CostFxnSSDBiasFieldMaskedExcluded::Impl
  {
    public:
      //////////////////////////////////////////////////
      // Public functions
      //////////////////////////////////////////////////
      /// Construct by passing in fully constructed volumes, and defining warp field
      /// characteristics
      /// \param vol_ref Reference (stationary) volume
      /// \param vol_mov Transformed volume
      /// \param affine_ref Pre-calculated affine transform FROM a common space TO the
      ///                   reference space
      /// \param affine_mov Pre-calculated affine transform FROM a common space TO the
      ///                   moving space
      /// \param mask_ref 3D volume used to mask the reference volume
      /// \param mask_mov 3D volume used to mask the moving volume
      /// \param warp_field Warp field to resample vol_mov
      /// \param bias_field Bias field to apply to vol_ref
      /// \param sampling_frequency How often to sample between B-spline knots. E.g. for:
      ///             knot_spacing        = 10mm
      ///             sampling_frequency  = 5
      ///        bias field will be sampled every 2mm
      Impl(
          std::shared_ptr<MMORF::Volume>                 vol_ref,
          std::shared_ptr<MMORF::Volume>                 vol_mov,
          const arma::fmat&                              affine_ref,
          const arma::fmat&                              affine_mov,
          std::shared_ptr<MMORF::Volume>                 mask_ref,
          std::shared_ptr<MMORF::Volume>                 mask_mov,
          const std::shared_ptr<MMORF::WarpFieldBSpline> warp_field,
          std::shared_ptr<MMORF::BiasFieldBSpline>       bias_field,
          int                                            sampling_frequency,
          const int                                      id
          )
        : vol_ref_(vol_ref)
        , vol_mov_(vol_mov)
        , affine_ref_(affine_ref)
        , affine_mov_(affine_mov)
        , mask_ref_(mask_ref)
        , mask_mov_(mask_mov)
        , warp_field_(warp_field)
        , bias_field_(bias_field)
        , sample_dimensions_(bias_field_->get_robust_sample_dimensions(sampling_frequency))
        , spline_1D_(3,sampling_frequency) // Cubic spline with the correct no. samples
        , id_(id)
        , count_cost_(0)
        , count_grad_(0)
        , count_hess_(0)
      {
        if (Config::getInstance().debug()){
          auto tmp_debug_dir = "mkdir -p " + debug_dir_ + std::to_string(id_);
          auto tmp_sysval = std::system(tmp_debug_dir.c_str());
        }
        /// \todo Replace assert with exception
        assert(sampling_frequency > 0);
        sample_positions_bias_ = bias_field_->get_robust_sample_positions(sampling_frequency);
        sample_positions_ref_ = WarpUtils::apply_affine_transform(sample_positions_bias_, affine_ref_);
        // Calculate normalisation values for both volumes
        auto sample_positions_mov = WarpUtils::apply_affine_transform(sample_positions_bias_, affine_mov);
        auto ref_samples = vol_ref_->sample(sample_positions_ref_);
        auto mov_samples = vol_mov_->sample(sample_positions_mov);
        auto ref_mask_samples = std::vector<float>(ref_samples.size(), 1.0f);
        auto mov_mask_samples = std::vector<float>(mov_samples.size(), 1.0f);
        if(mask_ref_){
          ref_mask_samples = mask_ref_->sample(sample_positions_ref_);
        }
        if(mask_mov_){
          mov_mask_samples = mask_mov_->sample(sample_positions_mov);
        }
        norm_factor_ref_ = calculate_robust_norm_factor_(
          ref_samples,
          ref_mask_samples,
          mov_mask_samples
          );
        norm_factor_mov_ = calculate_robust_norm_factor_(
          mov_samples,
          ref_mask_samples,
          mov_mask_samples
          );
      }

      /// Get the current value of the parameters
      std::vector<std::vector<float> > get_parameters() const
      {
        return bias_field_->get_parameters();
      }

      /// Set the current value of the parameters
      void set_parameters(
          const std::vector<std::vector<float> >& parameters)
      {
        bias_field_->set_parameters(parameters);
      }

      /// Get cost under current parameterisation
      float cost() const
      {
        // Sample reference and moving volumes
        auto samples_ref = vol_ref_->sample(sample_positions_ref_);
        normalise_samples_(samples_ref,norm_factor_ref_);
        samples_ref = bias_field_->apply_bias(sample_positions_bias_, samples_ref);
        auto sample_positions_warped = WarpUtils::apply_nonlinear_then_affine_transform(
            sample_positions_bias_,
            affine_mov_,
            *warp_field_);
        auto samples_mov = vol_mov_->sample(sample_positions_warped);
        normalise_samples_(samples_mov,norm_factor_mov_);
        // Calculate cost from samples
        auto samples_error = std::vector<float>(samples_ref.size());
        std::transform(
            samples_mov.begin(),
            samples_mov.end(),
            samples_ref.begin(),
            samples_error.begin(),
            std::minus<float>()
            );
        auto cost = std::inner_product(
            samples_error.begin(),
            samples_error.end(),
            samples_error.begin(),
            0.0f);
        if (mask_ref_){
          auto samples_mask_ref = mask_ref_->sample(sample_positions_ref_);
          std::transform(
              samples_error.begin(),
              samples_error.end(),
              samples_mask_ref.begin(),
              samples_error.begin(),
              std::multiplies<float>()
              );
        }
        if (mask_mov_){
          auto samples_mask_mov = mask_mov_->sample(sample_positions_warped);
          std::transform(
              samples_error.begin(),
              samples_error.end(),
              samples_mask_mov.begin(),
              samples_error.begin(),
              std::multiplies<float>()
              );
        }
        // Avergage cost
        cost /= samples_ref.size();
        return cost;
      }

      /// Get Jte under current parameterisation
      arma::fvec grad() const
      {
        // Calculate jte_sz
        auto bias_field_sz = bias_field_->get_parameter_size();
        // Create and sample error volume
        // Sample reference and moving volumes
        auto samples_ref = vol_ref_->sample(sample_positions_ref_);
        normalise_samples_(samples_ref,norm_factor_ref_);
        auto samples_ref_biased = bias_field_->apply_bias(sample_positions_bias_, samples_ref);
        auto sample_positions_warped = WarpUtils::apply_nonlinear_then_affine_transform(
            sample_positions_bias_,
            affine_mov_,
            *warp_field_);
        auto samples_mov = vol_mov_->sample(sample_positions_warped);
        normalise_samples_(samples_mov,norm_factor_mov_);
        // Calculate current sample error
        auto samples_error = std::vector<float>(samples_ref.size());
        std::transform(
            samples_ref_biased.begin(),
            samples_ref_biased.end(),
            samples_mov.begin(),
            samples_error.begin(),
            std::minus<float>()
            );
        if (mask_ref_){
          auto samples_mask_ref = mask_ref_->sample(sample_positions_ref_);
          std::transform(
              samples_error.begin(),
              samples_error.end(),
              samples_mask_ref.begin(),
              samples_error.begin(),
              std::multiplies<float>()
              );
        }
        if (mask_mov_){
          auto samples_mask_mov = mask_mov_->sample(sample_positions_warped);
          std::transform(
              samples_error.begin(),
              samples_error.end(),
              samples_mask_mov.begin(),
              samples_error.begin(),
              std::multiplies<float>()
              );
        }
        // Convert spline to vec
        auto spline_vec = CostFxnSplineUtils::spline_as_vec(spline_1D_);
        // Pre-multiply the error and reference samples
        auto samples_pre_mult = std::vector<float>(samples_error.size());
        std::transform(
            samples_error.begin(),
            samples_error.end(),
            samples_ref.begin(),
            samples_pre_mult.begin(),
            std::multiplies<float>()
            );
        auto jte = CostFxnSplineUtils::calculate_sub_jte(
            samples_pre_mult,
            sample_dimensions_,
            spline_vec,
            spline_vec,
            spline_vec,
            bias_field_->get_dimensions(),
            std::vector<int>(3, static_cast<int>(spline_1D_.KnotSpacing())));
        // Return result
        auto jte_return = arma::fvec(jte.data(), jte.size());
        jte_return = (2.0f / static_cast<float>(samples_ref.size())) * jte_return;
        if (Config::getInstance().debug()){
          // Create folder
          auto debug_dir =
            debug_dir_ +
            std::to_string(id_) + "/grad/" +
            std::to_string(count_grad_++);
          auto mkdir_command =
            "mkdir -p " +
            debug_dir;
          auto tmp_sysval = std::system(mkdir_command.c_str());
          // Save grad
          jte_return.save(debug_dir + "/grad", arma::raw_ascii);
        }
        return jte_return;
      }

      /// Get JtJ under current parameterisation
      MMORF::SparseDiagonalMatrixTiled hess() const
      {
        // Get size of bias field
        auto bias_field_sz = bias_field_->get_parameter_size();
        auto samples_ref = vol_ref_->sample(sample_positions_ref_);
        normalise_samples_(samples_ref,norm_factor_ref_);
        // Convert spline to vec
        auto spline_vec = CostFxnSplineUtils::spline_as_vec(spline_1D_);
        // Create sparse tiled matrix to store Hessian
        auto sparse_jtj = CostFxnSplineUtils::create_empty_jtj(bias_field_, CostFxn::HESS_FULL, 1);
        // Square the ref image
        std::transform(
            samples_ref.begin(),
            samples_ref.end(),
            samples_ref.begin(),
            samples_ref.begin(),
            std::multiplies<float>()
            );
        if (mask_ref_){
            auto samples_mask_ref = mask_ref_->sample(sample_positions_ref_);
            std::transform(
                samples_ref.begin(),
                samples_ref.end(),
                samples_mask_ref.begin(),
                samples_ref.begin(),
                std::multiplies<float>()
                );
        }
        if (mask_mov_){
            auto sample_positions_warped = WarpUtils::apply_nonlinear_then_affine_transform(
                sample_positions_bias_,
                affine_mov_,
                *warp_field_);
            auto samples_mask_mov = mask_mov_->sample(sample_positions_warped);
            std::transform(
                samples_ref.begin(),
                samples_ref.end(),
                samples_mask_mov.begin(),
                samples_ref.begin(),
                std::multiplies<float>()
                );
        }

        // Call kernel
        CostFxnSplineUtils::calculate_sub_jtj_symmetrical(
          // Input
          samples_ref,
          sample_dimensions_,
          spline_vec,
          spline_vec,
          spline_vec,
          bias_field_->get_dimensions(),
          std::vector<int>(3, static_cast<int>(spline_1D_.KnotSpacing())),
          0,
          0,
          sparse_jtj);
        // Return completed matrix
        sparse_jtj *= (2.0f / static_cast<float>(samples_ref.size()));
        // Debugging
        if (Config::getInstance().debug()){
          // Create folder
          auto debug_dir =
            debug_dir_ +
            std::to_string(id_) + "/hess/" +
            std::to_string(count_hess_++);
          auto mkdir_command =
            "mkdir -p " +
            debug_dir;
          auto tmp_sysval = std::system(mkdir_command.c_str());
          // Save hess
          sparse_jtj.convert_to_csc().save(debug_dir + "/hess", arma::coord_ascii);
        }
        return sparse_jtj;
      }
    private:
      //////////////////////////////////////////////////
      // Private functions
      //////////////////////////////////////////////////

      /// Calculate a scaling value that normalises to the robust mean
      /// \details By robust mean we refer to the mean of values which are part of the actual
      ///          object of interest. In order to do this, we first find the global mean of
      ///          the samples, and then find the mean of all samples with values greater
      ///          than 1/6th of the global mean
      float calculate_robust_norm_factor_(
        const std::vector<float>& image_samples,
        const std::vector<float>& ref_mask_samples,
        const std::vector<float>& mov_mask_samples) const
      {
        // First pass through, find global mean
        auto global_mean = 0.0;
        auto global_weighting = 0.0;
        for (auto i = 0; i < image_samples.size(); ++i){
          global_mean += image_samples[i]*ref_mask_samples[i]*mov_mask_samples[i];
          global_weighting += ref_mask_samples[i]*mov_mask_samples[i];
        }
        global_mean /= global_weighting;
        // Second pass through, find robust mean
        auto robust_mean = 0.0;
        auto robust_weighting = 0;
        for (auto i = 0; i < image_samples.size(); ++i){
          if (image_samples[i]*ref_mask_samples[i]*mov_mask_samples[i] > 0.17*global_mean){
            robust_mean += image_samples[i]*ref_mask_samples[i]*mov_mask_samples[i];
            robust_weighting += ref_mask_samples[i]*mov_mask_samples[i];
          }
        }
        robust_mean /= robust_weighting;
        // Set the normalisation factor such that the robust mean scales to 100
        auto robust_norm = static_cast<float>(100.0/robust_mean);
        return robust_norm;
      }

      /// Normalise a set of samples
      void normalise_samples_(std::vector<float>& samples, float norm_factor) const
      {
        for (auto& sample : samples){
          sample *= norm_factor;
        }
      }

      /// Attempt to correct for differences in contrast between the volumes which might
      /// confound our SSD calculations
      MMORF::IntensityMapperPolynomial create_intensity_mapper_(
          const std::vector<float>& samples_domain,
          const std::vector<float>& samples_range,
          const std::vector<float>& samples_mask) const
      {
        // Extract non-masked-out values
        auto samples_domain_masked = std::vector<float>();
        auto samples_range_masked = std::vector<float>();
        for (auto i = 0; i < samples_mask.size(); ++i){
          if (samples_mask[i] > 0.1f){ // Using 0.1 here just to limit what is valid
            samples_domain_masked.push_back(samples_domain[i]);
            samples_range_masked.push_back(samples_range[i]);
          }
        }
        // Create intensity mapper
        auto polynomial_degree = 3;
        auto intensity_mapper = MMORF::IntensityMapperPolynomial(
            polynomial_degree,
            samples_domain_masked,
            samples_range_masked);
        // Resample domain
        return intensity_mapper;
      }

      //////////////////////////////////////////////////
      // Private datamembers
      //////////////////////////////////////////////////
      // Private datamembers
      std::shared_ptr<MMORF::Volume>           vol_ref_;
      std::shared_ptr<MMORF::Volume>           vol_mov_;
      arma::fmat                               affine_ref_;
      arma::fmat                               affine_mov_;
      std::shared_ptr<MMORF::Volume>           mask_ref_;
      std::shared_ptr<MMORF::Volume>           mask_mov_;
      std::shared_ptr<MMORF::WarpFieldBSpline> warp_field_;
      std::shared_ptr<MMORF::BiasFieldBSpline> bias_field_;
      std::vector<int>                         sample_dimensions_;
      BASISFIELD::Spline1D<float>              spline_1D_;
      std::vector<std::vector<float> >         sample_positions_bias_;
      std::vector<std::vector<float> >         sample_positions_ref_;
      float                                    norm_factor_ref_;
      float                                    norm_factor_mov_;
      // Debug only datamembers
      int                                      id_;
      mutable int                              count_cost_;
      mutable int                              count_grad_;
      mutable int                              count_hess_;
      const static std::string                 debug_dir_;
  };

  /// Static debug directory initialisation
  const std::string CostFxnSSDBiasFieldMaskedExcluded::Impl::debug_dir_ =
    "debug/CostFxnSSDBiasFieldMaskedExcluded/";

////////////////////////////////////////////////////////////////////////////////
// Main Class
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Rule of 5
////////////////////////////////////////////////////////////////////////////////
  /// Default dtor
  CostFxnSSDBiasFieldMaskedExcluded::~CostFxnSSDBiasFieldMaskedExcluded() = default;
  /// Move ctor
  CostFxnSSDBiasFieldMaskedExcluded::CostFxnSSDBiasFieldMaskedExcluded(CostFxnSSDBiasFieldMaskedExcluded&& rhs) = default;
  /// Move assignment operator
  CostFxnSSDBiasFieldMaskedExcluded& CostFxnSSDBiasFieldMaskedExcluded::operator=(CostFxnSSDBiasFieldMaskedExcluded&& rhs) = default;
  /// Copy ctor
  CostFxnSSDBiasFieldMaskedExcluded::CostFxnSSDBiasFieldMaskedExcluded(const CostFxnSSDBiasFieldMaskedExcluded& rhs)
    : pimpl_(nullptr)
  {
    if (rhs.pimpl_){
      pimpl_ = std::make_unique<Impl>(*rhs.pimpl_);
    }
  }
  /// Copy assignment operator
  CostFxnSSDBiasFieldMaskedExcluded& CostFxnSSDBiasFieldMaskedExcluded::operator=(const CostFxnSSDBiasFieldMaskedExcluded& rhs)
  {
    if (!rhs.pimpl_){
      pimpl_.reset();
    }
    else if (!pimpl_){
      pimpl_ = std::make_unique<Impl>(*rhs.pimpl_);
    }
    else{
      *pimpl_ = *rhs.pimpl_;
    }
    return *this;
  }

////////////////////////////////////////////////////////////////////////////////
// Class Specific Functions
////////////////////////////////////////////////////////////////////////////////
  /// Construct by passing in fully constructed volumes, and defining warp field
  /// characteristics
  /// \param vol_ref Reference (stationary) volume
  /// \param vol_mov Transformed volume
  /// \param affine_ref Pre-calculated affine transform FROM a common space TO the
  ///                   reference space
  /// \param affine_mov Pre-calculated affine transform FROM a common space TO the
  ///                   moving space
  /// \param mask_ref 3D volume used to mask the reference volume
  /// \param mask_mov 3D volume used to mask the moving volume
  /// \param knot_spacing Bias field B-spline knot spacing (mm) in reference space
  /// \param sampling_frequency How often to sample between B-spline knots. E.g. for:
  ///             knot_spacing        = 10mm
  ///             sampling_frequency  = 5
  ///        warp field will be sampled every 2mm
  CostFxnSSDBiasFieldMaskedExcluded::CostFxnSSDBiasFieldMaskedExcluded(
          std::shared_ptr<MMORF::Volume>                 vol_ref,
          std::shared_ptr<MMORF::Volume>                 vol_mov,
          const arma::fmat&                              affine_ref,
          const arma::fmat&                              affine_mov,
          std::shared_ptr<MMORF::Volume>                 mask_ref,
          std::shared_ptr<MMORF::Volume>                 mask_mov,
          const std::shared_ptr<MMORF::WarpFieldBSpline> warp_field,
          std::shared_ptr<MMORF::BiasFieldBSpline>       bias_field,
          int                                            sampling_frequency
          )
    : pimpl_(std::make_unique<Impl>(
          vol_ref,
          vol_mov,
          affine_ref,
          affine_mov,
          mask_ref,
          mask_mov,
          warp_field,
          bias_field,
          sampling_frequency,
          next_id_++)
        )
  {}

  /// Get the current value of the parameters
  std::vector<std::vector<float> > CostFxnSSDBiasFieldMaskedExcluded::get_parameters() const
  {
    return pimpl_->get_parameters();
  }

  /// Set the current value of the parameters
  void CostFxnSSDBiasFieldMaskedExcluded::set_parameters(
      const std::vector<std::vector<float> >& parameters)
  {
    pimpl_->set_parameters(parameters);
  }

  /// Get cost under current parameterisation
  float CostFxnSSDBiasFieldMaskedExcluded::cost() const
  {
    return pimpl_->cost();
  }

  /// Get Jte under current parameterisation
  arma::fvec CostFxnSSDBiasFieldMaskedExcluded::grad() const
  {
    return pimpl_->grad();
  }

  /// Get JtJ under current parameterisation
  MMORF::SparseDiagonalMatrixTiled CostFxnSSDBiasFieldMaskedExcluded::hess() const
  {
    return pimpl_->hess();
  }

  /// Static datamember initialisation
  int CostFxnSSDBiasFieldMaskedExcluded::next_id_ = 0;
} // MMORF
