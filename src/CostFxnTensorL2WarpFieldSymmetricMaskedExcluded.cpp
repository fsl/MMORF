//////////////////////////////////////////////////////////////////////////////////////////////
/// \file
/// \brief Cost function based on the L2 norm of a masked tensor volume
/// \details Requires a B-spline parametrised warp field
///          This is now extended to include a greedy approximation to symmetrisation of the
///          problem by multiplying the cost by (1 + |J|).
/// \author Frederik Lange
/// \date March 2021
/// \copyright Copyright (C) 2021 University of Oxford
//////////////////////////////////////////////////////////////////////////////////////////////
#include "CostFxnTensorL2WarpFieldSymmetricMaskedExcluded.h"
#include "CostFxnTensorL2WarpFieldSymmetricMaskedExcludedUtils.h"
#include "CostFxnSplineUtils.h"
#include "WarpUtils.h"
#include "Volume.h"
#include "WarpFieldBSpline.h"
#include "SparseDiagonalMatrixTiled.h"
#include "IntensityMapperPolynomial.h"

#include "basisfield/fsl_splines.h"

#include <armadillo>

#include <memory>
#include <vector>
#include <utility>
#include <cmath>
#include <algorithm>
#include <functional>

namespace MMORF
{
////////////////////////////////////////////////////////////////////////////////
// PIMPL Class
////////////////////////////////////////////////////////////////////////////////
  template<MMORF::CostFxn::HessType hess_type>
  class CostFxnTensorL2WarpFieldSymmetricMaskedExcluded<hess_type>::Impl
  {
    public:
      //////////////////////////////////////////////////
      // Public functions
      //////////////////////////////////////////////////
      /// Construct by passing in fully constructed volumes, and defining warp field
      /// characteristics
      /// \param vol_ref Reference (stationary) volume
      /// \param vol_mov Transformed volume
      /// \param affine_ref Pre-calculated affine transform FROM a common space TO the
      ///                   reference space
      /// \param affine_mov Pre-calculated affine transform FROM a common space TO the
      ///                   moving space
      /// \param mask_ref 3D volume used to mask the reference volume
      /// \param mask_mov 3D volume used to mask the moving volume
      /// \param knot_spacing Warp field B-spline knot spacing (mm) in reference space
      /// \param sampling_frequency How often to sample between B-spline knots. E.g. for:
      ///             knot_spacing        = 10mm
      ///             sampling_frequency  = 5
      ///        warp field will be sampled every 2mm
      Impl(
          std::shared_ptr<MMORF::VolumeTensor>     vol_ref,
          std::shared_ptr<MMORF::VolumeTensor>     vol_mov,
          const arma::fmat&                        affine_ref,
          const arma::fmat&                        affine_mov,
          std::shared_ptr<MMORF::Volume>           mask_ref,
          std::shared_ptr<MMORF::Volume>           mask_mov,
          std::shared_ptr<MMORF::WarpFieldBSpline> warp_field,
          int                                      sampling_frequency
          )
        : vol_ref_(vol_ref)
        , vol_mov_(vol_mov)
        , affine_ref_(affine_ref)
        , affine_mov_(affine_mov)
        , mask_ref_(mask_ref)
        , mask_mov_(mask_mov)
        , warp_field_(warp_field)
        , sample_dimensions_(warp_field_->get_robust_sample_dimensions(sampling_frequency))
        , spline_1D_(3,sampling_frequency) // Cubic spline with the correct no. samples
      {
        /// \todo Replace assert with exception
        assert(sampling_frequency > 0);
        sample_positions_warp_ = warp_field_->get_robust_sample_positions(sampling_frequency);
        sample_positions_ref_ = WarpUtils::apply_affine_transform(sample_positions_warp_, affine_ref_);
        // Calculate sample resolution
        auto warp_dims = warp_field_->get_dimensions();
        auto warp_extents = warp_field_->get_extents();
        for (auto i = 0; i < warp_dims.size(); ++i){
          auto dim_res =
            warp_field_->get_knot_spacing() / static_cast<float>(sampling_frequency);
          sample_resolution_.push_back(dim_res);
        }
      }

      /// Get the current value of the parameters
      std::vector<std::vector<float> > get_parameters() const
      {
        return warp_field_->get_parameters();
      }

      /// Set the current value of the parameters
      void set_parameters(
          const std::vector<std::vector<float> >& parameters)
      {
        warp_field_->set_parameters(parameters);
      }

      /// Get cost under current parameterisation
      float cost() const
      {
        return CostFxnTensorL2WarpFieldSymmetricMaskedExcludedUtils::cost(
            vol_ref_,
            vol_mov_,
            mask_ref_,
            mask_mov_,
            affine_ref_,
            affine_mov_,
            warp_field_,
            sample_positions_ref_,
            sample_positions_warp_);
      }

      /// Get Jte under current parameterisation
      arma::fvec grad() const
      {
        return CostFxnTensorL2WarpFieldSymmetricMaskedExcludedUtils::grad(
            vol_ref_,
            vol_mov_,
            mask_ref_,
            mask_mov_,
            affine_ref_,
            affine_mov_,
            warp_field_,
            sample_positions_ref_,
            sample_positions_warp_,
            spline_1D_,
            sample_dimensions_,
            sample_resolution_);
      }

      /// Get JtJ under current parameterisation
      MMORF::SparseDiagonalMatrixTiled hess() const
      {
        return CostFxnTensorL2WarpFieldSymmetricMaskedExcludedUtils::hess(
            vol_ref_,
            vol_mov_,
            mask_ref_,
            mask_mov_,
            affine_ref_,
            affine_mov_,
            warp_field_,
            sample_positions_ref_,
            sample_positions_warp_,
            spline_1D_,
            sample_dimensions_,
            sample_resolution_,
            hess_type);
      }

    private:

      //////////////////////////////////////////////////
      // Private datamembers
      //////////////////////////////////////////////////
      // Private datamembers
      std::shared_ptr<MMORF::VolumeTensor>     vol_ref_;
      std::shared_ptr<MMORF::VolumeTensor>     vol_mov_;
      arma::fmat                               affine_ref_;
      arma::fmat                               affine_mov_;
      std::shared_ptr<MMORF::Volume>           mask_ref_;
      std::shared_ptr<MMORF::Volume>           mask_mov_;
      std::shared_ptr<MMORF::WarpFieldBSpline> warp_field_;
      std::vector<int>                         sample_dimensions_;
      BASISFIELD::Spline1D<float>              spline_1D_;
      std::vector<std::vector<float> >         sample_positions_warp_;
      std::vector<std::vector<float> >         sample_positions_ref_;
      std::vector<float>                       sample_resolution_;
  };
////////////////////////////////////////////////////////////////////////////////
// Main Class
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Rule of 5
////////////////////////////////////////////////////////////////////////////////
  /// Default dtor
  template<MMORF::CostFxn::HessType hess_type>
  CostFxnTensorL2WarpFieldSymmetricMaskedExcluded<hess_type>::~CostFxnTensorL2WarpFieldSymmetricMaskedExcluded() = default;
  /// Move ctor
  template<MMORF::CostFxn::HessType hess_type>
  CostFxnTensorL2WarpFieldSymmetricMaskedExcluded<hess_type>::CostFxnTensorL2WarpFieldSymmetricMaskedExcluded(
      CostFxnTensorL2WarpFieldSymmetricMaskedExcluded<hess_type>&& rhs) = default;
  /// Move assignment operator
  template<MMORF::CostFxn::HessType hess_type>
  CostFxnTensorL2WarpFieldSymmetricMaskedExcluded<hess_type>&
  CostFxnTensorL2WarpFieldSymmetricMaskedExcluded<hess_type>::operator=(
      CostFxnTensorL2WarpFieldSymmetricMaskedExcluded<hess_type>&& rhs) = default;
  /// Copy ctor
  template<MMORF::CostFxn::HessType hess_type>
  CostFxnTensorL2WarpFieldSymmetricMaskedExcluded<hess_type>::CostFxnTensorL2WarpFieldSymmetricMaskedExcluded(
      const CostFxnTensorL2WarpFieldSymmetricMaskedExcluded<hess_type>& rhs)
    : pimpl_(nullptr)
  {
    if (rhs.pimpl_){
      pimpl_ = std::make_unique<Impl>(*rhs.pimpl_);
    }
  }
  /// Copy assignment operator
  template<MMORF::CostFxn::HessType hess_type>
  CostFxnTensorL2WarpFieldSymmetricMaskedExcluded<hess_type>&
  CostFxnTensorL2WarpFieldSymmetricMaskedExcluded<hess_type>::operator=(
      const CostFxnTensorL2WarpFieldSymmetricMaskedExcluded<hess_type>& rhs)
  {
    if (!rhs.pimpl_){
      pimpl_.reset();
    }
    else if (!pimpl_){
      pimpl_ = std::make_unique<Impl>(*rhs.pimpl_);
    }
    else{
      *pimpl_ = *rhs.pimpl_;
    }
    return *this;
  }
////////////////////////////////////////////////////////////////////////////////
// Class Specific Functions
////////////////////////////////////////////////////////////////////////////////
  /// Construct by passing in fully constructed volumes, and defining warp field
  /// characteristics
  /// \param vol_ref Reference (stationary) volume
  /// \param vol_mov Transformed volume
  /// \param affine_ref Pre-calculated affine transform FROM a common space TO the
  ///                   reference space
  /// \param affine_mov Pre-calculated affine transform FROM a common space TO the
  ///                   moving space
  /// \param mask_ref 3D volume used to mask the reference volume
  /// \param mask_mov 3D volume used to mask the moving volume
  /// \param knot_spacing Warp field B-spline knot spacing (mm) in reference space
  /// \param sampling_frequency How often to sample between B-spline knots. E.g. for:
  ///             knot_spacing        = 10mm
  ///             sampling_frequency  = 5
  ///        warp field will be sampled every 2mm
  template<MMORF::CostFxn::HessType hess_type>
  CostFxnTensorL2WarpFieldSymmetricMaskedExcluded<hess_type>::CostFxnTensorL2WarpFieldSymmetricMaskedExcluded(
          std::shared_ptr<MMORF::VolumeTensor>     vol_ref,
          std::shared_ptr<MMORF::VolumeTensor>     vol_mov,
          const arma::fmat&                        affine_ref,
          const arma::fmat&                        affine_mov,
          std::shared_ptr<MMORF::Volume>           mask_ref,
          std::shared_ptr<MMORF::Volume>           mask_mov,
          std::shared_ptr<MMORF::WarpFieldBSpline> warp_field,
          int                                      sampling_frequency
          )
    : pimpl_(std::make_unique<Impl>(
          vol_ref,
          vol_mov,
          affine_ref,
          affine_mov,
          mask_ref,
          mask_mov,
          warp_field,
          sampling_frequency)
        )
  {}
  /// Get the current value of the parameters
  template<MMORF::CostFxn::HessType hess_type>
  std::vector<std::vector<float> >
  CostFxnTensorL2WarpFieldSymmetricMaskedExcluded<hess_type>::get_parameters() const
  {
    return pimpl_->get_parameters();
  }
  /// Set the current value of the parameters
  template<MMORF::CostFxn::HessType hess_type>
  void CostFxnTensorL2WarpFieldSymmetricMaskedExcluded<hess_type>::set_parameters(
      const std::vector<std::vector<float> >& parameters)
  {
    pimpl_->set_parameters(parameters);
  }
  /// Get cost under current parameterisation
  template<MMORF::CostFxn::HessType hess_type>
  float CostFxnTensorL2WarpFieldSymmetricMaskedExcluded<hess_type>::cost() const
  {
    return pimpl_->cost();
  }
  /// Get Jte under current parameterisation
  template<MMORF::CostFxn::HessType hess_type>
  arma::fvec CostFxnTensorL2WarpFieldSymmetricMaskedExcluded<hess_type>::grad() const
  {
    return pimpl_->grad();
  }
  /// Get JtJ under current parameterisation
  template<MMORF::CostFxn::HessType hess_type>
  MMORF::SparseDiagonalMatrixTiled
  CostFxnTensorL2WarpFieldSymmetricMaskedExcluded<hess_type>::hess() const
  {
    return pimpl_->hess();
  }

  // Template specialisations for full/diag hessian calculation
  template class CostFxnTensorL2WarpFieldSymmetricMaskedExcluded<MMORF::CostFxn::HESS_FULL>;
  template class CostFxnTensorL2WarpFieldSymmetricMaskedExcluded<MMORF::CostFxn::HESS_DIAG>;
} // MMORF
