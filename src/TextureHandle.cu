//////////////////////////////////////////////////////////////////////////////////////////////
/// \file
/// \brief RAII wrappers for dealing with CUDA Texture objects
/// \details Use these helper classes to deal with the hassle of loading image volumes into
///          texture memory, and ensuring proper memory acquisition and release
/// \author Frederik Lange
/// \date May 2018
/// \copyright Copyright (C) 2018 University of Oxford
//////////////////////////////////////////////////////////////////////////////////////////////

#include "TextureHandle.cuh"
#include "helper_cuda.cuh"

#include <thrust/device_vector.h>

#include <vector>

namespace MMORF
{

////////////////////////////////////////////////////////////////////////////////
// Class Specific Functions
////////////////////////////////////////////////////////////////////////////////
   /// Ctor
   TextureHandleLinear::TextureHandleLinear(
     const std::vector<float>& ima,
     const std::vector<int>& ima_sz,
     const bool clamp_border,
     const bool normalised)
     : TextureHandleLinear(ima,
                           ima_sz[0], ima_sz[1], ima_sz[2],
                           clamp_border,
                           normalised) {}

  /// Ctor
  TextureHandleLinear::TextureHandleLinear(
    const std::vector<float>& ima,
    const int  xsz,
    const int  ysz,
    const int  zsz,
    const bool clamp_border,
    const bool normalised)
    : texture_(0)
    , memory_(ima)
  {
    // Create resource descriptor
    cudaResourceDesc res_desc;
    memset(&res_desc,0,sizeof(res_desc));
    res_desc.resType = cudaResourceTypeLinear;
    res_desc.res.linear.devPtr = thrust::raw_pointer_cast(memory_.data());
    res_desc.res.linear.desc.f = cudaChannelFormatKindFloat;
    res_desc.res.linear.desc.x = 32;
    res_desc.res.linear.sizeInBytes = xsz*ysz*zsz*sizeof(float);
    // Create texture descriptor
    cudaTextureDesc tex_desc;
    memset(&tex_desc,0,sizeof(tex_desc));
    tex_desc.readMode = cudaReadModeElementType;
    // Create texture object based on channel and resource descriptors
    checkCudaErrors(cudaCreateTextureObject(&texture_,&res_desc,&tex_desc,NULL));
  }
  /// Dtor
  TextureHandleLinear::~TextureHandleLinear()
  {
    // Release memory
    checkCudaErrors(cudaDestroyTextureObject(texture_));
  }
  /// Return created texture
  /// \todo Check if this should potentially return a const& instead?
  cudaTextureObject_t TextureHandleLinear::get_texture()
  {
    return texture_;
  }

  /// Ctor
  TextureHandlePitched::TextureHandlePitched(
    const std::vector<float>& ima,
    const std::vector<int>& ima_sz,
    const bool clamp_border,
    const bool normalised)
    : TextureHandlePitched(ima,
                           ima_sz[0], ima_sz[1], ima_sz[2],
                           clamp_border,
                           normalised) {}

  /// Ctor
  TextureHandlePitched::TextureHandlePitched(
    const std::vector<float>& ima,
    const int  xsz,
    const int  ysz,
    const int  zsz,
    const bool clamp_border,
    const bool normalised)
    : texture_(0),
      memory_(0)
  {

    // Make a copy of the data - neatest way
    // to avoid const nastiness when passing
    // to make_cudaPitchedPtr below.
    auto imacpy = std::vector<float>(ima);

    // allocate 3D array on device
    cudaExtent extent                 = make_cudaExtent(xsz, ysz, zsz);
    cudaChannelFormatDesc channelDesc = cudaCreateChannelDesc<float>();
    checkCudaErrors(cudaMalloc3DArray(&memory_, &channelDesc, extent));

    // copy data to device array
    cudaMemcpy3DParms params = {0};
    params.extent   = extent;
    params.srcPtr   = make_cudaPitchedPtr(imacpy.data(),
                                          xsz * sizeof(float),
                                          xsz,
                                          ysz);
    params.dstArray = memory_;
    params.kind     = cudaMemcpyHostToDevice;
    checkCudaErrors(cudaMemcpy3D(&params));

    // create texture object
    cudaResourceDesc rdesc;
    cudaTextureDesc  tdesc;

    memset(&rdesc, 0, sizeof(rdesc));
    memset(&tdesc, 0, sizeof(tdesc));

    rdesc.resType         = cudaResourceTypeArray;
    rdesc.res.array.array = memory_;
    tdesc.readMode        = cudaReadModeElementType;
    tdesc.filterMode      = cudaFilterModeLinear;

    if (normalised) tdesc.normalizedCoords = 1;
    else            tdesc.normalizedCoords = 0;

    enum cudaTextureAddressMode addressMode;

    if (clamp_border) addressMode = cudaAddressModeClamp;
    else              addressMode = cudaAddressModeBorder;

    tdesc.addressMode[0] = addressMode;
    tdesc.addressMode[1] = addressMode;
    tdesc.addressMode[2] = addressMode;

    checkCudaErrors(cudaCreateTextureObject(&texture_, &rdesc, &tdesc, NULL));
  }

  cudaTextureObject_t TextureHandlePitched::get_texture()
  {
    return texture_;
  }

  TextureHandlePitched::~TextureHandlePitched() {
    checkCudaErrors(cudaDestroyTextureObject(texture_));
    checkCudaErrors(cudaFreeArray(memory_));
  }

} // MMORF
