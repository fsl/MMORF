//////////////////////////////////////////////////////////////////////////////////////////////
/// \file CostFxnSSDWarpFieldSymmetricMaskedExcludedUtils.cu
/// \brief Utility functions used by CostFxnsSSDWarpFieldSymmetricMaskedExcluded classes.
/// \details
/// \author Frederik Lange
/// \date February 2024
/// \copyright Copyright (C) 2018 University of Oxford
//////////////////////////////////////////////////////////////////////////////////////////////

#include "Volume.h"
#include "CostFxnSplineUtils.h"
#include "WarpUtils.h"
#include "CostFxn.h"
#include "WarpFieldBSpline.h"
#include "BiasFieldBSpline.h"

#include "basisfield/fsl_splines.h"

#include <armadillo>

#include <memory>
#include <vector>


namespace MMORF {

  namespace CostFxnSSDWarpFieldSymmetricMaskedExcludedUtils {

    /// Normalise a set of samples
    void normalise_samples(
        std::vector<float>& samples,
        float               norm_factor);

    /// Calculate the derivitive of the moving volume wrt to changes in the x, y and z
    /// displacements in the shared reference space
    std::vector<std::vector<float> > calculate_derivatives_mov(
        const std::shared_ptr<MMORF::Volume>    vol_mov,
        const std::vector<std::vector<float> >& positions,
        const arma::fmat&                       affine_mat);

    /// Get cost under current parameterisation
    float cost(
        const std::shared_ptr<MMORF::Volume>           vol_mov,
        const std::shared_ptr<MMORF::Volume>           vol_ref,
        const std::shared_ptr<MMORF::Volume>           mask_mov,
        const std::shared_ptr<MMORF::Volume>           mask_ref,
        const std::shared_ptr<MMORF::BiasFieldBSpline> bias_field,
        const std::shared_ptr<MMORF::WarpFieldBSpline> warp_field,
        const arma::fmat                               affine_mov,
        const float                                    norm_factor_mov,
        const float                                    norm_factor_ref,
        const std::vector<std::vector<float> >         sample_positions_ref,
        const std::vector<std::vector<float> >         sample_positions_warp)
    {
      // Sample reference and moving volumes
      auto samples_ref = vol_ref->sample(sample_positions_ref);
      normalise_samples(samples_ref,norm_factor_ref);
      if (bias_field){
        samples_ref = bias_field->apply_bias(sample_positions_ref, samples_ref);
      }
      //samples_ref = intensity_mapper_.map_intensities(samples_ref);
      auto sample_positions_warped = WarpUtils::apply_nonlinear_then_affine_transform(
          sample_positions_warp,
          affine_mov,
          *warp_field);
      auto samples_mov = vol_mov->sample(sample_positions_warped);
      normalise_samples(samples_mov,norm_factor_mov);
      // Calculate Jacobian determinant for current parametrisation
      auto samples_jac_det = warp_field->jacobian_determinants(sample_positions_warp);
      // Calculate cost from samples
      auto samples_error = std::vector<float>(samples_ref.size());
      std::transform(
          samples_mov.begin(),
          samples_mov.end(),
          samples_ref.begin(),
          samples_error.begin(),
          std::minus<float>()
          );
      std::transform(
          samples_error.begin(),
          samples_error.end(),
          samples_error.begin(),
          samples_error.begin(),
          std::multiplies<float>()
          );
      std::transform(
          samples_error.begin(),
          samples_error.end(),
          samples_jac_det.begin(),
          samples_error.begin(),
          [](float a, float b) -> float { return 0.5f*(1.0f + b)*a; }
          );
      if (mask_ref){
        auto samples_mask_ref = mask_ref->sample(sample_positions_ref);
        std::transform(
            samples_error.begin(),
            samples_error.end(),
            samples_mask_ref.begin(),
            samples_error.begin(),
            std::multiplies<float>()
            );
      }
      if (mask_mov){
        auto samples_mask_mov = mask_mov->sample(sample_positions_warped);
        std::transform(
            samples_error.begin(),
            samples_error.end(),
            samples_mask_mov.begin(),
            samples_error.begin(),
            std::multiplies<float>()
            );
      }
      auto cost = std::accumulate(
          samples_error.begin(),
          samples_error.end(),
          0.0f
          );
      // Average cost
      cost /= samples_ref.size();
      return cost;
    }

    /// Get Jte under current parameterisation
    arma::fvec grad(
        const std::shared_ptr<MMORF::Volume>           vol_mov,
        const std::shared_ptr<MMORF::Volume>           vol_ref,
        const std::shared_ptr<MMORF::Volume>           mask_mov,
        const std::shared_ptr<MMORF::Volume>           mask_ref,
        const std::shared_ptr<MMORF::BiasFieldBSpline> bias_field,
        const std::shared_ptr<MMORF::WarpFieldBSpline> warp_field,
        const arma::fmat                               affine_mov,
        const float                                    norm_factor_mov,
        const float                                    norm_factor_ref,
        const std::vector<std::vector<float> >         sample_positions_ref,
        const std::vector<std::vector<float> >         sample_positions_warp,
        const std::vector<int>                         sample_dimensions,
        const BASISFIELD::Spline1D<float>&             spline_1D)
    {
      // Calculate jte_sz and make a vector for storing the result
      auto warp_sz = warp_field->get_parameter_size();
      auto jte_sz = warp_sz.first * warp_sz.second;
      auto jte_vec = std::vector<float>(jte_sz);

      // Create and sample error volume
      // Sample reference and moving volumes
      auto samples_ref = vol_ref->sample(sample_positions_ref);
      normalise_samples(samples_ref,norm_factor_ref);
      if (bias_field){
        samples_ref = bias_field->apply_bias(sample_positions_ref, samples_ref);
      }
      //samples_ref = intensity_mapper_.map_intensities(samples_ref);
      auto sample_positions_warped = WarpUtils::apply_nonlinear_then_affine_transform(
          sample_positions_warp,
          affine_mov,
          *warp_field);
      auto samples_mov = vol_mov->sample(sample_positions_warped);
      normalise_samples(samples_mov,norm_factor_mov);
      // Calculate Jacobian determinant for current parametrisation
      auto samples_jac_det = warp_field->jacobian_determinants(sample_positions_warp);
      // Calculate current sample error
      auto samples_error = std::vector<float>(samples_ref.size());
      std::transform(
          samples_mov.begin(),
          samples_mov.end(),
          samples_ref.begin(),
          samples_error.begin(),
          std::minus<float>()
          );
      std::transform(
          samples_error.begin(),
          samples_error.end(),
          samples_jac_det.begin(),
          samples_error.begin(),
          [](float a, float b) -> float { return 0.5f*(1.0f + b)*a; }
          );
      if (mask_ref){
        auto samples_mask_ref = mask_ref->sample(sample_positions_ref);
        std::transform(
            samples_error.begin(),
            samples_error.end(),
            samples_mask_ref.begin(),
            samples_error.begin(),
            std::multiplies<float>()
            );
      }
      if (mask_mov){
        auto samples_mask_mov = mask_mov->sample(sample_positions_warped);
        std::transform(
            samples_error.begin(),
            samples_error.end(),
            samples_mask_mov.begin(),
            samples_error.begin(),
            std::multiplies<float>()
            );
      }
      // Convert spline to vec
      auto spline_vec = CostFxnSplineUtils::spline_as_vec(spline_1D);
      // Calculate derivative samples
      auto samples_mov_deriv_vec = calculate_derivatives_mov(
          vol_mov,
          sample_positions_warped,
          affine_mov);
      // Loop over all warp directions
      for (auto warp_dim = 0; warp_dim < warp_sz.first; ++warp_dim){
        // Normalise sampled derivatives
        normalise_samples(samples_mov_deriv_vec[warp_dim],norm_factor_mov);
        // Pre-multiply the error and derivative samples
        auto samples_pre_mult = std::vector<float>(samples_error.size());
        std::transform(
            samples_error.begin(),
            samples_error.end(),
            samples_mov_deriv_vec[warp_dim].begin(),
            samples_pre_mult.begin(),
            std::multiplies<float>()
            );
        // Get correct index into jte_dev (x, then y, then z)
        auto sub_jte = CostFxnSplineUtils::calculate_sub_jte(
            samples_pre_mult,
            sample_dimensions,
            spline_vec,
            spline_vec,
            spline_vec,
            warp_field->get_dimensions(),
            std::vector<int>(3, static_cast<int>(spline_1D.KnotSpacing())));
        std::copy(sub_jte.begin(), sub_jte.end(),
                  jte_vec.begin() + warp_dim * warp_sz.second);
      }
      // Return result
      auto jte_return = arma::fvec(jte_vec.data(), jte_vec.size());
      jte_return = 2.0f*jte_return/samples_ref.size();
      return jte_return;
    }

    /// Get JtJ under current parameterisation
    MMORF::SparseDiagonalMatrixTiled hess(
        const std::shared_ptr<MMORF::Volume>           vol_mov,
        const std::shared_ptr<MMORF::Volume>           mask_mov,
        const std::shared_ptr<MMORF::Volume>           mask_ref,
        const std::shared_ptr<MMORF::WarpFieldBSpline> warp_field,
        const arma::fmat                               affine_mov,
        const float                                    norm_factor_mov,
        const std::vector<std::vector<float> >         sample_positions_ref,
        const std::vector<std::vector<float> >         sample_positions_warp,
        const std::vector<int>                         sample_dimensions,
        const BASISFIELD::Spline1D<float>&             spline_1D,
        const MMORF::CostFxn::HessType                 hess_type)
    {
        // Get size of warp field
        auto warp_sz = warp_field->get_parameter_size();
        // Apply warp field to sample positions
        auto sample_positions_warped = WarpUtils::apply_nonlinear_then_affine_transform(
            sample_positions_warp,
            affine_mov,
            *warp_field);
        // Sample derivative of moving volume in all directions and store in vector
        auto samples_mov_deriv_vec = calculate_derivatives_mov(
            vol_mov,
            sample_positions_warped,
            affine_mov);
        for (auto i = 0; i < warp_sz.first; ++i){
          normalise_samples(samples_mov_deriv_vec[i],norm_factor_mov);
        }
        // Calculate Jacobian determinant for current parametrisation
        auto samples_jac_det = warp_field->jacobian_determinants(sample_positions_warp);
        // Convert spline to vec
        auto spline_vec = CostFxnSplineUtils::spline_as_vec(spline_1D);
        // Create sparse tiled matrix to store Hessian
        auto sparse_jtj = CostFxnSplineUtils::create_empty_jtj(warp_field, hess_type, 3);
        // Loop over all warp directions
        for (auto row = 0; row < warp_sz.first; ++row){
          // Loop over all warp directions again
          for (auto col = 0; col < warp_sz.first; ++col){
            // Check if this is a unique sub-jtj - we will keep the main diagonal and below
            if (col > row) continue;
            // Pre-multiply the derivative samples
            auto samples_pre_mult = std::vector<float>(samples_mov_deriv_vec[0].size());
            std::transform(
                samples_mov_deriv_vec[row].begin(),
                samples_mov_deriv_vec[row].end(),
                samples_mov_deriv_vec[col].begin(),
                samples_pre_mult.begin(),
                std::multiplies<float>()
                );
            // Pre-multiply by the Jacobian modulation
            std::transform(
                samples_pre_mult.begin(),
                samples_pre_mult.end(),
                samples_jac_det.begin(),
                samples_pre_mult.begin(),
                [](float a, float b) -> float { return 0.5f*(1.0f + b)*a; }
                );
            if (mask_ref){
              auto samples_mask_ref = mask_ref->sample(sample_positions_ref);
              std::transform(
                  samples_pre_mult.begin(),
                  samples_pre_mult.end(),
                  samples_mask_ref.begin(),
                  samples_pre_mult.begin(),
                  std::multiplies<float>()
                  );
            }
            if (mask_mov){
              auto samples_mask_mov = mask_mov->sample(sample_positions_warped);
              std::transform(
                  samples_pre_mult.begin(),
                  samples_pre_mult.end(),
                  samples_mask_mov.begin(),
                  samples_pre_mult.begin(),
                  std::multiplies<float>()
                  );
            }
            // Call kernel
            auto calc_sub_jtj =
              hess_type == MMORF::CostFxn::HESS_FULL
              ? CostFxnSplineUtils::calculate_sub_jtj_symmetrical
              : CostFxnSplineUtils::calculate_sub_jtj_symmetrical_diag_hess;

            calc_sub_jtj(
              // Input
              samples_pre_mult,
              sample_dimensions,
              spline_vec,
              spline_vec,
              spline_vec,
              warp_field->get_dimensions(),
              std::vector<int>(3, static_cast<int>(spline_1D.KnotSpacing())),
              row,
              col,
              sparse_jtj);
            // Save above diagonal sub-jtj
            if (hess_type == MMORF::CostFxn::HESS_FULL && (row > col))
            {
              sparse_jtj.copy_submatrix(row, col, col, row);
            }
          }
        }
        // Return completed matrix
        sparse_jtj *= (2.0 / static_cast<float>(samples_mov_deriv_vec[0].size()));
        return sparse_jtj;
    }

    /// Calculate a scaling value that normalises to the robust mean
    /// \details By robust mean we refer to the mean of values which are part of the actual
    ///          object of interest. In order to do this, we first find the global mean of
    ///          the samples, and then find the mean of all samples with values greater
    ///          than 1/6th of the global mean
    float calculate_robust_norm_factor(
        const std::vector<float>& image_samples,
        const std::vector<float>& ref_mask_samples,
        const std::vector<float>& mov_mask_samples)
    {
      // First pass through, find global mean
      auto global_mean = 0.0;
      auto global_weighting = 0.0;
      for (auto i = 0; i < image_samples.size(); ++i){
        global_mean += image_samples[i]*ref_mask_samples[i]*mov_mask_samples[i];
        global_weighting += ref_mask_samples[i]*mov_mask_samples[i];
      }
      global_mean /= global_weighting;
      // Second pass through, find robust mean
      auto robust_mean = 0.0;
      auto robust_weighting = 0;
      for (auto i = 0; i < image_samples.size(); ++i){
        if (image_samples[i]*ref_mask_samples[i]*mov_mask_samples[i] > 0.17*global_mean){
          robust_mean += image_samples[i]*ref_mask_samples[i]*mov_mask_samples[i];
          robust_weighting += ref_mask_samples[i]*mov_mask_samples[i];
        }
      }
      robust_mean /= robust_weighting;
      // Set the normalisation factor such that the robust mean scales to 100
      auto robust_norm = static_cast<float>(100.0/robust_mean);
      return robust_norm;
    }

    void normalise_samples(
        std::vector<float>& samples,
        float               norm_factor)
    {
      for (auto& sample : samples){
        sample *= norm_factor;
      }
    }

      /// Calculate the derivitive of the moving volume wrt to changes in the x, y and z
      /// displacements in the shared reference space
      std::vector<std::vector<float> > calculate_derivatives_mov(
          const std::shared_ptr<MMORF::Volume>    vol_mov,
          const std::vector<std::vector<float> >& positions,
          const arma::fmat&                       affine_mat)
      {
        auto deriv_dim = positions.size();
        auto samples_dim = positions[0].size();
        // Create an armadillo matrix to store the samples as this will make life easier when
        // doing the affine transform
        auto derivatives_vec = vol_mov->sample_gradient(positions);
        auto derivatives_arma = arma::fmat(positions[0].size(), positions.size());
        for (auto i = 0; i < deriv_dim; ++i){
          for (auto j = 0; j < samples_dim; ++j){
            derivatives_arma(j,i) = derivatives_vec[i][j];
          }
        }
        // Multiply derivative samples by the transposed affine matrix (excluding the offset)
        //
        // This step is a bit confusing for 2 reasons.
        //  1)  I'm post-multiplying by the affine. This is done because of Armadillo's
        //      column-major formatting. This makes the operation somewhat more efficient
        //      (although I am now dubious about this given the confusion it may cause).
        //      IMPORTANTLY this requires introducing a transpose into the affine.
        //
        //  2)  This is the more confusing part. The reason we multiply by the transpose
        //      affine is because of the relationship:
        //
        //            if:   f2(y) = f1(Ty)  <-- Here "T" is the warp-to-moving affine
        //            =>    grad(f2(y)) = transp(T)*grad(f1(Ty))
        //
        //            Where T is any linear transform (e.g. an affine matrix)
        //
        //      But because we've already introduced a transpose in 1), we undo the transpose,
        //      and therefore we simply post-multiply by the original affine.
        //
        derivatives_arma = derivatives_arma * affine_mat.submat(0,0,2,2);
        // Convert back to std::vector
        auto derivatives_return = std::vector<std::vector<float> >(positions.size());
        for (auto i = 0; i < derivatives_return.size(); ++i){
          derivatives_return[i] =
            arma::conv_to<std::vector<float> >::from(derivatives_arma.col(i));
        }
        return derivatives_return;
      }

  } // namespace CostFxnLogJacobianSingularValuesUtils
} // namespace MMORF
