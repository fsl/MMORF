//////////////////////////////////////////////////////////////////////////////////////////////
/// \file
/// \brief Linear solver implementaion using conjugate gradient
/// \details
/// \author Jesper Andersson, Frederik Lange, Paul McCarthy
/// \date May 2024
/// \copyright Copyright (C) 2024 University of Oxford
//////////////////////////////////////////////////////////////////////////////////////////////

#include "LinearSolverCG.h"
#include "LinearSolver.h"
#include "SparseDiagonalMatrixTiled.h"

#include <armadillo>

namespace MMORF {

  bool conjugate_gradient(
      const MMORF::SparseDiagonalMatrixTiled& A,
      const arma::fcolvec&                    b,
      arma::fcolvec&                          x,
      int                                     max_iter,
      float                                   tol);

  LinearSolverCG::LinearSolverCG(
      const int   max_iterations,
      const float relative_tolerance)
    : max_iterations_(max_iterations)
    , relative_tolerance_(relative_tolerance)
  {}

  LinearSolverCG::~LinearSolverCG() = default;

  // Solve Ax = b for x, given A (coefficients) and b (constants)
  // A == coefficients == hessian
  // b == constants    == gradient
  arma::fmat LinearSolverCG::solve(
      const MMORF::SparseDiagonalMatrixTiled& coefficients,
      const arma::fmat&                       constants) const
  {
    arma::fcolvec x(constants.n_rows, arma::fill::zeros);
    conjugate_gradient(
        coefficients, constants, x, max_iterations_, relative_tolerance_);
    return x;
  }

  // conjugate gradient with Jacobi preconditioning
  //   miscmaths/cg.h
  //   Templates for the Solution of Linear Systems, Barrett et. al.

  // TODO error handling
  bool conjugate_gradient(
      const MMORF::SparseDiagonalMatrixTiled& A,
      const arma::fcolvec&                    b,
      arma::fcolvec&                          x,
      int                                     max_iter,
      float                                   tol)
  {

    bool converged = false;
    arma::fcolvec y, z, r, p, M;
    float rz, rz_old, alpha, beta;

    float bnorm = arma::norm(b);

    // jacobi preconditioning
    M     = 1 / A.diag(0);
    y     = A * x;
    r     = b - y;
    z     = M % r;
    p     = arma::fcolvec(z);
    rz    = arma::as_scalar(r.t() * z);

    for (int i = 0; i < max_iter; i++) {

      if (arma::norm(r) <= (tol * bnorm)) {
        converged = true;
        break;
      }

      y      = A * p;
      alpha  = rz / arma::as_scalar(y.t() * p);
      x      = x + alpha * p;
      r      = r - alpha * y;
      z      = M % r;
      rz_old = rz;
      rz     = arma::as_scalar(r.t() * z);
      beta   = rz / rz_old;
      p      = z + beta * p;

    }

    if (converged) return 0;
    else           return 1;
  }
} // MMORF
