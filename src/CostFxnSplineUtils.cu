//////////////////////////////////////////////////////////////////////////////////////////////
/// \file CostFxnSplineUtils.cu
/// \brief Spline field utility functions used by CostFxns
/// \details
/// \author Frederik Lange
/// \date February 2024
/// \copyright Copyright (C) 2018 University of Oxford
//////////////////////////////////////////////////////////////////////////////////////////////

#include "CostFxnSplineUtils.h"
#include "TextureHandle.cuh"
#include "CostFxnHelpers.cuh"
#include "CostFxnKernels.cuh"
#include "WarpFieldBSpline.h"
#include "BiasFieldBSpline.h"
#include "CostFxn.h"

#include "helper_cuda.cuh"

#include <thrust/host_vector.h>
#include <thrust/device_vector.h>

#include <memory>
#include <vector>


namespace MMORF {

  namespace CostFxnSplineUtils {

    // Calculate a sub-vector of Jte
    std::vector<float> calculate_sub_jte(
        const std::vector<float>& prod_ima,
        const std::vector<int>&   ima_sz,
        const std::vector<float>& spline_x,
        const std::vector<float>& spline_y,
        const std::vector<float>& spline_z,
        const std::vector<int>&   coef_sz,
        const std::vector<int>&   ksp)
    {
      // Make everything a device vector using thrust::
      auto sub_jte_sz   = coef_sz[0] * coef_sz[1] * coef_sz[2];
      auto sub_jte_dev  = thrust::device_vector<float>(sub_jte_sz);
      auto sub_jte_raw  = thrust::raw_pointer_cast(&sub_jte_dev[0]);
      auto spline_x_dev = thrust::device_vector<float>(spline_x);
      auto spline_y_dev = thrust::device_vector<float>(spline_y);
      auto spline_z_dev = thrust::device_vector<float>(spline_z);
      // Create texture handle
      auto texture_handle = MMORF::TextureHandleLinear(prod_ima,ima_sz);
      auto tex = texture_handle.get_texture();
      // Get all the raw pointers ready for the Kernel
      float *spline_x_raw = thrust::raw_pointer_cast(spline_x_dev.data());
      float *spline_y_raw = thrust::raw_pointer_cast(spline_y_dev.data());
      float *spline_z_raw = thrust::raw_pointer_cast(spline_z_dev.data());
      // Calculate parameters for running kernel
      int min_grid_size;
      int threads;
      cudaOccupancyMaxPotentialBlockSize(
          &min_grid_size,
          &threads,
          MMORF::kernel_make_jte,
          0,
          0);
      unsigned int blocks = static_cast<unsigned int>(
        std::ceil(float(sub_jte_sz)/float(threads)));
      dim3 blocks_1d(blocks);
      unsigned int smem = (spline_x.size()+spline_y.size()+spline_z.size())*sizeof(float);
      // Call CUDA Kernel
      MMORF::kernel_make_jte<<<blocks_1d,threads,smem>>>(
          // Input
          ima_sz.at(0),
          ima_sz.at(1),
          ima_sz.at(2),
          tex,
          spline_x_raw,
          spline_y_raw,
          spline_z_raw,
          ksp.at(0),
          ksp.at(1),
          ksp.at(2),
          coef_sz.at(0),
          coef_sz.at(1),
          coef_sz.at(2),
          // Output
          sub_jte_raw);
      /// \todo Add error checking to cudaDeviceSynchronize
      checkCudaErrors(cudaDeviceSynchronize());

      // copy result back to host
      auto sub_jte = std::vector<float>(sub_jte_dev.size());
      thrust::copy(sub_jte_dev.begin(), sub_jte_dev.end(), sub_jte.begin());
      return sub_jte;
    }

    // Calculate the offsets for a sparse diagonal matrix
    // for a field of the specified size
    std::vector<int> calculate_sparse_diag_offsets(
        const std::vector<int> &coef_sz) {
      auto offsets = std::vector<int>(343,0);
  #pragma omp parallel for
      for (unsigned int i = 0; i < 343; ++i)
        {
          unsigned int first_row, first_col, last_row, last_col;
          MMORF::identify_diagonal(i,7,7,7,coef_sz[0],coef_sz[1],coef_sz[2],
                                   &first_row,&last_row,&first_col,&last_col);
          if (first_col == 0){
            offsets.at(i) = -static_cast<int>(first_row);
          }
          else{
            offsets.at(i) = static_cast<int>(first_col);
          }
        }
      return offsets;
    }


    // Calclulate sub-matrix of jtj for symmetrical splines
    void calculate_sub_jtj_symmetrical(
        const std::vector<float>&         prod_ima,
        const std::vector<int>&           ima_sz,
        const std::vector<float>&         spline_x,
        const std::vector<float>&         spline_y,
        const std::vector<float>&         spline_z,
        const std::vector<int>&           coef_sz,
        const std::vector<int>&           ksp,
        const unsigned int                sub_jtj_row,
        const unsigned int                sub_jtj_col,
        MMORF::SparseDiagonalMatrixTiled& sparse_jtj)
    {
      // Make everything a device vector using thrust::
      auto sparse_jtj_offsets_dev = thrust::device_vector<int>(sparse_jtj.get_offsets());
      auto spline_x_dev = thrust::device_vector<float>(spline_x);
      auto spline_y_dev = thrust::device_vector<float>(spline_y);
      auto spline_z_dev = thrust::device_vector<float>(spline_z);
      // Create texture handle
      auto texture_handle = MMORF::TextureHandleLinear(prod_ima,ima_sz);
      auto tex = texture_handle.get_texture();
      // Get all the raw pointers ready for the Kernel
      float *spline_x_raw = thrust::raw_pointer_cast(spline_x_dev.data());
      float *spline_y_raw = thrust::raw_pointer_cast(spline_y_dev.data());
      float *spline_z_raw = thrust::raw_pointer_cast(spline_z_dev.data());
      int *sparse_jtj_offsets_raw = thrust::raw_pointer_cast(
          sparse_jtj_offsets_dev.data());
      float *sparse_jtj_raw = sparse_jtj.get_raw_pointer(sub_jtj_row, sub_jtj_col);
      // Calculate parameters for running kernel
      int min_grid_size;
      int threads;
      cudaOccupancyMaxPotentialBlockSize(
          &min_grid_size,
          &threads,
          MMORF::kernel_make_jtj_symmetrical,
          0,
          0);
      unsigned int blocks = 172;
      unsigned int chunks = static_cast<unsigned int>(
          std::ceil(float(coef_sz.at(0)*coef_sz.at(1)*coef_sz.at(2))/float(threads)));
      dim3 blocks_2d(blocks,chunks);
      unsigned int smem = (spline_x.size()+spline_y.size()+spline_z.size())*sizeof(float);
      // Call CUDA Kernel
      MMORF::kernel_make_jtj_symmetrical<<<blocks_2d,threads,smem>>>(
          // Input
          ima_sz.at(0),
          ima_sz.at(1),
          ima_sz.at(2),
          tex,
          spline_x_raw,
          spline_y_raw,
          spline_z_raw,
          ksp.at(0),
          ksp.at(1),
          ksp.at(2),
          coef_sz.at(0),
          coef_sz.at(1),
          coef_sz.at(2),
          sparse_jtj_offsets_raw,
          // Output
          sparse_jtj_raw);
      /// \todo Add error checking to cudaDeviceSynchronize
      checkCudaErrors(cudaDeviceSynchronize());
    }

    // Calclulate sub-matrix of jtj for symmetrical splines
    void calculate_sub_jtj_symmetrical_diag_hess(
        const std::vector<float>&         prod_ima,
        const std::vector<int>&           ima_sz,
        const std::vector<float>&         spline_x,
        const std::vector<float>&         spline_y,
        const std::vector<float>&         spline_z,
        const std::vector<int>&           coef_sz,
        const std::vector<int>&           ksp,
        const unsigned int                sub_jtj_row,
        const unsigned int                sub_jtj_col,
        MMORF::SparseDiagonalMatrixTiled& sparse_jtj)
    {
      // Calculate real Heassian offsets as sparse_jtj is now only a main diagonal matrix
      auto offsets = calculate_sparse_diag_offsets(coef_sz);
      // Make everything a device vector using thrust::
      auto sparse_jtj_offsets_dev = thrust::device_vector<int>(offsets);
      auto spline_x_dev = thrust::device_vector<float>(spline_x);
      auto spline_y_dev = thrust::device_vector<float>(spline_y);
      auto spline_z_dev = thrust::device_vector<float>(spline_z);
      // Create texture handle
      auto texture_handle = MMORF::TextureHandleLinear(prod_ima,ima_sz);
      auto tex = texture_handle.get_texture();
      // Get all the raw pointers ready for the Kernel
      float *spline_x_raw = thrust::raw_pointer_cast(spline_x_dev.data());
      float *spline_y_raw = thrust::raw_pointer_cast(spline_y_dev.data());
      float *spline_z_raw = thrust::raw_pointer_cast(spline_z_dev.data());
      int *sparse_jtj_offsets_raw = thrust::raw_pointer_cast(
          sparse_jtj_offsets_dev.data());
      // Note that in this case we save everything to the main diagonal, so we just use the
      // sub_jtj_row for both the row and column parameter
      float *sparse_jtj_raw_1 = sparse_jtj.get_raw_pointer(sub_jtj_row, sub_jtj_row);
      float *sparse_jtj_raw_2 = sparse_jtj.get_raw_pointer(sub_jtj_col, sub_jtj_col);
      // Calculate parameters for running kernel
      int min_grid_size;
      int threads;
      cudaOccupancyMaxPotentialBlockSize(
          &min_grid_size,
          &threads,
          MMORF::kernel_make_jtj_symmetrical,
          0,
          0);
      unsigned int blocks = 172;
      unsigned int chunks = static_cast<unsigned int>(
          std::ceil(float(coef_sz.at(0)*coef_sz.at(1)*coef_sz.at(2))/float(threads)));
      dim3 blocks_2d(chunks,blocks);
      unsigned int smem = (spline_x.size()+spline_y.size()+spline_z.size())*sizeof(float);
      // Call CUDA Kernel
      // Check if this is on the main diagonal of the tiled matrix or not
      if (sub_jtj_row == sub_jtj_col){
        MMORF::kernel_make_jtj_symmetrical_diag_hess_main<<<blocks_2d,threads,smem>>>(
          // Input
          ima_sz.at(0),
          ima_sz.at(1),
          ima_sz.at(2),
          tex,
          spline_x_raw,
          spline_y_raw,
          spline_z_raw,
          ksp.at(0),
          ksp.at(1),
          ksp.at(2),
          coef_sz.at(0),
          coef_sz.at(1),
          coef_sz.at(2),
          sparse_jtj_offsets_raw,
          // Output
          sparse_jtj_raw_1);
        /// \todo Add error checking to cudaDeviceSynchronize
        checkCudaErrors(cudaDeviceSynchronize());
      }
      else{
        MMORF::kernel_make_jtj_symmetrical_diag_hess_off<<<blocks_2d,threads,smem>>>(
          // Input
          ima_sz.at(0),
          ima_sz.at(1),
          ima_sz.at(2),
          tex,
          spline_x_raw,
          spline_y_raw,
          spline_z_raw,
          ksp.at(0),
          ksp.at(1),
          ksp.at(2),
          coef_sz.at(0),
          coef_sz.at(1),
          coef_sz.at(2),
          sparse_jtj_offsets_raw,
          // Output
          sparse_jtj_raw_1,
          sparse_jtj_raw_2);
        /// \todo Add error checking to cudaDeviceSynchronize
        checkCudaErrors(cudaDeviceSynchronize());
      }
    }

    // Calclulate sub-matrix of jtj for symmetrical splines
    void calculate_sub_jtj_non_symmetrical(
        const std::vector<float>&         prod_ima,
        const std::vector<int>&           ima_sz,
        const std::vector<float>&         spline_x_1,
        const std::vector<float>&         spline_y_1,
        const std::vector<float>&         spline_z_1,
        const std::vector<float>&         spline_x_2,
        const std::vector<float>&         spline_y_2,
        const std::vector<float>&         spline_z_2,
        const std::vector<int>&           coef_sz,
        const std::vector<int>&           ksp,
        const unsigned int                sub_jtj_row,
        const unsigned int                sub_jtj_col,
        MMORF::SparseDiagonalMatrixTiled& sparse_jtj)
    {
      // Make everything a device vector using thrust::
      auto sparse_jtj_offsets_dev = thrust::device_vector<int>(sparse_jtj.get_offsets());
      auto spline_x_1_dev = thrust::device_vector<float>(spline_x_1);
      auto spline_y_1_dev = thrust::device_vector<float>(spline_y_1);
      auto spline_z_1_dev = thrust::device_vector<float>(spline_z_1);
      auto spline_x_2_dev = thrust::device_vector<float>(spline_x_2);
      auto spline_y_2_dev = thrust::device_vector<float>(spline_y_2);
      auto spline_z_2_dev = thrust::device_vector<float>(spline_z_2);
      // Create texture handle
      auto texture_handle = MMORF::TextureHandleLinear(prod_ima,ima_sz);
      auto tex = texture_handle.get_texture();
      // Get all the raw pointers ready for the Kernel
      float *spline_x_1_raw = thrust::raw_pointer_cast(spline_x_1_dev.data());
      float *spline_y_1_raw = thrust::raw_pointer_cast(spline_y_1_dev.data());
      float *spline_z_1_raw = thrust::raw_pointer_cast(spline_z_1_dev.data());
      float *spline_x_2_raw = thrust::raw_pointer_cast(spline_x_2_dev.data());
      float *spline_y_2_raw = thrust::raw_pointer_cast(spline_y_2_dev.data());
      float *spline_z_2_raw = thrust::raw_pointer_cast(spline_z_2_dev.data());
      int *sparse_jtj_offsets_raw = thrust::raw_pointer_cast(
          sparse_jtj_offsets_dev.data());
      float *sparse_jtj_raw = sparse_jtj.get_raw_pointer(sub_jtj_row, sub_jtj_col);
      // Calculate parameters for running kernel
      int min_grid_size;
      int block_size;
      cudaOccupancyMaxPotentialBlockSize(
          &min_grid_size,
          &block_size,
          MMORF::kernel_make_jtj_non_symmetrical,
          0,
          0);
      unsigned int grid_size_diags = 343;
      unsigned int grid_size_chunks =
        (coef_sz[0]*coef_sz[1]*coef_sz[2] + block_size - 1)/block_size;
      auto grid_2d = dim3(grid_size_diags, grid_size_chunks);
      unsigned int smem =
        (spline_x_1.size() + spline_y_1.size() + spline_z_1.size()
        + spline_x_2.size() + spline_y_2.size() + spline_z_2.size())
        * sizeof(float);
      // Call CUDA Kernel
      MMORF::kernel_make_jtj_non_symmetrical<<<grid_2d,block_size,smem>>>(
          // Input
          ima_sz.at(0),
          ima_sz.at(1),
          ima_sz.at(2),
          tex,
          spline_x_1_raw,
          spline_y_1_raw,
          spline_z_1_raw,
          spline_x_2_raw,
          spline_y_2_raw,
          spline_z_2_raw,
          ksp.at(0),
          ksp.at(1),
          ksp.at(2),
          ksp.at(0),
          ksp.at(1),
          ksp.at(2),
          coef_sz.at(0),
          coef_sz.at(1),
          coef_sz.at(2),
          coef_sz.at(0),
          coef_sz.at(1),
          coef_sz.at(2),
          sparse_jtj_offsets_raw,
          // Output
          sparse_jtj_raw);
      checkCudaErrors(cudaDeviceSynchronize());
    }

    // Calclulate sub-matrix of jtj for symmetrical splines
    void calculate_sub_jtj_non_symmetrical_diag_hess(
        const std::vector<std::vector<float> >& prod_ima,
        const std::vector<int>&                 ima_sz,
        const std::vector<float>&               spline_x_1,
        const std::vector<float>&               spline_y_1,
        const std::vector<float>&               spline_z_1,
        const std::vector<float>&               spline_x_2,
        const std::vector<float>&               spline_y_2,
        const std::vector<float>&               spline_z_2,
        const std::vector<int>&                 coef_sz,
        const std::vector<int>&                 ksp,
        const unsigned int                      sub_jtj_row,
        const unsigned int                      sub_jtj_col,
        MMORF::SparseDiagonalMatrixTiled&       sparse_jtj)
    {
      // Calculate real Heassian offsets as sparse_jtj is now only a main diagonal matrix
      auto offsets = calculate_sparse_diag_offsets(coef_sz);
      // Make everything a device vector using thrust::
      auto sparse_jtj_offsets_dev = thrust::device_vector<int>(offsets);
      auto spline_x_1_dev = thrust::device_vector<float>(spline_x_1);
      auto spline_y_1_dev = thrust::device_vector<float>(spline_y_1);
      auto spline_z_1_dev = thrust::device_vector<float>(spline_z_1);
      auto spline_x_2_dev = thrust::device_vector<float>(spline_x_2);
      auto spline_y_2_dev = thrust::device_vector<float>(spline_y_2);
      auto spline_z_2_dev = thrust::device_vector<float>(spline_z_2);
      // Create texture handle. Note that as we are dealing with all combinations of
      // Jacobian elements which contribute to this sub-JTJ simultaneously we have to make
      // a vector of 9 different textures
      auto texture_handle_1 = MMORF::TextureHandleLinear(prod_ima[0],ima_sz);
      auto texture_handle_2 = MMORF::TextureHandleLinear(prod_ima[1],ima_sz);
      auto texture_handle_3 = MMORF::TextureHandleLinear(prod_ima[2],ima_sz);
      auto texture_handle_4 = MMORF::TextureHandleLinear(prod_ima[3],ima_sz);
      auto texture_handle_5 = MMORF::TextureHandleLinear(prod_ima[4],ima_sz);
      auto texture_handle_6 = MMORF::TextureHandleLinear(prod_ima[5],ima_sz);
      auto texture_handle_7 = MMORF::TextureHandleLinear(prod_ima[6],ima_sz);
      auto texture_handle_8 = MMORF::TextureHandleLinear(prod_ima[7],ima_sz);
      auto texture_handle_9 = MMORF::TextureHandleLinear(prod_ima[8],ima_sz);
      auto tex_dev = thrust::device_vector<cudaTextureObject_t>(9);
      tex_dev[0] = texture_handle_1.get_texture();
      tex_dev[1] = texture_handle_2.get_texture();
      tex_dev[2] = texture_handle_3.get_texture();
      tex_dev[3] = texture_handle_4.get_texture();
      tex_dev[4] = texture_handle_5.get_texture();
      tex_dev[5] = texture_handle_6.get_texture();
      tex_dev[6] = texture_handle_7.get_texture();
      tex_dev[7] = texture_handle_8.get_texture();
      tex_dev[8] = texture_handle_9.get_texture();
      cudaTextureObject_t *tex_raw = thrust::raw_pointer_cast(tex_dev.data());
      // Get all the raw pointers ready for the Kernel
      float *spline_x_1_raw = thrust::raw_pointer_cast(spline_x_1_dev.data());
      float *spline_y_1_raw = thrust::raw_pointer_cast(spline_y_1_dev.data());
      float *spline_z_1_raw = thrust::raw_pointer_cast(spline_z_1_dev.data());
      float *spline_x_2_raw = thrust::raw_pointer_cast(spline_x_2_dev.data());
      float *spline_y_2_raw = thrust::raw_pointer_cast(spline_y_2_dev.data());
      float *spline_z_2_raw = thrust::raw_pointer_cast(spline_z_2_dev.data());
      int *sparse_jtj_offsets_raw = thrust::raw_pointer_cast(
          sparse_jtj_offsets_dev.data());

      float *sparse_jtj_raw_1 = sparse_jtj.get_raw_pointer(sub_jtj_row, sub_jtj_row);
      // Calculate parameters for running kernel
      int min_grid_size;
      int block_size;
      cudaOccupancyMaxPotentialBlockSize(
          &min_grid_size,
          &block_size,
          MMORF::kernel_make_jtj_non_symmetrical_diag_hess_spred,
          0,
          0);
      unsigned int grid_size_diags = 343;
      unsigned int grid_size_chunks =
        (coef_sz[0]*coef_sz[1]*coef_sz[2] + block_size - 1)/block_size;
      auto grid_2d = dim3(grid_size_chunks, grid_size_diags);
      unsigned int smem =
        (spline_x_1.size() + spline_y_1.size() + spline_z_1.size()
        + spline_x_2.size() + spline_y_2.size() + spline_z_2.size())
        * sizeof(float);
      // Call CUDA Kernel
      MMORF::kernel_make_jtj_non_symmetrical_diag_hess_spred<<<grid_2d,block_size,smem>>>(
          // Input
          ima_sz.at(0),
          ima_sz.at(1),
          ima_sz.at(2),
          tex_raw,
          spline_x_1_raw,
          spline_y_1_raw,
          spline_z_1_raw,
          spline_x_2_raw,
          spline_y_2_raw,
          spline_z_2_raw,
          ksp.at(0),
          ksp.at(1),
          ksp.at(2),
          ksp.at(0),
          ksp.at(1),
          ksp.at(2),
          coef_sz.at(0),
          coef_sz.at(1),
          coef_sz.at(2),
          coef_sz.at(0),
          coef_sz.at(1),
          coef_sz.at(2),
          sparse_jtj_offsets_raw,
          // Output
          sparse_jtj_raw_1);
      checkCudaErrors(cudaDeviceSynchronize());
    }

    /// Calculate a sub-vector of Jte
    std::vector<float> calculate_sub_grad(
        const std::vector<float>&     grad_jxx_ima,
        const std::vector<int>&       ima_sz,
        const std::vector<float>&     spline_x,
        const std::vector<float>&     spline_y,
        const std::vector<float>&     spline_z,
        const std::vector<int>&       coef_sz,
        const std::vector<int>&       ksp)
    {
      // Make everything a device vector using thrust::
      auto sub_jte_sz   = coef_sz[0] * coef_sz[1] * coef_sz[2];
      auto sub_jte_dev  = thrust::device_vector<float>(sub_jte_sz);
      auto sub_jte_raw  = thrust::raw_pointer_cast(&sub_jte_dev[0]);
      auto spline_x_dev = thrust::device_vector<float>(spline_x);
      auto spline_y_dev = thrust::device_vector<float>(spline_y);
      auto spline_z_dev = thrust::device_vector<float>(spline_z);
      // Create texture handle
      auto texture_handle = MMORF::TextureHandleLinear(grad_jxx_ima,ima_sz);
      auto tex = texture_handle.get_texture();
      // Get all the raw pointers ready for the Kernel
      float *spline_x_raw = thrust::raw_pointer_cast(spline_x_dev.data());
      float *spline_y_raw = thrust::raw_pointer_cast(spline_y_dev.data());
      float *spline_z_raw = thrust::raw_pointer_cast(spline_z_dev.data());
      // Calculate parameters for running kernel
      int min_grid_size;
      int block_size;
      cudaOccupancyMaxPotentialBlockSize(
          &min_grid_size,
          &block_size,
          MMORF::kernel_make_jte,
          0,
          0);
      unsigned int grid_size = (sub_jte_sz + block_size - 1)/block_size;
      auto grid_1d = dim3(grid_size);
      unsigned int smem = (spline_x.size()+spline_y.size()+spline_z.size())*sizeof(float);
      // Call CUDA Kernel
      MMORF::kernel_make_jte<<<grid_1d,block_size,smem>>>(
          // Input
          ima_sz[0],
          ima_sz[1],
          ima_sz[2],
          tex,
          spline_x_raw,
          spline_y_raw,
          spline_z_raw,
          ksp[0],
          ksp[1],
          ksp[2],
          coef_sz[0],
          coef_sz[1],
          coef_sz[2],
          // Output
          sub_jte_raw);
      /// \todo Add error checking to cudaDeviceSynchronize
      checkCudaErrors(cudaDeviceSynchronize());

      // copy result back to host
      auto sub_jte = std::vector<float>(sub_jte_dev.size());
      thrust::copy(sub_jte_dev.begin(), sub_jte_dev.end(), sub_jte.begin());
      return sub_jte;
    }

    // Convert 1D spline to vector of floats
    // Note that this function takes care of correcting the derivative scaling if the
    // spline_res parameter is used. This parameter is the resolution between each value
    // in the spline
    std::vector<float> spline_as_vec(
        const BASISFIELD::Spline1D<float>& spline,
        float                              spline_res,
        int                                diff_order)
    {
      auto spline_vals = std::vector<float>(spline.KernelSize());
      for (auto i = 0; i < spline.KernelSize(); ++i)
      {
        spline_vals.at(i) =
          spline(static_cast<unsigned int>(i+1))
          / std::pow(spline_res, diff_order);
      }
      return spline_vals;
    }


    // Create an empty JtJ matrix. NB!!! In this case the matrix only contains the main
    // diagonal! And therefore we need a separate method to "calculate offsets" for what
    // the true Hessian would look like. This is annoying, but it's an unfortunate side-
    // effect of efficiency in the kernel.
    template<typename Field>
    MMORF::SparseDiagonalMatrixTiled create_empty_jtj(
        const std::shared_ptr<Field>  field,
        enum MMORF::CostFxn::HessType hess_type,
        unsigned int                  n_tiles)
    {
      auto coef_sz = field->get_dimensions();
      auto offsets =
        hess_type == CostFxn::HESS_FULL
        ? calculate_sparse_diag_offsets(coef_sz)
        : std::vector<int>(1,0);
      unsigned int max_diagonal = coef_sz[0]*coef_sz[1]*coef_sz[2];
      MMORF::SparseDiagonalMatrixTiled r_matrix(max_diagonal, max_diagonal, n_tiles, offsets);
      return r_matrix;
    }

    template MMORF::SparseDiagonalMatrixTiled create_empty_jtj<MMORF::WarpFieldBSpline>(
        const std::shared_ptr<MMORF::WarpFieldBSpline> field,
        enum MMORF::CostFxn::HessType                  hess_type,
        unsigned int                                   n_tiles);

    template MMORF::SparseDiagonalMatrixTiled create_empty_jtj<MMORF::BiasFieldBSpline>(
        const std::shared_ptr<MMORF::BiasFieldBSpline> field,
        enum MMORF::CostFxn::HessType                  hess_type,
        unsigned int                                   n_tiles);
  } // namespace CostFxnSplineUtils
} // namespace MMORF
