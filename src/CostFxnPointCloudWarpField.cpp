//////////////////////////////////////////////////////////////////////////////////////////////
/// \file
/// \brief Cost function based on the sum of squared distances between two point clouds
/// \details The warp is defined by b-spline field coefficients, one set per dimension in the
///          volume. Implicit point correspondance is assumed (i.e., clouds must be the same
///          size and ordered identically)
/// \author Frederik Lange
/// \date April 2024
/// \copyright Copyright (C) 2024 University of Oxford
//////////////////////////////////////////////////////////////////////////////////////////////
#include "CostFxnPointCloudWarpField.h"
#include "CostFxn.h"
#include "WarpFieldBSpline.h"
#include "SparseDiagonalMatrixTiled.h"
#include "WarpUtils.h"
#include "CostFxnSplineUtils.h"

#include "newmesh/newmesh.h"

#include <armadillo>

#include <memory>

/// Multi-Modal Registration Framework
namespace MMORF
{
  ////////////////////////////////////////////////////////////////////////////////
  // Class Specific Functions
  ////////////////////////////////////////////////////////////////////////////////
  /// Construct by passing in a list of points
  /// \param points_ref Reference (stationary) points list (3 x n_points)
  /// \param points_mov Moving points list (3 x n_points)
  /// \param affine_ref Pre-calculated affine transform FROM a common space TO the
  ///                   reference space
  /// \param affine_mov Pre-calculated affine transform FROM a common space TO the
  ///                   moving space
  /// \param warp_field Shared pointer to fully formed warp field
  template<MMORF::CostFxn::HessType hess_type>
  CostFxnPointCloudWarpField<hess_type>::CostFxnPointCloudWarpField(
      const std::vector<std::vector<float> >&  points_ref,
      const std::vector<std::vector<float> >&  points_mov,
      const arma::fmat&                        affine_ref,
      const arma::fmat&                        affine_mov,
      std::shared_ptr<MMORF::WarpFieldBSpline> warp_field
      )
  : points_ref_(points_ref)
  , points_mov_(points_mov)
  , affine_ref_(affine_ref)
  , affine_mov_(affine_mov)
  , warp_field_(warp_field)
  , hess_(0,0,0,std::vector<int>(0))
  {
    auto points_ref_aff = WarpUtils::apply_affine_transform(
      points_ref_,
      affine_ref_.i()
      );
    auto warp_dims = warp_field_->get_parameter_size();
    auto spline_indices = warp_field_->get_coefficient_indices_with_support_at_positions(points_ref_aff);
    auto spline_values = warp_field_->get_basis_values_with_support_at_positions(points_ref_aff);
    hess_ = CostFxnSplineUtils::create_empty_jtj(warp_field_, hess_type, 3);
    for (size_t i = 0; i < points_ref_aff[0].size(); ++i){ // Loop over points
      for (size_t j = 0; j < spline_indices[i].size(); j++){ // Loop over splines with support at each point
        for (size_t k = 0; k < points_ref_aff.size(); k++){ // Loop over directions
          for (size_t x = 0; x < spline_indices[i].size(); x++){ // Loop over splines again
            for (size_t y = 0; y < points_ref_aff.size(); y++){ // Loop over directions again
              auto row_ind = k*warp_dims.second + spline_indices[i][j];
              auto col_ind =
                hess_type == CostFxn::HESS_FULL
                ? y*warp_dims.second + spline_indices[i][x]
                : row_ind;
              auto hess_val_old = hess_.get_element(row_ind,col_ind);
              auto hess_val_change =
                hess_type == CostFxn::HESS_FULL
                ? (spline_values[i][j] * spline_values[i][x])/points_ref_aff[0].size()
                : std::abs((spline_values[i][j] * spline_values[i][x])/points_ref_aff[0].size());
              auto hess_val_new = hess_val_old + hess_val_change;
              hess_.set_element(row_ind, col_ind, hess_val_new);
            }
          }
        }
      }
    }
  }

  /// Construct by passing in fully constructed newmesh gifti objects
  /// \param mesh_ref Reference (stationary) gifti mesh
  /// \param mesh_mov Moving gifti mesh
  /// \param affine_ref Pre-calculated affine transform FROM a common space TO the
  ///                   reference space
  /// \param affine_mov Pre-calculated affine transform FROM a common space TO the
  ///                   moving space
  /// \param warp_field Shared pointer to fully formed warp field
  template<MMORF::CostFxn::HessType hess_type>
  CostFxnPointCloudWarpField<hess_type>::CostFxnPointCloudWarpField(
      const NEWMESH::newmesh&                  mesh_ref,
      const NEWMESH::newmesh&                  mesh_mov,
      const arma::fmat&                        affine_ref,
      const arma::fmat&                        affine_mov,
      std::shared_ptr<MMORF::WarpFieldBSpline> warp_field
      )
  //: points_ref_(3, std::vector<float>(mesh_ref.nvertices()/15 + 1, 0.0f))
  //, points_mov_(3, std::vector<float>(mesh_mov.nvertices()/15 + 1, 0.0f))
  : points_ref_(3, std::vector<float>(mesh_ref.nvertices(), 0.0f))
  , points_mov_(3, std::vector<float>(mesh_mov.nvertices(), 0.0f))
  , affine_ref_(affine_ref)
  , affine_mov_(affine_mov)
  , warp_field_(warp_field)
  , hess_(0,0,0,std::vector<int>(0))
  {
    std::cout << "!!! points_ref_[0].size() = " << points_ref_[0].size() << std::endl;
    assert (mesh_ref.nvertices() == mesh_mov.nvertices());
    for (size_t point = 0; point < mesh_ref.nvertices(); ++point)
    //for (size_t point = 0; point*15 < mesh_ref.nvertices(); ++point)
    {
      auto ref_point = mesh_ref.get_point(point).get_coord();
      auto mov_point = mesh_mov .get_point(point).get_coord();
      points_ref_[0][point] = ref_point.X;
      points_ref_[1][point] = ref_point.Y;
      points_ref_[2][point] = ref_point.Z;
      points_mov_[0][point] = mov_point.X;
      points_mov_[1][point] = mov_point.Y;
      points_mov_[2][point] = mov_point.Z;
    }
    auto points_ref_aff = WarpUtils::apply_affine_transform(
      points_ref_,
      affine_ref_.i()
      );
    auto warp_dims = warp_field_->get_parameter_size();
    auto spline_indices = warp_field_->get_coefficient_indices_with_support_at_positions(points_ref_aff);
    auto spline_values = warp_field_->get_basis_values_with_support_at_positions(points_ref_aff);
    hess_ = CostFxnSplineUtils::create_empty_jtj(warp_field_, hess_type, 3);
    for (size_t i = 0; i < points_ref_aff[0].size(); ++i){ // Loop over points
      for (size_t j = 0; j < spline_indices[i].size(); j++){ // Loop over splines with support at each point
        for (size_t k = 0; k < points_ref_aff.size(); k++){ // Loop over directions
          for (size_t x = 0; x < spline_indices[i].size(); x++){ // Loop over splines again
            for (size_t y = 0; y < points_ref_aff.size(); y++){ // Loop over directions again
              auto row_ind = k*warp_dims.second + spline_indices[i][j];
              auto col_ind =
                hess_type == CostFxn::HESS_FULL
                ? y*warp_dims.second + spline_indices[i][x]
                : row_ind;
              auto hess_val_old = hess_.get_element(row_ind,col_ind);
              auto hess_val_change =
                hess_type == CostFxn::HESS_FULL
                ? (spline_values[i][j] * spline_values[i][x])/points_ref_aff[0].size()
                : std::abs((spline_values[i][j] * spline_values[i][x])/points_ref_aff[0].size());
              auto hess_val_new = hess_val_old + hess_val_change;
              hess_.set_element(row_ind, col_ind, hess_val_new);
            }
          }
        }
      }
    }
  }

  /// Get the current value of the parameters
  template<MMORF::CostFxn::HessType hess_type>
  std::vector<std::vector<float> > CostFxnPointCloudWarpField<hess_type>::get_parameters() const
  {
    return warp_field_->get_parameters();
  }

  /// Set the current value of the parameters
  template<MMORF::CostFxn::HessType hess_type>
  void CostFxnPointCloudWarpField<hess_type>::set_parameters(
      const std::vector<std::vector<float> >& parameters)
  {
    warp_field_->set_parameters(parameters);
  }

  /// Get cost under current parameterisation
  template<MMORF::CostFxn::HessType hess_type>
  float CostFxnPointCloudWarpField<hess_type>::cost() const
  {
    auto points_ref_trans = WarpUtils::apply_affine_then_nonlinear_transform(
      points_ref_,
      affine_ref_.i(),
      *warp_field_
      );
    auto points_mov_trans = WarpUtils::apply_affine_transform(
      points_mov_,
      affine_mov_.i()
      );
    auto sse = 0.0f;
    for (size_t i = 0; i < points_ref_trans.size(); ++i){
      for (size_t j = 0; j < points_ref_trans[i].size(); ++j){
        auto diff = points_ref_trans[i][j] - points_mov_trans[i][j];
        sse += (diff * diff);
      }
    }
    auto mse = sse / points_ref_trans[0].size();
    return mse;
  }

  /// Get Jte under current parametrisation
  template<MMORF::CostFxn::HessType hess_type>
  arma::fvec CostFxnPointCloudWarpField<hess_type>::grad() const
  {
    auto points_ref_aff = WarpUtils::apply_affine_transform(
      points_ref_,
      affine_ref_.i()
      );
    auto points_ref_trans = WarpUtils::apply_affine_then_nonlinear_transform(
      points_ref_,
      affine_ref_.i(),
      *warp_field_
      );
    auto points_mov_trans = WarpUtils::apply_affine_transform(
      points_mov_,
      affine_mov_.i()
      );
    auto warp_dims = warp_field_->get_parameter_size();
    auto spline_indices = warp_field_->get_coefficient_indices_with_support_at_positions(points_ref_aff);
    auto spline_values = warp_field_->get_basis_values_with_support_at_positions(points_ref_aff);
    auto jte = std::vector<float>(warp_dims.first * warp_dims.second, 0.0f);
    for (size_t i = 0; i < points_ref_aff[0].size(); ++i){ // Loop over points
      for (size_t j = 0; j < spline_indices[i].size(); j++){ // Loop over splines with support at each point
        for (size_t k = 0; k < points_ref_aff.size(); k++){ // Loop over directions
          auto diff = points_ref_trans[k][i] - points_mov_trans[k][i];
          jte[k*warp_dims.second + spline_indices[i][j]] += (diff*spline_values[i][j])/points_ref_aff[0].size();
        }
      }
    }
    auto jte_return = arma::fvec(jte.data(), jte.size());
    return jte_return;
  }

  template<MMORF::CostFxn::HessType hess_type>
  MMORF::SparseDiagonalMatrixTiled CostFxnPointCloudWarpField<hess_type>::hess() const
  {
    return hess_;
  }

  template class CostFxnPointCloudWarpField<MMORF::CostFxn::HessType::HESS_FULL>;
  template class CostFxnPointCloudWarpField<MMORF::CostFxn::HessType::HESS_DIAG>;
} // MMORF
